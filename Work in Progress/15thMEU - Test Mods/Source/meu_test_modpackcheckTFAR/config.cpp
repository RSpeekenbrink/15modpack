
class CfgPatches
{
	class 15thmeu_testmodpackTFAR
	{
		units[] = {};
		weapons[] = {};
		magazine[] = {};
		ammo[] = {};
		author[] = {"15thMEU"};
		requiredVersion = 1.0;
		requiredAddons[] = {};
		version = 1.0;
        versionStr = "1.0";
        versionAr[] = {1.0};
        versionDesc = "15th MEU TFAR Mod Pack Test";
        versionAct = "";
	};
};
class CfgSettings 
{
   class CBA 
   {
      class Versioning 
      {
         class 15thmeu_testmodpackTFAR {};
      };
   };
};