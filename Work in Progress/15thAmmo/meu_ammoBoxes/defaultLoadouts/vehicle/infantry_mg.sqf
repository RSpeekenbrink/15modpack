// REVISION 02JUN16

// Weapons Platoon, Machine Gun Vehicle Supplies

[
	["rhsusf_mag_15Rnd_9x19_FMJ", 4],
	["rhs_mag_30Rnd_556x45_Mk318_Stanag", 10],
	["rhsusf_100Rnd_762x51", 10],
	["ClaymoreDirectionalMine_Remote_Mag",2],
	["itemRadio", 2],
	["ACE_fieldDressing", 6],
	["ACE_epinephrine", 2],
	["rhs_mag_an_m8hc", 4],
	["rhs_mag_m18_green", 2],
	["rhs_mag_m18_yellow", 2],
	["SmokeShellBlue", 2],
	["rhs_mag_m67", 2],
	["ace_M84", 2],
	["rhs_mag_M433_HEDP", 4],
	["rhs_mag_m714_White", 4],
	["rhs_mag_m713_Red", 2],
	["Chemlight_green",5],
	["rhs_weap_M136", 1] // no comma
];