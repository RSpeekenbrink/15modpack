// REVISION 02JUN16

// Weapons Platoon, Mortar Vehicle Supplies

[
	["rhs_weap_M136", 2],
	["rhs_mag_30Rnd_556x45_Mk318_Stanag", 20],
	["rhs_mag_an_m8hc", 6],
	["rhs_mag_m18_green", 6],
	["DemoCharge_Remote_Mag", 2],
	["ACE_fieldDressing", 6],
	["ACE_epinephrine", 2],
	["ace_Clacker", 1],
	["ace_UAVBattery", 1],
	["B_UavTerminal", 1],
	["Laserdesignator", 1],
	["ToolKit", 1],
	["B_UAV_01_backpack_F", 1] // no comma
];
