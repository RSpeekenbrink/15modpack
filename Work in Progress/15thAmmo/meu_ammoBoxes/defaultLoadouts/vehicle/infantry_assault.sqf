// REVISION 02JUN16

// Weapons Platoon, Assault Supply Box
[
	["rhsusf_mag_15Rnd_9x19_FMJ", 4],
	["rhs_mag_30Rnd_556x45_Mk318_Stanag", 10],
	["rhs_mag_smaw_HEDP", 3],
	["rhs_mag_smaw_HEAA", 2],
	["DemoCharge_Remote_Mag",4],
	["ClaymoreDirectionalMine_Remote_Mag",2],
	["SatchelCharge_Remote_Mag",2],
	["itemRadio", 2],
	["ACE_fieldDressing", 6],
	["ACE_epinephrine", 2],
	["rhs_mag_an_m8hc", 4],
	["rhs_mag_m18_green", 2],
	["rhs_mag_m18_yellow", 2],
	["SmokeShellBlue", 2],
	["rhs_mag_m67", 2],
	["ace_M84", 2],
	["rhs_mag_M433_HEDP", 4],
	["rhs_mag_m714_White", 4],
	["rhs_mag_m713_Red", 2],
	["Chemlight_green",5] // no comma
];