// REVISION 16JUN16

// BAS Supply Box

[
	["ACE_packingBandage",100],
	["ACE_epinephrine",20],
	["ace_Morphine",40],
	["ACE_plasmaIV_500",10],
	["ItemRadio",5],
	["rhs_mag_30Rnd_556x45_Mk318_Stanag",20],
	["rhsusf_mag_15Rnd_9x19_FMJ",10],
	["ace_M84",40],
	["rhs_mag_an_m8hc",5],
	["rhs_mag_m18_green",5],
	["rhs_mag_m18_purple",5],
	["SmokeShellBlue",5],
	["Chemlight_green",5]
];