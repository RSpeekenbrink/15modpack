// REVISION 16JUN16

//EOD Lead

 _unit = _this select 1;

 if (!local _unit) exitWith {};

 removeallweapons _unit;
 removebackpack _unit;
 removeuniform _unit;
 removevest _unit;
 removeheadgear _unit;
 _unit addvest "rhsusf_spc_rifleman";
 _unit addheadgear "rhsusf_ech_helmet_marpatwd";

 _unit unassignItem "nvgoggles";
 _unit removeItem "nvgoggles";

 _unit additem "rhsusf_ANPVS_14";
 _unit assignItem "rhsusf_ANPVS_14";

 _unit additem "itemGPS";
 _unit assignitem "itemGPS";

 _unit additem "itemMap";
 _unit assignitem "itemMap";

 _unit additem "itemCompass";
 _unit assignitem "itemCompass";

 _unit additem "itemRadio";
 _unit additem "itemRadio";
 _unit additem "itemRadio";
 _unit additem "itemRadio";
 _unit additem "itemRadio";

 _unit addbackpack "tfw_ilbe_dd_coy";

 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";

 _unit addweapon "rhs_weap_m4";
 _unit addPrimaryWeaponItem "rhsusf_acc_ACOG_USMC";
 _unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side";

 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";

 _unit addweapon "rhsusf_weap_m9";

 _unit addweapon "ACE_Vector";
 _unit assignitem "ACE_Vector";

 _unit addweapon "itemandroid";

 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_m713_Red";
 _unit addmagazine "rhs_mag_m713_Red";
 _unit addmagazine "rhs_mag_m715_Green";
 _unit addmagazine "rhs_mag_m715_Green";
 _unit additem "ACE_EarPlugs";

 _unit adduniform "rhs_uniform_FROG01_wd";