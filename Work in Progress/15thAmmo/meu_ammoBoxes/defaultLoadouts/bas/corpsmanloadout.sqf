// REVISION 02JUN16

//15th Corpsman Loadout
 _unit = _this select 1;

 if (!local _unit) exitWith {};

 removeallweapons _unit;
 removebackpack _unit;
 removeuniform _unit;
 removevest _unit;
 removeheadgear _unit;

 _unit addvest "rhsusf_spc_corpsman";
 _unit addheadgear "rhsusf_ech_helmet_marpatwd";

 _unit unassignItem "nvgoggles";
 _unit removeItem "nvgoggles";

 _unit additem "rhsusf_ANPVS_14";
 _unit unassignItem "rhsusf_ANPVS_14";

 _unit additem "itemGPS";
 _unit assignitem "itemGPS";

 _unit additem "itemRadio";

 _unit addbackpack "Trixie_Coyote_OCP";

 (unitBackpack _unit) additemCargo ["ACE_fieldDressing" ,30];
 (unitBackpack _unit) additemCargo ["ACE_epinephrine" ,10];
 (unitBackpack _unit) additemCargo ["ace_Morphine" ,20];
 (unitBackpack _unit) additemCargo ["ACE_bloodIV_250" ,2];

 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";

 _unit addweapon "rhs_weap_m4";
 _unit addPrimaryWeaponItem "rhsusf_acc_anpeq15A";

 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";

 _unit addweapon "rhsusf_weap_m9";

 _unit addmagazine "rhs_mag_m18_green";
 _unit addmagazine "SmokeShellBlue";
 _unit addmagazine "rhs_mag_m18_purple";
 _unit addmagazine "rhs_mag_m18_purple";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit additem "ace_Earplugs";

 _unit adduniform "rhs_uniform_FROG01_wd";