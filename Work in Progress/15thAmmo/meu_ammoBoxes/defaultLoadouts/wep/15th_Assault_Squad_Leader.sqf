// REVISION 16JUN16

//15th Weapons Assault Squad Leader

 _unit = _this select 1;

 if (!local _unit) exitWith {};

 removeallweapons _unit;
 removebackpack _unit;
 removeuniform _unit;
 removevest _unit;
 removeheadgear _unit;
 _unit addvest "rhsusf_spc_squadleader";
 _unit addheadgear "rhsusf_ech_helmet_marpatwd";

  _unit unassignItem "nvgoggles";
 _unit removeItem "nvgoggles";

 _unit additem "itemGPS";
 _unit assignitem "itemGPS";

 _unit additem "itemRadio";
 _unit additem "itemRadio";
 _unit additem "itemRadio";

 _unit addbackpack "B_Kitbag_cbr";

 (unitBackpack _unit) additemCargo ["ACE_fieldDressing",3];
 (unitBackpack _unit) additemCargo ["rhsusf_ANPVS_14",1];
 (unitBackpack _unit) addmagazineCargo ["rhs_mag_smaw_HEDP",1];
 (unitBackpack _unit) addmagazineCargo ["DemoCharge_Remote_Mag",1];
 (unitBackpack _unit) addmagazineCargo ["SatchelCharge_Remote_Mag",1];
 (unitBackpack _unit) additemCargo ["itemandroid",1];

 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";

 _unit addmagazine "rhs_mag_M433_HEDP";
 _unit addmagazine "rhs_mag_M433_HEDP";
 _unit addmagazine "rhs_mag_M433_HEDP";
 _unit addmagazine "rhs_mag_M433_HEDP";
 _unit addmagazine "rhs_mag_M433_HEDP";
 _unit addmagazine "rhs_mag_M433_HEDP";
 _unit addmagazine "rhs_mag_M433_HEDP";
 _unit addmagazine "rhs_mag_M433_HEDP";

 _unit addweapon "rhs_weap_m4_m203";
 _unit addPrimaryWeaponItem "rhsusf_acc_ACOG_USMC";
 _unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side";

 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";

 _unit addweapon "rhsusf_weap_m9";

 _unit addweapon "ACE_Vector";
 _unit assignitem "ACE_Vector";

 _unit addmagazine "rhs_mag_m713_Red";
 _unit addmagazine "rhs_mag_m713_Red";
 _unit addmagazine "rhs_mag_m67";
 _unit addmagazine "rhs_mag_m67";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_m18_green";
 _unit additem "ace_Eaplugs";
 _unit additem "ItemCtabHCam";

 _unit addmagazine "ace_M84";
 _unit addmagazine "ace_M84";

 _unit adduniform "rhs_uniform_FROG01_wd";