// REVISION 02JUN16

//15th Weapons Assault Team Leader

 _unit = _this select 1;

 if (!local _unit) exitWith {};

 removeallweapons _unit;
 removebackpack _unit;
 removeuniform _unit;
 removevest _unit;
 removeheadgear _unit;
 _unit addvest "rhsusf_spc_teamleader";
 _unit addheadgear "rhsusf_ech_helmet_marpatwd";

 _unit unassignItem "nvgoggles";
 _unit removeItem "nvgoggles";

 _unit additem "itemGPS";
 _unit assignitem "itemGPS";

 _unit addbackpack "B_Kitbag_cbr";

 (unitBackpack _unit) additemCargo ["ACE_fieldDressing",3];
 (unitBackpack _unit) additemCargo ["rhsusf_ANPVS_14",1];
 (unitBackpack _unit) addmagazineCargo ["rhs_mag_smaw_HEDP",1];
 (unitBackpack _unit) addmagazineCargo ["rhs_mag_smaw_HEAA",1];
 (unitBackpack _unit) addmagazineCargo ["rhs_mag_smaw_SR",3];

 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";

 _unit addweapon "rhs_weap_m4";
 _unit addPrimaryWeaponItem "rhsusf_acc_ACOG_USMC";
 _unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side";
 _unit addweapon "tf47_smaw";

 _unit addweapon "ACE_Vector";
 _unit assignitem "ACE_Vector";

 _unit addmagazine "rhs_mag_m67";
 _unit addmagazine "rhs_mag_m67";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_m18_green";
 _unit addmagazine "SmokeshellBlue";
 _unit additem "ace_Earplugs";
 _unit additem "itemRadio";
 _unit additem "itemRadio";

 _unit addmagazine "ace_M84";
 _unit addmagazine "ace_M84";

 _unit adduniform "rhs_uniform_FROG01_wd";