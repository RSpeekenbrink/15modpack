// REVISION 02JUN16

//15th Weapons MG Gunner

 _unit = _this select 1;

 if (!local _unit) exitWith {};

 removeallweapons _unit;
 removebackpack _unit;
 removeuniform _unit;
 removevest _unit;
 removeheadgear _unit;
 _unit addvest "rhsusf_spc_rifleman";
 _unit addheadgear "rhsusf_ech_helmet_marpatwd";

 _unit unassignItem "nvgoggles";
 _unit removeItem "nvgoggles";

 _unit addbackpack "B_Kitbag_cbr";

 (unitBackpack _unit) additemCargo ["ACE_fieldDressing",3];
 (unitBackpack _unit) additemCargo ["rhsusf_ANPVS_14",1];
 (unitBackpack _unit) additemCargo ["SmokeshellBlue",1];
 (unitBackpack _unit) additemCargo ["AV_ESS_tan_clr",1];

 _unit addmagazine "rhsusf_acc_anpeq15side";
 _unit addmagazine "rhsusf_acc_anpeq15side";
 _unit addmagazine "rhsusf_100Rnd_762x51_m62_tracer";

 _unit addweapon "rhs_weap_m240B";
 _unit addPrimaryWeaponItem "rhsusf_acc_ELCAN";
 _unit addPrimaryWeaponItem "bipod_01_F_Blk";

 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";

 _unit addweapon "rhsusf_weap_m9";

 _unit addmagazine "rhs_mag_m67";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_an_m8hc";
 _unit addmagazine "rhs_mag_m18_green";
 _unit additem "ace_Earplugs";

 _unit adduniform "rhs_uniform_FROG01_wd";