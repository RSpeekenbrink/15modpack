// REVISION 16JUN16

//Armored Vehicle, Crewman Loadout

 _unit = _this select 1;

 if (!local _unit) exitWith {};

 removeallweapons _unit;
 removebackpack _unit;
 removeuniform _unit;
 removevest _unit;
 removeheadgear _unit;
 _unit addvest "rhsusf_spc_rifleman";
 _unit addheadgear "H_HelmetCrew_I";

 _unit unassignItem "nvgoggles";
 _unit removeItem "nvgoggles";

 _unit additem "rhsusf_ANPVS_14";
 _unit assignItem "rhsusf_ANPVS_14";

 _unit additem "itemGPS";
 _unit assignitem "itemGPS";

 _unit additem "itemRadio";
 _unit additem "itemRadio";
 _unit assignitem "itemRadio";

 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";
 _unit addmagazine "rhs_mag_30Rnd_556x45_Mk318_Stanag";

 _unit addweapon "rhs_weap_m4";
 _unit addPrimaryWeaponItem "rhsusf_acc_compm4";
 _unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side";

 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";
 _unit addmagazine "rhsusf_mag_15Rnd_9x19_FMJ";

 _unit addweapon "rhsusf_weap_m9";

 _unit addweapon "Binocular";
 _unit assignitem "Binocular";

 _unit addmagazine "SmokeShellBlue";
 _unit addmagazine "rhs_mag_m18_red";
 _unit addmagazine "rhs_mag_m18_red";
 _unit addmagazine "rhs_mag_m18_green";
 _unit addmagazine "rhs_mag_m18_green";
 _unit additem "ACE_EarPlugs";

 _unit adduniform "rhs_uniform_FROG01_d";