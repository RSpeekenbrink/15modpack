// Eden Attributes for 15th Ammo Crates
// version date 7/30/16
// fight9

class Attributes
{
	class meu_boxManager
	{		
		displayName = "Disable 15th Loadout Manager";
		tooltip = "Removes the option to open the 15th Loadout Manager";
		property = "meu_LOM_main";
		control = "CheckBox";
		expression = "_this setVariable ['%s',_value];";
		defaultValue = false;		
	};
	class meu_boxDefaults
	{		
		displayName = "Disable 15th Default Loadouts";
		tooltip = "Removes the option to use default loadouts";
		property = "meu_LOM_defaults";
		control = "CheckBox";
		expression = "_this setVariable ['%s',_value];";
		defaultValue = false;		
	};
	class meu_boxRestrictions
	{
		displayName = "Disable 'Gear in Box' Check";
		tooltip = "15th Loadout Manager 'Gear in Box' check. Disabling allows all saved gear.";
		property = "meu_LOM_boxCheck";
		control = "CheckboxReversed";
		expression = "_this setVariable ['%s',_value];";
		defaultValue = true;
	};
};