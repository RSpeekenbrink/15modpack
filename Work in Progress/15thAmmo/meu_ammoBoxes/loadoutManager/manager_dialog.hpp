// 15TH LOADOUT MANAGER 2.5
// fight9
// built on Riouken's framework
// version date 7/30/16

#include "manager_macros.hpp"

#define HEADER_BG_COLOR		COLOR_MU_BG_CONFIG
#define LIST_TEXT_COLORS	COLOR_WARNING_CONFIG
#define MAIN_BG_COLOR		{0,0,0,0.8}
#define NO_SHOW onload = "_this select 0 ctrlShow false;";

#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(-0.1)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)

class ctrlCheckbox;
class ctrlButton;
class meu_loadoutManager
{
	idd = 5555555;
	movingEnable = true;
	onLoad = "uiNamespace setVariable ['meu_managerGUI',(_this select 0)];";
	onUnLoad = "missionNamespace setVariable ['meu_managerBOX',nil];";
//	onKeyDown = "if (_this select 1 in actionKeys 'Gear') then {['buttons',[(uiNamespace getVariable 'meu_managerGUI') displayCtrl 15027]] call meu_fnc_manager;}";
	onKeyDown = STR_CALL_FUNC("keydown",_this);
	objects[] = {};
	
	class Controls
	{	
		////////////////////////////
		//// FRAMES
		////////////////////////////
		class meu_LOHeader_Box_base // base class BOX
		{
			idc = -1;
            x = 0 * GUI_GRID_W + GUI_GRID_X;
			y = 2.9 * GUI_GRID_H + GUI_GRID_Y;
            w = 39 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
			colorBackground[] = HEADER_BG_COLOR;
			style = 0x02;
			type = 0;
			shadow = 2;
			colorText[] = {1,1,1,1};
			font = "PuristaMedium";
			sizeEx = 0.02;
			text = "";
		};
 		class meu_LOBoxPic: meu_LOHeader_Box_base // base class BOX2
		{
			idc = -1;
			x = 12.1 * GUI_GRID_W + GUI_GRID_X; 
			y = 4 * GUI_GRID_H + GUI_GRID_Y;
			w = 8.8 * GUI_GRID_W;
			h = 9 * GUI_GRID_H;
			colorBackground[] = MAIN_BG_COLOR;
		}; 
		class meu_LOBoxSaved: meu_LOBoxPic
		{
			idc = -1;
			text = "";
			x = 0 * GUI_GRID_W + GUI_GRID_X;
			y = 4 * GUI_GRID_H + GUI_GRID_Y;
			w = 12 * GUI_GRID_W;
			h = 20 * GUI_GRID_H;
		};
		class meu_LOBoxGear: meu_LOBoxPic
		{
			idc = -1;
			text = "";
			x = 21 * GUI_GRID_W + GUI_GRID_X;
			y = 4 * GUI_GRID_H + GUI_GRID_Y;
			w = 18 * GUI_GRID_W;
			h = 20 * GUI_GRID_H;
		};
		class meu_LOBoxLower: meu_LOBoxPic
		{
			idc = -1;
			x = 12.1 * GUI_GRID_W + GUI_GRID_X; 
			y = 13.07 * GUI_GRID_H + GUI_GRID_Y;
			w = 8.8 * GUI_GRID_W;
			h = 10.9 * GUI_GRID_H;
		};
		class meu_LOBoxMenu: meu_LOBoxPic
		{
			idc = -1;
			x = -1.6 * GUI_GRID_W + GUI_GRID_X; 
			y = 4 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.5 * GUI_GRID_W;
			h = 8.3 * GUI_GRID_H;
		}; 		
		////////////////////////////
		//// MAIN LISTS
		////////////////////////////
		class meu_LOListSaved_RscListBox_base // base class RscListBox
		{
			idc = MEU_CTRL_SAVEDLIST;
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = 5 * GUI_GRID_H + GUI_GRID_Y;
			w = 10 * GUI_GRID_W;
			h = 18 * GUI_GRID_H; 
			
			onMouseButtonDblClick = STR_CALL_FUNC("buttons",_this);
			onSetFocus = STR_CALL_FUNC("listDefault",[]);
			onLBSelChanged = STR_CALL_FUNC("listgear",nil);
			onKeyDown = "if (_this select 1 in [211]) then {[""buttons"",[(uiNamespace getVariable 'meu_managerGUI') displayCtrl 15022]] call meu_fnc_manager;};";
			
			deletable = 0;
			fade = 0;
			access = 0;
			type = 5;
			rowHeight = 0;
			colorText[] = LIST_TEXT_COLORS;
			colorDisabled[] = {1,1,1,0.25};
			colorScrollbar[] = {1,0,0,0};
			colorSelect[] = {0,0,0,1};
			colorSelect2[] = {0,0,0,1};
			colorSelectBackground[] = 
			{
				"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"
			};
			colorSelectBackground2[] = 
			{
				"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"
			};
			colorBackground[] = {0,0,0,0.3};
			soundSelect[] = {"\A3\ui_f\data\sound\RscListbox\soundSelect",0.09,1};
			autoScrollSpeed = -1;
			autoScrollDelay = 5;
			autoScrollRewind = 0;
			arrowEmpty = "#(argb,8,8,3)color(1,1,1,1)";
			arrowFull = "#(argb,8,8,3)color(1,1,1,1)";
			colorPicture[] = {1,1,1,1};
			colorPictureSelected[] = {0,0,0,1};
			colorPictureDisabled[] = {1,1,1,0.25};
			colorPictureRight[] = LIST_TEXT_COLORS;
			colorPictureRightSelected[] = {0,0,0,1};
			colorPictureRightDisabled[] = {1,1,1,0.25};
			tooltipColorText[] = {1,1,1,1};
			tooltipColorBox[] = {1,1,1,1};
			tooltipColorShade[] = {0,0,0,0.65};
			class ListScrollBar
			{
				color[] = {1,1,1,1};
				autoScrollEnabled = 1;
			};
			style = 16;
			font = "PuristaMedium";
			size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
			sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 32) * 1)";
			shadow = 0;
			colorShadow[] = {0,0,0,0.5};
			period = 1.2;
			maxHistoryDelay = 1;
		};
		class meu_LOListGear_RscTree_base  // base class RscTree
		{
			idc = MEU_CTRL_GEARTREE;
			x = 22 * GUI_GRID_W + GUI_GRID_X;
			y = 5 * GUI_GRID_H + GUI_GRID_Y;
			w = 16 * GUI_GRID_W;
			h = 18 * GUI_GRID_H;
			//////////////
			access = 0; 
			type = 12; 
			style = 0x00; 
			default = 0; 
			blinkingPeriod = 0; 
			colorBorder[] = {0,0,0,0}; 
			colorBackground[] = {0,0,0,0.3}; 
			colorSelect[] = {0,0,0,0}; 
			colorMarked[] = {1,0.5,0,0.5};
			colorMarkedSelected[] = {1,0.5,0,1};
			// v1.6 possible fix
			colorPicture[] = {1,1,1,1};
			colorPictureSelected[] = {1,1,1,1};
			colorPictureDisabled[] = {1,1,1,0.25};
			colorPictureRight[] = {1,1,1,1};
			colorPictureRightSelected[] = {0,0,0,1};
			colorPictureRightDisabled[] = {1,1,1,0.25};
			size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
			sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 32) * 1)";
			font = "PuristaMedium";  
			shadow = 1; 
			colorText[] = LIST_TEXT_COLORS;  
			colorSelectText[] = {1,0.5,0,1}; 
			colorMarkedText[] = {1,1,1,1};
			tooltip = "";
			tooltipColorShade[] = {0,0,0,1}; 
			tooltipColorText[] = {1,1,1,1};
			tooltipColorBox[] = {1,1,1,1};
			multiselectEnabled = 0;
			expandOnDoubleclick = 1;
			hiddenTexture = "A3\ui_f\data\gui\rsccommon\rsctree\hiddenTexture_ca.paa"; 
			expandedTexture = "A3\ui_f\data\gui\rsccommon\rsctree\expandedTexture_ca.paa"; 
			maxHistoryDelay = 1;
			class ScrollBar { 
				width = 0;
				height = 0;
				scrollSpeed = 0.1;
				arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
				arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
				border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
				thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
				color[] =  
				{
					"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])",
					"(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])",
					"(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])",
					"(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"
				};
			}; 
			colorDisabled[] = {1,1,1,0.25};
			colorArrow[] =  
			{
				"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"
			};
		};
		class meu_LOListDefault: meu_LOListSaved_RscListBox_base 
		{
			idc = MEU_CTRL_DEFAULTLIST;
			x = 12.6 * GUI_GRID_W + GUI_GRID_X; 
			y = 14.07 * GUI_GRID_H + GUI_GRID_Y;
			w = 7.8 * GUI_GRID_W;
			h = 8.9 * GUI_GRID_H;
			
			onSetFocus = STR_CALL_FUNC("listSaved",[-1]);
			onMouseButtonDblClick = STR_CALL_FUNC("loadDefault",_this);
			onKeyDown = "";
		};
		////////////////////////////
		//// TEXT
		////////////////////////////
		class meu_LO_rscPicture_base // base class RscPicture
		{
			idc = -1;
			text = "\meu_ammoBoxes\loadoutManager\logo256.paa";
			x = 11.6 * GUI_GRID_W + GUI_GRID_X;
			y = 3.6 * GUI_GRID_H + GUI_GRID_Y;
			w = 10 * GUI_GRID_W;
			h = 10 * GUI_GRID_H;
			style = 0x30 + 0x800;
			colorBackground[] = {0,0,0,0};
			colorText[] = {1,1,1,1};
			font = "puristaMedium";
			sizeEx = 0;
			lineSpacing = 0;
			fixedWidth = 0;
			shadow = 0;
			deletable = 0;
			fade = 0;
			access = 0;
			type = 0;
			tooltipColorText[] = {1,1,1,1};
			tooltipColorBox[] = {1,1,1,1};
			tooltipColorShade[] = {0,0,0,0.65};
		};
		class meu_LOTextHeader_RscText_base // base class RscText
		{
			idc = -1;
            x = 0 * GUI_GRID_W + GUI_GRID_X;
			y = 2.9 * GUI_GRID_H + GUI_GRID_Y; //1
            w = 39 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
			text = "15TH LOADOUT MANAGER 2.5";
			shadow = 0;
			deletable = 0;
			fade = 0;
			access = 0;
			type = 0;
			colorBackground[] = {0,0,0,0};
			colorText[] = {1,1,1,1};
			fixedWidth = 0;
			style = 0;
			colorShadow[] = {0,0,0,0.5};
			font = "PuristaMedium";
			SizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
			linespacing = 1;
			tooltipColorText[] = {1,1,1,1};
			tooltipColorBox[] = {1,1,1,1};
			tooltipColorShade[] = {0,0,0,0.65};
		};
		class meu_LOTextGear: meu_LOTextHeader_RscText_base
		{
			idc = MEU_CTRL_GEARTEXT;
            x = 22 * GUI_GRID_W + GUI_GRID_X;
			y = 4 * GUI_GRID_H + GUI_GRID_Y; //1
            w = 16 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
			text = "Current Gear";
			shadow = 0;
//			SizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 35) * 1)";
			sizeEx = "3 * (1 / (getResolution select 3)) * pixelGrid * 0.5";
		};
		class meu_LOTextSaved: meu_LOTextHeader_RscText_base
		{
			idc = MEU_CTRL_SAVEDTEXT;
            x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = 4 * GUI_GRID_H + GUI_GRID_Y; //1
            w = 10 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
			text = "Saved Loadouts";
			shadow = 0;
//			SizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 35) * 1)";
			sizeEx = "3 * (1 / (getResolution select 3)) * pixelGrid * 0.5";
		};
		class meu_LOTextDefault: meu_LOTextHeader_RscText_base
		{
			idc = -1;
            x = 12.6 * GUI_GRID_W + GUI_GRID_X;
			y = 13.07 * GUI_GRID_H + GUI_GRID_Y; //1
            w = 10 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
			text = "Default Gear";
			shadow = 0;
			SizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 35) * 1)";
		};
		////////////////////////////
		//// ICONS
		////////////////////////////
		class meu_LOXClose_RscActiveText_base // base class RscActiveText
		{
			idc = MEU_CTRL_CLOSEICON;
			text = "\A3\Ui_f\data\GUI\Rsc\RscDisplayArcadeMap\icon_exit_cross_ca.paa";
			tooltip = "Exit";
			action = "closeDialog 0;";
			
			x = 38 * GUI_GRID_W + GUI_GRID_X;
			y = 3.05 * GUI_GRID_H + GUI_GRID_Y;
			w = 0.76 * GUI_GRID_W;
			h = 0.7 * GUI_GRID_H;
			
			style = 48;
			default = false;
			access = 0;
			type = 11;
			color[] = {1,1,1,0.7};
			colorActive[] = {1,1,1,1};
			colorText[] = {1,1,1,0.7};
			colorDisabled[] = {1,1,1,0.4};
			
			soundEnter[] = {"\A3\ui_f\data\sound\RscButtonMenu\soundEnter",0.09,1};
			soundPush[] = {"\A3\ui_f\data\sound\RscButtonMenu\soundPush",0.09,1};
			soundClick[] = {"\A3\ui_f\data\sound\RscButtonMenu\soundClick",0.09,1};
			soundEscape[] = {"\A3\ui_f\data\sound\RscButtonMenu\soundEscape",0.09,1};
			
			font = "PuristaMedium";
			sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
			tooltipColorText[] = {1,1,1,1};
			tooltipColorBox[] = {1,1,1,1};
			tooltipColorShade[] = {0,0,0,0.65};
		};
		class meu_LOFav: meu_LOXClose_RscActiveText_base
		{
			idc = MEU_CTRL_FAVORITE;
			text = ICON_FAVORITE;
			tooltip = "Favorite Selected Loadout.";
			action = "";
			
			x = -1.45 * GUI_GRID_W + GUI_GRID_X;
			y = 6.35 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.2 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			
			onMouseButtonClick = STR_CALL_FUNC("buttons",_this);
		};
		class meu_LODeleteIcon: meu_LOXClose_RscActiveText_base
		{
			idc = MEU_CTRL_DELETEICON;
			text = ICON_DELETE;
			tooltip = "Delete Selected Loadout.";
			action = "";
			
			x = -1.45 * GUI_GRID_W + GUI_GRID_X;
			y = 7.45 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.2 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			
			onMouseButtonClick = STR_CALL_FUNC("buttons",_this);
		};
		class meu_LORenameIcon: meu_LOXClose_RscActiveText_base
		{
			idc = MEU_CTRL_RENAMEICON;
			text = ICON_RENAME;
			tooltip = "Rename Selected Loadout.";
			action = "";
			
			x = -1.45 * GUI_GRID_W + GUI_GRID_X;
			y = 8.65 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.2 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			
			onMouseButtonClick = STR_CALL_FUNC("buttons",_this);
		};
		class meu_LOSettingsIcon: meu_LOXClose_RscActiveText_base
		{
			idc = MEU_CTRL_SETTINGSICON;
			text = ICON_SETTINGS;
			tooltip = "Open Settings Panel.";
			action = "";
			
			x = -1.5 * GUI_GRID_W + GUI_GRID_X;
			y = 9.85 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.4 * GUI_GRID_W;
			h = 1.1 * GUI_GRID_H;
			
			onMouseButtonClick = STR_CALL_FUNC("buttons",_this);
		};
		class meu_LOSaveIcon: meu_LOXClose_RscActiveText_base
		{
			idc = MEU_CTRL_SAVEICON;
			text = ICON_SAVE;
			tooltip = "Save Current Loadout.";
			action = "";
			
			x = -1.45 * GUI_GRID_W + GUI_GRID_X;
			y = 5.25 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.2 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			
			onMouseButtonClick = STR_CALL_FUNC("buttons",_this);
		};
		class meu_LOLoadIcon: meu_LOXClose_RscActiveText_base
		{
			idc = MEU_CTRL_LOADICON;
			text = ICON_LOAD;
			tooltip = "Load Selected Loadout.";
			action = STR_CALL_FUNC("loadSaved",[]);
			
			x = -1.45 * GUI_GRID_W + GUI_GRID_X;
			y = 4.2 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.3 * GUI_GRID_W;
			h = 1.1 * GUI_GRID_H;
		};
		class meu_LOGearIcon: meu_LOXClose_RscActiveText_base
		{
			idc = MEU_CTRL_INVICON;
			text = ICON_CRATE;
			tooltip = "Open Crate Inventory";
			action = "";
			
			x = -1.55 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.4 * GUI_GRID_W;
			h = 1.1 * GUI_GRID_H;
			
			onMouseButtonClick = STR_CALL_FUNC("buttons",_this);
		};
		class meu_LOExpandIcon: meu_LOXClose_RscActiveText_base
		{
			idc = MEU_CTRL_EXPANDICON;
			text = ICON_EXPAND;
			tooltip = "Expand/Collapse";
			action = "";
			
			x = 36.9 * GUI_GRID_W + GUI_GRID_X;
			y = 4 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.4 * GUI_GRID_W;
			h = 1.1 * GUI_GRID_H;
			
			onMouseButtonClick = STR_CALL_FUNC("buttons",_this);
//			onLoad = "(_this select 0) ctrlSetText (['\meu_ammoBoxes\loadoutManager\icons\expandedTexture_ca.paa','\meu_ammoBoxes\loadoutManager\icons\hiddenTexture_ca.paa'] select ((profileNamespace getVariable ['meu_Lo_settings',[0,0,0]]) select 2))";
			onLoad = "(_this select 0) ctrlSetText (['\meu_ammoBoxes\loadoutManager\icons\expandedTexture_ca.paa','\meu_ammoBoxes\loadoutManager\icons\hiddenTexture_ca.paa'] select ((missionNamespace getVariable ['meu_Lo_settings',[0,0,0]]) select 2))";
		};
		////////////////////////////
		//// SAVING CONTROLS
		//////////////////////////// 
		class meu_LOSaveBG: meu_LOBoxPic
		{
			idc = MEU_CTRL_SAVEBG;
			x = 6 * GUI_GRID_W + GUI_GRID_X; 
			y = 10.95 * GUI_GRID_H + GUI_GRID_Y;
			w = 28 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			colorBackground[] = {0,0,0,1};
			NO_SHOW
		};
		class meu_LOSaveEdit_RscEdit_base // base class RscEdit
		{
			idc = MEU_CTRL_SAVEEDIT;
			text = "";
			x = 7 * GUI_GRID_W + GUI_GRID_X;
			y = 12 * GUI_GRID_H + GUI_GRID_Y;
			w = 13.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
			sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 32) * 1)";
			colorText[] = {1,0.5,0,1}; 
			// return: 28 & num enter: 156
			onKeyDown = "if ( _this select 1 in [28,156] ) then { private _null = ['buttons',_this] call meu_fnc_manager; false };";
			
			deletable = 0;
			fade = 0;
			access = 0;
			type = 2;
			colorBackground[] = {0,0,0,1};
			colorDisabled[] = {1,1,1,0.25};
			colorSelection[] = 
			{
				"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])",
				1
			};
			autocomplete = "";
			style = "0x00 + 0x40";
			font = "PuristaMedium";
			shadow = 2;
			canModify = 1;
			tooltipColorText[] = {1,1,1,1};
			tooltipColorBox[] = {1,1,1,1};
			tooltipColorShade[] = {0,0,0,0.65};
			NO_SHOW
		};	
		class meu_LOSaveSave: ctrlButton
		{
			idc = MEU_CTRL_SAVESAVE;
			x = 22 * GUI_GRID_W + GUI_GRID_X;
			y = 12.25 * GUI_GRID_H + GUI_GRID_Y;
			h = "4.5 * (pixelH * pixelGrid * 	0.5)";
			w = "18 * (pixelW * pixelGrid * 	0.5)";
			text = "SAVE";
			NO_SHOW
			onMouseButtonClick = STR_CALL_FUNC("buttons",_this);
		};
		class meu_LOSaveRename: meu_LOSaveSave
		{
			idc = MEU_CTRL_SAVERENAME;
			NO_SHOW
		};
		class meu_LOSaveCancel: meu_LOSaveSave
		{
			idc = MEU_CTRL_SAVECANCEL;
			x = 28 * GUI_GRID_W + GUI_GRID_X;
			text = "CANCEL";
			NO_SHOW
		};
		class meu_LOSaveText: meu_LOTextHeader_RscText_base
		{
			idc = MEU_CTRL_SAVETEXT;
            x = 7 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
            w = 18 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
			text = "Loadout Name";
			shadow = 0;
//			SizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 35) * 1)";
			sizeEx = "3 * (1 / (getResolution select 3)) * pixelGrid * 0.5";
			NO_SHOW
		};
		class meu_LOSaveClear: meu_LOXClose_RscActiveText_base
		{
			idc = MEU_CTRL_SAVECLEAR;
			text = "\A3\Ui_f\data\GUI\Rsc\RscDisplayArcadeMap\icon_exit_cross_ca.paa";
			tooltip = "Clear";
			action = "((uiNamespace getVariable 'meu_managerGUI') displayCtrl  15061) ctrlSetText ''; ctrlSetFocus ((uiNamespace getVariable 'meu_managerGUI') displayCtrl  15061);";
			
			x = 6.3 * GUI_GRID_W + GUI_GRID_X;
			y = 12.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 0.57 * GUI_GRID_W;
			h = 0.525 * GUI_GRID_H;
			NO_SHOW
		};
		////////////////////////////
		//// SETTINGS BOXES/ICONS/BUTTONS
		////////////////////////////
		class meu_LOBoxSettings: meu_LOBoxPic
		{
			idc = MEU_CTRL_PREFBGFRAME;
			x = 0 * GUI_GRID_W + GUI_GRID_X; 
			y = 8 * GUI_GRID_H + GUI_GRID_Y;
			w = 39 * GUI_GRID_W;
			h = 10 * GUI_GRID_H;
			colorBackground[] = MAIN_BG_COLOR;
			NO_SHOW
		};
		class meu_LOAutoCloseBox_RscCheckBox_base: ctrlCheckbox // base class RscCheckBox
		{	
			// ctrlCheckbox base class required to get onCheckedChanged to work here
			idc = MEU_CTRL_PREFAUTOBOX;
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = 10 * GUI_GRID_H + GUI_GRID_Y;
/* 			h = "5 * (pixelH * pixelGrid * 	0.25)";
			w = "5 * (pixelW * pixelGrid * 	0.25)";    */	
			h = "5 * (pixelH * pixelGrid * 	0.5)";
			w = "5 * (pixelW * pixelGrid * 	0.5)";  
			color[] = {1,1,1,1};	
//			checked = "(profileNamespace getVariable ['meu_Lo_settings',[0]]) select 0";
			checked = "(missionNamespace getVariable ['meu_Lo_settings',[0]]) select 0";
			onCheckedChanged = "['settings',['save','autoclose',_this select 1]] call meu_fnc_manager;";
			NO_SHOW
		};
		class meu_LOHideDefaultBox: meu_LOAutoCloseBox_RscCheckBox_base
		{
			idc = MEU_CTRL_PREFHIDEBOX;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
//			checked = "(profileNamespace getVariable ['meu_Lo_settings',[0,0]]) select 1";
			checked = "(missionNamespace getVariable ['meu_Lo_settings',[0,0]]) select 1";
			onCheckedChanged = "0 = ['settings',['save','hidedefaults',_this select 1]] call meu_fnc_manager; missionNamespace setVariable ['meu_boxDefaults',([true,false] select (_this select 1))];";
			NO_SHOW
		};
		class meu_LOIconCycleTips: meu_LOXClose_RscActiveText_base
		{
			idc = MEU_CTRL_CYCLETIPS;
			text = ICON_CYCLETIPS;
			tooltip = "Next Tip";
			action = ""; // having issues with quotations - scripted EH instead
			
			x = 0.5 * GUI_GRID_W + GUI_GRID_X;
			y = 16.5 * GUI_GRID_H + GUI_GRID_Y;
			h = "4.5 * (pixelH * pixelGrid * 	0.5)";
			w = "4.5 * (pixelW * pixelGrid * 	0.5)";
			NO_SHOW
		};
		class meu_LOButtonExport: ctrlButton
		{
			idc = MEU_CTRL_PREFBUTEXPORT;
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = 13.2 * GUI_GRID_H + GUI_GRID_Y;
			h = "5 * (pixelH * pixelGrid * 	0.5)";//.25
			w = "20 * (pixelW * pixelGrid * 	0.5)";
			text = "Export";
			tooltip = "Export Loadout And Settings To Clipboard.";
//			sizeEx = "4.32 * (1 / (getResolution select 3)) * pixelGrid * 0.25";
			action = "['settings',['export']] call meu_fnc_manager;";
			NO_SHOW
		};
		class meu_LOButtonImport: meu_LOButtonExport
		{
			idc = MEU_CTRL_PREFBUTIMPORT;
			x = 7 * GUI_GRID_W + GUI_GRID_X;
			text = "Import";
			tooltip = "Import Loadout And Settings From Clipboard.";
			action = "['settings',['import']] call meu_fnc_manager;";
			NO_SHOW
		};
		////////////////////////////
		//// SETTINGS TEXT
		////////////////////////////
		class meu_LOTextSettingsHeader: meu_LOTextHeader_RscText_base
		{
			idc = MEU_CTRL_PREFHEADERTEXT;
			x = 0.2 * GUI_GRID_W + GUI_GRID_X;
			y = 8.2 * GUI_GRID_H + GUI_GRID_Y;
			w = 10 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			text = "Preferences";
			sizeEx = "3.6 * (1 / (getResolution select 3)) * pixelGrid * 0.5";
			NO_SHOW
		}; 
		class meu_LOTextAutoClose: meu_LOTextHeader_RscText_base
		{
			idc = MEU_CTRL_PREFAUTOTEXT;
			x = 2.7 * GUI_GRID_W + GUI_GRID_X;
			y = 10 * GUI_GRID_H + GUI_GRID_Y; //1
			w = 10 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			text = "Auto Close On Load";
			tooltip = "Closes The LOM When Applying A Saved Or Default Loadout.";
			sizeEx = "3 * (1 / (getResolution select 3)) * pixelGrid * 0.5"; //0.25
			NO_SHOW
		};
		class meu_LOTextHideDefaults: meu_LOTextAutoClose
		{
			idc = MEU_CTRL_PREFHIDETEXT;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			text = "Hide Default Loadouts";
			tooltip = "Hide Defaults From Crate Scroll Actions. Still Available In the LOM.";
			NO_SHOW
		};
		class meu_LOTextTipsScroll: meu_LOTextHeader_RscText_base
		{
			idc = MEU_CTRL_PREFTIPSTEXT;
			x = 2 * GUI_GRID_W + GUI_GRID_X;
			y = 16.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 37 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			text = "Tips Here";
			sizeEx = "3 * (1 / (getResolution select 3)) * pixelGrid * 0.5";
			NO_SHOW
		};	
	};
};

class cfgHints
{
	class meu_manager
	{
		displayname = "[15th] Ammo Boxes";
		class meu_usage
		{
			displayName = "15th Loadout Manager 2";
			displayNameShort = "Get Your Shit Quickly.";
			description = "15th AmmoBoxes come with a loadout manager. There are a few things you should know.%1%1Make your loadouts exactly how you want them. Including your radio frequencies.%1%1%3CTRL + Double Click%4 - Rename Loadout.%1%3SHFT + Double Click%4 - Add to favorites.%1%3CTRL + Click Save%4 - Quick save loadout with the same name%1%3DEL%4 - Delete Loadout.%1%3Gear Key%4 - Switch To Inventory Screen.%1%3Help Key%4 - Open Field Manual.%1%3CTRL + S%4 - Export Loadout in Script Format.%1%1Enjoy!";
			tip = "Please report any problems on the forums.";
			arguments[] = {};
			image = "\meu_ammoBoxes\loadoutManager\logo256.paa";
			noImage = false;
		};
		class meu_other
		{
			displayName = "Other Info";
			displayNameShort = "";
			description = "Other Info:%1- You Can Change Some Settings in Eden Attributes Window.%1- Packed weapons have thier attachments saved but loaded seperately.%1- To save loaded designator batteries, have an extra in your inventory.%1- GUI colors are set from your Arma profile.%1- Mission Makers can disabled the ""gear in box"" check with %3box setVariable [""meu_boxRestrictions"",false]%4%1- You can add the LOM to any object with %3[""init"",[_crate]] call meu_fnc_manager;%4";
			tip = "Riouken Created The Original Loadout Manager.";
			arguments[] = {};
			image = "\meu_ammoBoxes\loadoutManager\logo256.paa";
			noImage = false;
		};
		class meu_export: meu_other
		{
			displayName = "Import/Export"
			description = "Version 2.5 includes the ability to export and import your loadouts and settings between Arma profiles. This is particularly useful for when you get promoted and make a new profile name.%1%1%3Export:%4 Open the Preferences pane in the LOM and click the EXPORT button. Exit the mission and switch Arma profiles or name a new one. !BE CAREFUL NOT TO OVERWRITE THE CLIPBOARD!%1%3Import:%4 On your new profile, make a mission with an ammobox. Enter the LOM and open the Preferences pane. The IMPORT button should show enabled if the data you exported is correct. Select IMPORT and then select CONFIRM.%1%1 Thats it!";
			tip = "Hit CTRL + S To Export In Script Format.";
			
		};
	};
};
