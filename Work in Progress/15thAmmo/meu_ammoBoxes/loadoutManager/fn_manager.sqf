// 15th loadout manager 2.5
// fight9
// built on Riouken's framework
// version date: 7/30/16

#include "manager_macros.hpp"

private ["_fnc","_params"];
disableSerialization;
_fnc = param [0,"init",[""]];
_params = param [1,[]];
_display = uiNamespace getVariable ["meu_managerGUI",displayNull];
_r = false;

switch (toLower _fnc) do {
	
	case "init": {
		// add action to box
		private ["_box","_id","_name","_favs"];
		_box = _params param [0,objNull,[objNull]];
		
		// main action
		_name = format ["<t color='%1'>%2</t>","#85a2af",ACTION_NAME];
		_id = _box addAction [_name,{ CALL_FNC("open",_this select 3); },[_box],6,true,true,"",ACTION_CONDITION];
		_box setVariable ["meu_loadoutManager",true];
		
		// get favorites list & add them to box
		_favs = profileNamespace getVariable ["meu_Lo_favs",[]];
		{ 
			private "_blah"; 
			_blah = [_x,[_box]]; 
			SPAWN_FNC("favorites",_blah); 
		} forEach _favs;
						
		// load settings to missionnamespace variable
		private _settings = profileNamespace getVariable ["meu_Lo_settings",[0,0,0,0]];
		if (count _settings < 4) then {
			for "_i" from (count _settings) to 3 do {
				_settings set [_i,0];
			};
		};		
		missionNamespace setVariable ["meu_Lo_settings",_settings];
		
		// execute default spawn loadout - once
		if (isNil {missionNamespace getVariable "meu_Lo_init"}) then {
			private _spawn = profileNamespace getVariable ["meu_Lo_spawn",[]];
			if ( count _spawn > 0 && {_spawn select 0 > 0} ) then {
				if (_spawn select 1 isEqualTo typeOf _box) then {
					private _run = ["loadsaved",[_spawn select 0,_box,true]] spawn meu_fnc_manager;
					missionNamespace setVariable ["meu_Lo_init",true];
				};
			};
		};
		
		_r = _id;
	};
	
	case "open": {
		// open GUI & populate
		private ["_box","_diag","_display","_load"];
		// params
		_box = _params param [0,objNull,[objNull]];
		missionNamespace setVariable ["meu_managerBOX",_box];
		
		// diag
		_diag = createDialog "meu_loadoutManager";
		disableSerialization;		
		_display = uiNamespace getVariable ["meu_managerGUI",displayNull];
		
		// populate	
		GUI_REFRESH(-1)
		private _def = CALL_FNC("listDefault",[]);

		// cycle tips button - has issues doing this via config
		CTRL(MEU_CTRL_CYCLETIPS) ctrlSetEventHandler ["MouseButtonClick",
 			format [
				'((uiNamespace getVariable "meu_managerGUI") displayCtrl %1) ctrlSetText (selectRandom %2)',
				MEU_CTRL_PREFTIPSTEXT,
				MEU_HELP_TIPS
			] 
		];
		
		if (missionNamespace getVariable ["meu_Lo_settings",[0,0,0,0]] select 3 isEqualTo 0) then { playSound "sectorCaptured"; };
		
		_r = _diag;
	};
	
	case "listgear": { 
		// add gear to tree
		private ["_tree","_gear","_expand","_fnc_tvParentAdd","_fnc_tvChildAdd","_fnc_tvParantAndChild","_fnc_picture","_checking"];
		_tree = _params param [0,CTRL(MEU_CTRL_GEARTREE)];
		_gear = _params param [1,CALL_FNC("playerGear",[])];
		private _header = "Current Gear";
		
		// if there is a saved list selection, show that gear
		if (lbCurSel CTRL(MEU_CTRL_SAVEDLIST) > 0) then {
			_gear = call {
				private _index = parseNumber (CTRL(MEU_CTRL_SAVEDLIST) lbData (lbCurSel CTRL(MEU_CTRL_SAVEDLIST)));
				private _variable = format ["meu_Lo_%1",_index];	
				private _array = profileNamespace getVariable [_variable,[]];
				if CHECK_LOADOUT(_array) exitWith {_gear};
				_header = "Saved Gear: " + (_array select 0); // set header to loadout name
				_array select 1;
			};
		};
		
		// get box gear for preview
		_boxGear = CALL_FNC("boxGear",[]);
		
		// if current gear list, then expand trees.. also controls _fnc_picture's X's 
//		_expand = _header isEqualTo "Current Gear";
//		_expand = [false,true] select ((profileNamespace getVariable ['meu_Lo_settings',[0,0,0]]) select 2);
		_expand = [false,true] select ((missionNamespace getVariable ['meu_Lo_settings',[0,0,0,0]]) select 2);
		_checking = meu_managerBOX getVariable ["meu_boxRestrictions",true];
		tvClear _tree;
		CTRL(MEU_CTRL_GEARTEXT) ctrlSetText _header;
		
		_fnc_picture = {
			// if showing saved loadout, only show picture if item is in the box
			GET_PARENT(_this);
			if CHECK_IS_RADIO exitWith {(getText (configFile >> _config >> _this >> "picture"))};
			if (_this isEqualTo "Assigned Items") exitWith {getText (configFile >> "CfgWeapons" >> "itemGPS" >> "picture")};
			if (/* !_expand && { */_checking/* } */ && {!(_this in _boxGear)}) exitWith {ICON_NOTINBOX};
			(getText (configFile >> _config >> _this >> "picture"))
		};	
		_fnc_tvParantAndChild = {
			// checks for gear before running parent and child fncs
			private ["_parentGear","_childGear"];
			params ["_parentGear","_childGear",["_noHeadgear",""]];
			if (_parentGear isEqualTo "" && !(_noHeadgear isEqualTo "")) then {_parentGear = _noHeadgear;};
			if (_parentGear != "") then {
				private ["_parent","_child"];
				_parent = [_parentGear] call _fnc_tvParentAdd;
				if (_parent isEqualTo false) exitWith {false};
				if (count _childGear > 0) then {
					_child = [_parent,_childGear] call _fnc_tvChildAdd;
				};
			};
			true
		};
		_fnc_tvParentAdd = {
			// adds to tree and returns parent 
			params ["_gear"];
			private _config = CALL_FNC("config",[_gear]);
			if (_config isEqualTo "" && {_gear != "Assigned Items"}) exitWith {false};
			private _name = if (_gear isEqualTo "Assigned Items") then [{_gear},{getText (configFile >> _config >> _gear >> "displayname")}];
			private _parent = _tree tvAdd [ [], _name ];
			_tree tvSetPicture [ [_parent], _gear call _fnc_picture ];
			if !_expand then { //_expand
				_tree tvExpand [_parent];
			};
			_parent
		};
		_fnc_tvChildAdd = {
			// loops through child gear adding to parent
			params ["_parent","_gear"];
			{
				if (_x != "") then {
					private ["_config","_child"];
					_config = CALL_FNC("config",[_x]);
					if !(_config isEqualTo "") then {
						_child = _tree tvAdd [ [_parent], getText (configFile >> _config >> _x >> "displayname") ];
						_tree tvSetPicture [ [_parent,_child], _x call _fnc_picture ];
					};
				};
			} forEach _gear;
			true
		};
		// list goggles with assigned
		_assigned = +GEAR(6);
		_assigned pushBack GEAR(1);
		// add to tree		
		{ _x call _fnc_tvParantAndChild; } forEach [
			[_gear select 9,_gear select 10], 	// primary, Items
			[_gear select 13,_gear select 14], 	// handgun, items
			[_gear select 11,_gear select 12], 	// launcher, item
			[_gear select 2,_gear select 3], 	// uniform, items
			[_gear select 4,_gear select 5], 	// vest, items
			[_gear select 7,_gear select 8], 	// pack, items
			[_gear select 0,_assigned,"Assigned Items"]	// headgear, assigned+goggles
		];
		// list freqs
		if (TFAR_CHECK) then {
			if (count _gear > 15) then {
				private ["_parent","_childA","_childB"];
				_parent = _tree tvAdd [ [], "Radio Presets" ];
				_tree tvSetPicture [ [_parent],"\A3\Weapons_F\Data\UI\gear_item_radio_ca.paa" ];
				if CHECK_COUNT(15) then {
					_childA = _tree tvAdd [ [_parent], "Short Range" ];
					{
						private "_gChild";
						_gChild = _tree tvAdd [ [_parent,_childA], _x ];
					} forEach GEAR(15);
				};
				if CHECK_COUNT(16) then {
					_childB = _tree tvAdd [ [_parent], "Long Range" ];
					{
						private "_gChild";
						_gChild = _tree tvAdd [ [_parent,_childb], _x ];
					} forEach GEAR(16);
				};
			};
		};
		
		_r = true;
	};
	
	case "listsaved": {
		// list saved loadouts
		private ["_savedList","_index","_favs"];
		_index = _params param [0,lbCurSel CTRL(MEU_CTRL_SAVEDLIST)];
		_savedList = CTRL(MEU_CTRL_SAVEDLIST);
		// loop through vars, get names & add to array
		meuLoadOuts = [];
		for "_num" from 1 to 20 do {
			call compile format ["
				meu_Lo_%1 = profileNamespace getVariable [""meu_Lo_%1"",[]];
				if ((count meu_Lo_%1) == 0) then {
					profileNamespace setVariable [""meu_Lo_%1"",[""Empty Loadout %1""]];
					meu_Lo_%1 = profileNamespace getVariable [""meu_Lo_%1"",[]]; 
				};
				meuLoadOuts pushBack meu_LO_%1;
				",
				_num
			];		
		};
		_favs = profileNamespace getVariable ["meu_LO_favs",[]];
		private _spawn = profileNamespace getVariable ["meu_Lo_spawn",[-1,""]];
		// clear, remove selected, reset text color
		lbClear _savedList;
		_savedList lbSetCurSel -1; 
		_savedList ctrlSetTextColor (COLOR_WARNING_ARRAY);

		// get empty loadouts first
		meuEmptyLoadouts = [];
		{
			if CHECK_LOADOUT(_x) then { meuEmptyLoadouts pushBack (_forEachIndex + 1); };
		} foreach meuLoadOuts;
		// add greyed new to top of list
		if (count meuEmptyLoadouts > 0) then {
			private _index = _savedList lbAdd " New Loadout";
			_savedList lbSetColor [_index,[1,1,1,0.25]];
			_savedList lbSetData [_index,str ( meuEmptyLoadouts select 0 )];
		}; 
		// add saved loadouts
		{
			private _num = _forEachIndex + 1;
			if !CHECK_LOADOUT(_x) then {
				private _tooltip = "";
				private _name = _x select 0;
				if (count _name > 23) then { // add dots to long name and set tooltip
					_tooltip = _name;
					_name = (_name select [0,20]) + "...";
				};
//				private _index = _savedList lbAdd (_x select 0);
				private _index = _savedList lbAdd _name;
				_savedList lbSetData [_index,str _num];
				_savedList lbSetToolTip [_index,_tooltip];
				if (_num in _favs) then {
					_savedList lbSetPictureRight [_index,ICON_FAVORITE];
				};
				if (_num in _spawn) then {
					_savedList lbSetPicture [_index,ICON_SPAWN];
				};
			};	
		} forEach meuLoadOuts;
		CTRL(MEU_CTRL_SAVEDTEXT) ctrlSetText format ["Saved Loadouts (%1/20 Available)",count meuEmptyLoadouts];
		lbSort _savedList;
		
		// set selected again when refreshed
		if (_index > -1) then {
			_savedList lbSetCurSel _index;
		};
		_r = true;
	};
	
	case "listdefault": {
		// list configured default loadouts
		private ["_defaultList","_cfg"];
		
		_defaultList = CTRL(MEU_CTRL_DEFAULTLIST);
		lbClear _defaultList;
		_defaultList lbSetCurSel -1;
		// get configured loadouts
		_cfg = configFile >> "CfgVehicles" >> typeOf meu_managerBOX;
		private _disabled = meu_managerBOX getVariable ["meu_boxDefaults",false];
		if ( (isArray (_cfg >> "MEU_LOADOUTS")) && {!_disabled}) then {
			private "_array";
			_array = getArray (_cfg >> "MEU_LOADOUTS");
			// add names to listBox
			{
				private "_index";	// remove "loadout" from name
				_index = _defaultList lbAdd STR_REPLACE(_x select 0,"Loadout","");
				_defaultList lbSetData [_index,_x select 1];
			} forEach _array;
			lbSort _defaultList;
		} else {
			// no loadouts, disable ctrls
			_defaultList lbAdd "No Defaults";
			_defaultList ctrlEnable false;
		};
				
		_r = true;
	};
	
	case "playergear": {
		// return all player gear
		private["_meu_xtendMag","_meu_headgear","_meu_goggles","_meu_uniform","_meu_uniformitems","_meu_vest","_meu_vestitems","_meu_asgnItems","_meu_backpack","_meu_packitems","_meu_PrimaryGun","_meu_pgunitems","_meu_SecondaryGun","_meu_sgunitems","_meu_handgun","_meu_handgunitems","_meu_swFreqs","_meu_lrFreqs"];
		_r = [];
		
		_meu_headgear = headgear player;	
		_meu_goggles = Goggles player;
		_meu_uniform = uniform player;
		_meu_uniformitems = uniformItems player;
		_meu_vest = vest player;
		_meu_vestitems = vestItems player;
		_meu_asgnItems = assignedItems player;		
		_meu_backpack = backpack player;
		_meu_packitems = backpackItems player;		
		_meu_PrimaryGun = primaryWeapon player;
		_meu_pgunitems = primaryWeaponItems player;
		_meu_SecondaryGun = secondaryWeapon player;
		_meu_sgunitems = secondaryWeaponItems player;				
		_meu_handgun = handgunWeapon player;
		_meu_handgunitems = handgunItems player;
		
		// add loaded magazines as weapon items
		_meu_xtendMag = magazinesAmmoFull player;
		{
			switch (toLower (_x select 4)) do {
				case (toLower _meu_PrimaryGun): {_meu_pgunitems pushBack (_x select 0);};
				case (toLower _meu_SecondaryGun): {_meu_sgunitems pushBack (_x select 0);};
				case (toLower _meu_handgun): {_meu_handgunitems pushBack (_x select 0);};
				default {};
			};	
		} forEach _meu_xtendMag;
		
		// if there is a laser designator in assigned items
		if ( { 
				([(configFile >> (CALL_FNC("config",[_x])) >> _x),"optics",0] call BIS_fnc_returnConfigEntry) > 0
			} count _meu_asgnItems > 0		
		) then {
			// add one extra battery to container if it has one already - compensates for lost loaded one
			{ 
				if ("Laserbatteries" in _x) exitWith { _x pushBack "Laserbatteries"; };
			} forEach [_meu_uniformitems,_meu_vestitems,_meu_packitems];
		};
		
		// if has installed earbuds 
		if (MEU_EARBUD_CHECK) then { _meu_uniformitems pushBack MEU_CLASS_EARBUDS };
		
		// get weapon items from packed weapons
		private "_packWeps";
		_packWeps = weaponsItemsCargo (backpackContainer player);
		if !(isNil "_packWeps") then {
			{	
				for "_i" from 1 to (count _x - 1) do {
					private "_item";
					_item = if ( CHECK_TYPE((_x select _i),[]) ) then {
						if ( count (_x select _i) != 0 ) then [{(_x select _i) select 0},{""}]
					} else {
						_x select _i
					};
					if (_item != "") then { _meu_packitems pushBack _item };
				};
			} forEach _packWeps;
		};
		
		// get radio frequencies
		_meu_swFreqs = [];
		_meu_lrFreqs = [];
		if (TFAR_CHECK) then {
			if (call TFAR_fnc_haveSWRadio) then {
				_swSet = (call TFAR_fnc_activeSwRadio) call TFAR_fnc_getSwSettings;
				if !(isNil "_swSet") then {
					_meu_swFreqs = _swSet select 2;
				};
			};
			if (call TFAR_fnc_haveLRRadio) then {
				_lrSet = (call TFAR_fnc_activeLrRadio) call TFAR_fnc_getLrSettings;
				if !(isNil "_lrSet") then {
					_meu_lrFreqs = _lrSet select 2;
				};
			};
		};
		
		// check for RHS grip sorcery
		if ( ((toLower _meu_PrimaryGun) find "rhs" > -1) && ((toLower _meu_PrimaryGun) find "_grip" > -1) ) then {		
			private _base = getText (configFile >> "CfgWeapons" >> _meu_PrimaryGun >> "baseWeapon");
			if !(_base isEqualto "") then {
				_meu_PrimaryGun = _base;
			};
		};

		_r = [_meu_headgear,_meu_goggles,_meu_uniform,_meu_uniformitems,_meu_vest,_meu_vestitems,_meu_asgnItems,_meu_backpack,_meu_packitems,_meu_PrimaryGun,_meu_pgunitems,_meu_SecondaryGun,_meu_sgunitems,_meu_handgun,_meu_handgunitems,_meu_swFreqs,_meu_lrFreqs];
		
	};
	
	case "boxgear": {
		// return all gear in box
		private ["_box", "_mags","_weps","_items","_rucks","_allGear"];
	
		_box = meu_managerBOX;
		_mags = getMagazineCargo _box;
		_weps = getWeaponCargo _box;
		_items = getItemCargo _box;
		_rucks = getBackpackCargo _box;
		_allGear = [] + (_mags select 0) + (_weps select 0) + (_items select 0) + (_rucks select 0);
		_allGear append ALLOWED_GOGGLES; // add goggles to box items
		_r = _allGear
	};
	
	case "buttons": {
		// handle GUI buttons per IDC
		private ["_button","_return"];
		_button = _params param [0,controlNull];
		_return = false;
				
		switch (ctrlIDC _button) do {
									
			// main save button
			case MEU_CTRL_SAVEICON: {
				private "_heldCtrl";
				_heldCtrl = _params param [5,false];
				
				// get loadout index
				private _index = if true then {
					if (lbCurSel CTRL(MEU_CTRL_SAVEDLIST) >= 0) exitWith {parseNumber (CTRL(MEU_CTRL_SAVEDLIST) lbData (lbCurSel CTRL(MEU_CTRL_SAVEDLIST)))};
					if (count meuEmptyLoadouts > 0) exitWith {meuEmptyLoadouts select 0};
					if true exitWith {-1}; 
				};	
				// no current selection & no empty available
				if (_index < 0) exitWith {
					private _m = ["message",[WARN_TEXT("No Empty Slots Available. Please Select One To Replace And Re-Save."),[]]] call FUNCTION_NAME;
				};
				// get text from loadout array // -1 to account for zero based lO array pos
				private _text = (meuLoadOuts select (_index - 1)) select 0;	
				// determine text used in editbox and header text or warning
				private _empty = format ["Empty Loadout %1",_index];
				private _isEmpty = _text isEqualTo _empty; 
				if (_isEmpty) then {
					// add weapon name to end of default name
					private _class = if true then {
						if (primaryWeapon player != "") exitWith {primaryWeapon player};
						if (handgunWeapon player != "") exitWith {handgunWeapon player};
						if (secondaryWeapon player != "") exitWith {secondaryWeapon player};
						""
					};
					if (_class != "") then {
						private _config = CALL_FNC("config",[_class]);
						private _name = if (_config isEqualTo "") exitWith {};
						_text = getText (configFile >> _config >> _class >> "displayname");
					} else {
						_text = format ["Loadout %1",20 - (count meuEmptyLoadouts - 1)];
					};				
				};								
				// set text to editbox
				CTRL(MEU_CTRL_SAVEEDIT) ctrlSetText _text;
				// if ctrl is held, skip ctrlShow & call save action
				if _heldCtrl exitWith { 
					private "_quick";
					_quick = ["buttons",[CTRL(MEU_CTRL_SAVESAVE)]] call FUNCTION_NAME; 
					_return = true;
				};
				// set control header text
				CTRL(MEU_CTRL_SAVETEXT) ctrlSetText (["WARNING: Will Overwrite Existing Loadout","Loadout Name"] select _isEmpty);
				CTRL(MEU_CTRL_SAVETEXT) ctrlSetTextColor ([[0.99,0.22,0.06,0.9],[1,1,1,1]] select _isEmpty);
				// show or disable groups
				{ CTRL(_x) ctrlShow true } forEach MEU_SAVE_GROUP;
				CTRL(MEU_CTRL_SAVERENAME) ctrlShow false; // disabled rename save
				{ CTRL(_x) ctrlEnable false } forEach MEU_DISABLE_GROUP;
				CTRL(MEU_CTRL_GEARTREE) ctrlSetTextColor [1,1,1,0.25]; // disabled color - IS THIS CONFIG'D NOW?
				CTRL(MEU_CTRL_GEARTREE) tvSetCurSel [-1]; // selection doesnt change to disabled color
				{ CTRL(_x) ctrlEnable false } forEach (MEU_MENU_GROUP + [MEU_CTRL_SETTINGSICON]); // add settings
				
				ctrlSetFocus CTRL(MEU_CTRL_SAVEEDIT);
				
				_return = true;
					
			};
			
			// save cancel button
			case MEU_CTRL_SAVECANCEL: { 
				// close save controls			
				{ CTRL(_x) ctrlShow false } forEach MEU_SAVE_GROUP;
				{ CTRL(_x) ctrlEnable true } forEach MEU_DISABLE_GROUP;
				CTRL(MEU_CTRL_GEARTREE) ctrlSetTextColor (COLOR_WARNING_ARRAY); // tree text color
				{ CTRL(_x) ctrlEnable true } forEach (MEU_MENU_GROUP + [MEU_CTRL_SETTINGSICON]); // add settings
				_return = false;
			};	
			
			// 'real' save button/ pressing enter
			case MEU_CTRL_SAVEEDIT; 
			case MEU_CTRL_SAVESAVE: { 
				// get ctrl text and save to profile	
				
				// set focus away from close button
				ctrlSetFocus CTRL(MEU_CTRL_GEARTREE); // stop close button getting hit
				private _listPos = lbCurSel CTRL(MEU_CTRL_SAVEDLIST);
				
				// if rename save button is shown
				if (ctrlShown CTRL(MEU_CTRL_SAVERENAME)) exitWith {
					private "_rename";
					_rename =["buttons",[CTRL(MEU_CTRL_SAVERENAME)]] call FUNCTION_NAME; 
					_return = true;
				};	
				
				// get loadout index
				private _index = if true then {
					if (lbCurSel CTRL(MEU_CTRL_SAVEDLIST) >= 0) exitWith {parseNumber ( CTRL(MEU_CTRL_SAVEDLIST) lbData (lbCurSel CTRL(MEU_CTRL_SAVEDLIST) ))};
					if (count meuEmptyLoadouts > 0) exitWith {meuEmptyLoadouts select 0};
					if true exitWith {-1}; 
				};	
				if (_index < 0) exitWith {
					private _m = ["message",[WARN_TEXT("No Empty Slots Available. Please Select One To Replace And Re-Save."),[]]] call FUNCTION_NAME;
				};
				
				// get loadout variable
				private _variable = format ["meu_Lo_%1",_index];
				private _empty = format ["Empty Loadout %1",_index];
				
				// get text from editbox
				_name = ctrlText CTRL(MEU_CTRL_SAVEEDIT);	
				if (_name == "") exitWith { _m = ["message",[WARN_TEXT("Please enter a name."),[]]] call FUNCTION_NAME; };
				
				// if they left default empty name, change it
				if (_name == _empty) then {
					_name = STR_REPLACE(_name,"Empty ",""); 
				};
				
				// get gear and create array
				private _gear = CALL_FNC("playerGear",[]);
				private _array = [_name,_gear];	
				
				// save
				profileNamespace setVariable [_variable,_array];
				saveProfileNamespace;	
				
				// refresh
				{ CTRL(_x) ctrlShow false } forEach MEU_SAVE_GROUP;
				{ CTRL(_x) ctrlEnable true } forEach MEU_DISABLE_GROUP;
				CTRL(MEU_CTRL_GEARTREE) ctrlSetTextColor (COLOR_WARNING_ARRAY); // tree text color
				{ CTRL(_x) ctrlEnable true } forEach (MEU_MENU_GROUP + [MEU_CTRL_SETTINGSICON]);
				
				GUI_REFRESH(_listPos)	
				// message
				private _m = ["message",[format ["Loadout was saved:<br/><br/>%1",WARN_TEXT(_name)],[]]] call FUNCTION_NAME;	
				_return = true;	
			};
			
			// main delete button
			case MEU_CTRL_DELETEICON: { 
				// delete selected loadout
				private _return = false;
				if ( lbCurSel CTRL(MEU_CTRL_SAVEDLIST) >= 0 ) then {
					private ["_index","_variable","_name","_old","_loadoutNo","_favs","_found"];
					// format new data
					_index = parseNumber (CTRL(MEU_CTRL_SAVEDLIST) lbData (lbCurSel CTRL(MEU_CTRL_SAVEDLIST)));
					_variable = format ["meu_Lo_%1",_index];	
					_old = (meuLoadOuts select (_index - 1)) select 0; // maybe move below check_loadout
					_name = format ["Empty Loadout %1",_index]; // maybe move below check_loadout
					
					private _array = profileNamespace getVariable [_variable,[]];
					if CHECK_LOADOUT(_array) exitWith {
						_m = ["message",[WARN_TEXT("The selected slot is empty."),[]]] call FUNCTION_NAME;
					};					
					// remove from favorites if found
					_loadoutNo = _index;
					_favs = profileNamespace getVariable ["meu_Lo_favs",[]];
					_found = _favs find _loadoutNo;
					if (_found > -1) then {
						private ["_blah","_removeFavs"];
						// if found, make Number negative, remove from array
						_loadoutNo = _loadoutNo * -1;
						_favs deleteAt _found;					
						// call fnc
						_blah = [_loadoutNo,(allMissionObjects "Reammobox_F")]; 
						_removeFavs = CALL_FNC("favorites",_blah);
						profileNamespace setVariable ["meu_Lo_favs",_favs];
					};					
					// save
					profileNamespace setVariable [_variable,[_name,[]]];
					saveProfileNamespace;
									
					_m = ["message",[format ["Loadout was deleted:<br/><br/>%1",ERROR_TEXT(_old)],[]]] call FUNCTION_NAME;				
					GUI_REFRESH(-1)
					_return = true;
				} else {
					_m = ["message",[WARN_TEXT("Please select a save slot."),[]]] call FUNCTION_NAME;
				};
				_r = _return;
			};
			
			// double clicking saved gear
			case MEU_CTRL_SAVEDLIST: {
				// TODO: consider incorperating into "laodsaved" instead of this subfunc
				private ["_ctrl","_heldCtrl","_heldShft"];
				_ctrl = _params param [0,CTRL(MEU_CTRL_SAVEDLIST)];
				_heldShft = _params param [4,false];
				_heldCtrl = _params param [5,false];
				
				// new functionality - quite convoluted now
				// load: double click >> "button"/savedlist >> "loadsaved"
				// save: Double click > "button"/savedlist >> "loadsaved" >> "button"/saveicon
				if (!_heldShft && !_heldCtrl) exitWith { 
					private _selection = lbCurSel CTRL(MEU_CTRL_SAVEDLIST);
					if (_selection >= 0) then {					
						private _load = CALL_FNC("loadsaved",[]);  
					};					
					_return = true;
				};
								
				// if SHIFT was held, exit with calling favorites fnc
				if _heldShft exitWith { 
					private "_quick";
					_quick = ["buttons",[CTRL(MEU_CTRL_FAVORITE)]] call FUNCTION_NAME; 
					_return = true;
				};
				
				// CTRL was held 
				if ( lbCurSel CTRL(MEU_CTRL_SAVEDLIST) >= 0 ) then {
					private ["_index","_variable","_name","_savedList"];
					// format new data
					_index = parseNumber (CTRL(MEU_CTRL_SAVEDLIST) lbData (lbCurSel CTRL(MEU_CTRL_SAVEDLIST)));
					_variable = format ["meu_Lo_%1",_index];						
					
					_array = profileNamespace getVariable [_variable,[]];
					// check
					if CHECK_LOADOUT(_array) exitWith {
						_m = ["message",[WARN_TEXT("The selected slot is empty."),[]]] call FUNCTION_NAME;
						_return = false;
					};
					
					// if CTRL was held or Rename Icon clicked, exit with showing name save controls
					if (_heldCtrl) exitWith {	
						private "_text";					
						_text = CTRL(MEU_CTRL_SAVEDLIST) lbText (lbCurSel CTRL(MEU_CTRL_SAVEDLIST));
						meu_renameIndex = lbCurSel CTRL(MEU_CTRL_SAVEDLIST);
						// hide/show ctrls
						{ CTRL(_x) ctrlShow true } forEach MEU_SAVE_GROUP;
						CTRL(MEU_CTRL_SAVESAVE) ctrlShow false; // hide gear save
						{ CTRL(_x) ctrlEnable false } forEach MEU_DISABLE_GROUP;
						CTRL(MEU_CTRL_GEARTREE) ctrlSetTextColor [1,1,1,0.25]; // disabled color
						// get list text and set on RscEdit
						CTRL(MEU_CTRL_SAVEEDIT) ctrlSetText _text;
						CTRL(MEU_CTRL_SAVETEXT) ctrlSetText "Rename Loadout";
						CTRL(MEU_CTRL_SAVETEXT) ctrlSetTextColor [1,1,1,1];
						ctrlSetFocus CTRL(MEU_CTRL_SAVESAVE);
						_return = true;
					};
								
					_return = true;
				} else {
					_m = ["message",[WARN_TEXT("Please select a save slot."),[]]] call FUNCTION_NAME;
					_return = false;
				};
			};
			
			// rename icon opens rename panel
			case MEU_CTRL_RENAMEICON: {
				if ( lbCurSel CTRL(MEU_CTRL_SAVEDLIST) >= 0 ) then {
					
					private _index = parseNumber (CTRL(MEU_CTRL_SAVEDLIST) lbData (lbCurSel CTRL(MEU_CTRL_SAVEDLIST)));
					private _variable = format ["meu_Lo_%1",_index];	
					private _array = profileNamespace getVariable [_variable,[]];
					// check
					if CHECK_LOADOUT(_array) exitWith {
						_m = ["message",[WARN_TEXT("The selected slot is empty."),[]]] call FUNCTION_NAME;
						_return = false;
					};				
					
					private _text = CTRL(MEU_CTRL_SAVEDLIST) lbText (lbCurSel CTRL(MEU_CTRL_SAVEDLIST));
					meu_renameIndex = lbCurSel CTRL(MEU_CTRL_SAVEDLIST);
					
					// hide/show ctrls
					{ CTRL(_x) ctrlShow true } forEach MEU_SAVE_GROUP;
					CTRL(MEU_CTRL_SAVESAVE) ctrlShow false; // hide gear save
					{ CTRL(_x) ctrlEnable false } forEach MEU_DISABLE_GROUP;
					CTRL(MEU_CTRL_GEARTREE) ctrlSetTextColor [1,1,1,0.25]; // disabled color
					CTRL(MEU_CTRL_GEARTREE) tvSetCurSel [-1];
					{ CTRL(_x) ctrlEnable false } forEach (MEU_MENU_GROUP + [MEU_CTRL_SETTINGSICON]); // add settings
					
					// get list text and set on RscEdit
					CTRL(MEU_CTRL_SAVEEDIT) ctrlSetText _text;
					CTRL(MEU_CTRL_SAVETEXT) ctrlSetText "Rename Loadout";
					CTRL(MEU_CTRL_SAVETEXT) ctrlSetTextColor [1,1,1,1];
					ctrlSetFocus CTRL(MEU_CTRL_SAVESAVE);
					
					_return = true;

				} else {
					_m = ["message",[WARN_TEXT("Please select a save slot."),[]]] call FUNCTION_NAME;
					_return = false;
				};
			};
			
			// rename save button
			case MEU_CTRL_SAVERENAME: {			
				private ["_index","_variable","_array","_name","_LO","_favs"];
				// data
				
				_index = parseNumber (CTRL(MEU_CTRL_SAVEDLIST) lbData (lbCurSel CTRL(MEU_CTRL_SAVEDLIST)));
				if (_index in [-1,0]) then {_index = parseNumber (CTRL(MEU_CTRL_SAVEDLIST) lbData meu_renameIndex)};
				_variable = format ["meu_Lo_%1",_index];
				private _listPos = lbCurSel CTRL(MEU_CTRL_SAVEDLIST);
				
				_array = profileNamespace getVariable [_variable,[]];
				
				// name
				_name = ctrlText CTRL(MEU_CTRL_SAVEEDIT);	
				if (_name == "") exitWith { _m = ["message",[WARN_TEXT("Please enter a name."),[]]] call FUNCTION_NAME; };
				
				// save
				_array set [0,_name];
				profileNamespace setVariable [_variable,_array];
				saveProfileNamespace;
				
				// rename favorites
				_LO = _index;
				_favs = profileNamespace getVariable ["meu_Lo_favs",[]];
				if (_LO in _favs) then {
					{	// rename on all boxes
						// if has manager
						if ( _x getVariable ["meu_loadoutManager",false] ) then {
							private ["_index","_box_favs"];
							_box_favs = _x getVariable ["meu_loadoutManager_favs",[]];
							// get sub array index 
							_index = -1;
							{ 
								if (_x select 0 == _LO) then {_index = _forEachIndex};
							} forEach _box_favs;
							// rename action
							if (_index > -1) then {
								private ["_text","_id"];
								_id = (_box_favs select _index) select 1;
								_text = format ["<img image='%2' /> %1",_name,ICON_FAVORITE];
								_x setUserActionText [_id,WARN_TEXT(_text)];
							};
						};
					} forEach (allMissionObjects "Reammobox_F");
				};
				
				// refresh
				{ CTRL(_x) ctrlShow false } forEach MEU_SAVE_GROUP;
				{ CTRL(_x) ctrlEnable true } forEach MEU_DISABLE_GROUP;
				CTRL(MEU_CTRL_GEARTREE) ctrlSetTextColor (COLOR_WARNING_ARRAY);// tree text color
				{ CTRL(_x) ctrlEnable true } forEach (MEU_MENU_GROUP + [MEU_CTRL_SETTINGSICON]);
				meu_renameIndex = nil;

				GUI_REFRESH(_listPos) //<-- new
				private _m = ["message",[format ["Loadout was renamed:<br/><br/>%1",WARN_TEXT(_name)],[]]] call FUNCTION_NAME;

				_return = true;
				
			};
			
			// set favorite
			case MEU_CTRL_FAVORITE: {
				if ( lbCurSel CTRL(MEU_CTRL_SAVEDLIST) >= 0 ) then {
					private ["_index","_variable","_array","_favs","_loadoutNo","_found","_fnc","_adjFavs"];
					// format new data
					private _indexPos = lbCurSel CTRL(MEU_CTRL_SAVEDLIST);
					_index = parseNumber (CTRL(MEU_CTRL_SAVEDLIST) lbData (lbCurSel CTRL(MEU_CTRL_SAVEDLIST)));
					_variable = format ["meu_Lo_%1",_index];
					
					_array = profileNamespace getVariable [_variable,[]];
					// check
					if CHECK_LOADOUT(_array) exitWith {
						_m = ["message",[WARN_TEXT("The selected slot is empty."),[]]] call FUNCTION_NAME;
						_return = false;
					};
					// get array
					_loadoutNo = _index;
					_favs = profileNamespace getVariable ["meu_Lo_favs",[]];
					
					// look for No in favs array
					_found = _favs find _loadoutNo;
					if (_found > -1) then {
						// if found, make Number negative, remove from array
						_loadoutNo = _loadoutNo * -1;
						_favs deleteAt _found;
					} else {
						// else, add to array
						_favs pushBack _loadoutNo;
					};
					// call fnc
					_fnc = [_loadoutNo,(allMissionObjects "Reammobox_F")]; 
					_adjFavs = CALL_FNC("favorites",_fnc);
					
					// save array
					profileNamespace setVariable ["meu_Lo_favs",_favs];
					saveProfileNamespace;
					
					GUI_REFRESH(_indexPos)
										
					_return = true;
				} else {
					_m = [WARN_TEXT("Please select a save slot."),[]];
					_m = CALL_FNC("message",_m);
					_return = false;
				}; 
				
			};
			
			// show settings panel
			case MEU_CTRL_SETTINGSICON: {
				private _s = ctrlShown CTRL(MEU_CTRL_PREFBGFRAME); // flip the below based on shown/hidden
				{ CTRL(_x) ctrlShow !_s; } forEach MEU_PREF_GROUP;
				{ CTRL(_x) ctrlEnable _s } forEach MEU_MENU_GROUP;
				{ CTRL(_x) ctrlEnable _s } forEach MEU_DISABLE_GROUP;
				CTRL(MEU_CTRL_CLOSEICON) ctrlEnable true; // part of menu group
				CTRL(MEU_CTRL_GEARTREE) tvSetCurSel [-1]; // deselect cause it doesnt disable
				CTRL(MEU_CTRL_SETTINGSICON) ctrlSetTextColor ([[1,1,1,0.7],COLOR_WARNING_ARRAY] select !_s);
				
				CTRL(MEU_CTRL_PREFTIPSTEXT) ctrlSetText (selectRandom MEU_HELP_TIPS);
				
				CTRL(MEU_CTRL_PREFBUTIMPORT) ctrlSetText "Import";
				CTRL(MEU_CTRL_PREFBUTIMPORT) ctrlSetTextColor [1,1,1,1];
				
				// get data
				private _clipData = copyFromClipboard;
//				private _bookends = "15th LOM Data - DO NOT MAKE EDITS TO THIS";						
				private _bookends = CHECK_BOOKENDS;						
				// first string tests
				private _test1 = (_clipData select [0,44]) isEqualTo ("[" + str _bookends);
				private _test2 = (_clipData select [(count _clipData) - 44,count _clipData]) isEqualTo (str _bookends + "]");
						
				CTRL(MEU_CTRL_PREFBUTIMPORT) ctrlEnable (_test1 && _test2);
				CTRL(MEU_CTRL_PREFBUTIMPORT) ctrlSetTooltip (["No Compatable Data Found.","Import Loadout And Settings From Clipboard."] select (_test1 && _test2));
				
				_return = true;
			};
			
			// open inventory on crate
			case MEU_CTRL_INVICON: {
				if (ctrlShown CTRL(MEU_CTRL_SAVEEDIT)) exitWith {_return = false;};
				uinamespace setVariable ['meu_managerBOX',meu_managerBOX];
				player action ['Gear',meu_managerBOX]; 
				private _nul = [] spawn {
					waitUntil {!isNull (findDisplay 602)};
					private _nul = (findDisplay 602) displayAddEventHandler ['Unload',{
						private _box = uiNamespace getVariable "meu_managerBOX";
						private _null = SPAWN_FNC("open",[_box]);
						uiNamespace setVariable ["meu_managerBOX",nil];
						false 
					}]; 
				};
				_return = true;		
			};
			
			// expand/collapse gear list
			case MEU_CTRL_EXPANDICON: {				
//				private _load = (profileNamespace getVariable ['meu_Lo_settings',[0,0,0]]) select 2;
				private _load = (missionNamespace getVariable ['meu_Lo_settings',[0,0,0,0]]) select 2;
				private _new = [1,0] select _load; // opposite day
				private _nul = ['settings',['save','expandgear',_new]] call meu_fnc_manager;
				CTRL(MEU_CTRL_EXPANDICON) ctrlSetText ([ICON_EXPAND,ICON_COLLAPSE] select _new);
				private _def = CALL_FNC("listDefault",[]);
				_r = true;
			};
			
			default {
				private _error = format ["ERROR: _button was %1",_button];
				private _msg = CALL_FNC("message",[ERROR_TEXT(_error)]);
			};
		};
		
		_r = _return;
	};
	
	case "loadsaved": {
		// load gear from saved
		private ["_index","_name","_array","_gear","_checking","_boxGear","_restricted"];
		// get array from selected index or send loadout number & box
		_index = _params param [0,parseNumber (CTRL(MEU_CTRL_SAVEDLIST) lbData (lbCurSel CTRL(MEU_CTRL_SAVEDLIST))),[0]];
		private _box = _params param [1,(missionNamespace getVariable ["meu_managerBOX",objNull])];
		private _isFav = _params param [2,false,[false]];
		missionNamespace setVariable ["meu_managerBOX",_box]; // define for favorites
		private _listPos = lbCurSel CTRL(MEU_CTRL_SAVEDLIST);

		
		// if in empty, save instead- this process is very convoluted now
		if ( !isNil {meuEmptyLoadouts} && {_index isEqualTo (meuEmptyLoadouts select 0)} ) exitWith {
			private _save = ["buttons",CTRL(MEU_CTRL_SAVEICON)] call FUNCTION_NAME; 
		};
		
		// no selection - _isFav skips this when called from addAction
		if ( !_isFav && {lbCurSel CTRL(MEU_CTRL_SAVEDLIST) < 0} ) exitWith {
			// check for default selection
			if (lbCurSel CTRL(MEU_CTRL_DEFAULTLIST) > -1) then {
				private "_default";
				_default = CALL_FNC("loadDefault",[]);
				_r = true;
			} else {
				_m = ["message",[WARN_TEXT("Please select a loadout."),[]]] call FUNCTION_NAME; 		
				_r = false;
			};
		}; 
		// data
		_name = format ["meu_Lo_%1",_index];
		_array = profileNamespace getVariable [_name,[]];	
		
		// checks
		if CHECK_LOADOUT(_array) exitWith {
			_m = ["message",[WARN_TEXT("The selected slot is empty."),[]]] call FUNCTION_NAME;
			_r = false;
		};
		
		// the gear
		_gear = _array select 1;

		_checking = _box getVariable ["meu_boxRestrictions",true];
		_boxGear = CALL_FNC("boxGear",[]);
		
		_restricted = [];
		// clear current gear
		if ( CHECK_STRING_X(primaryWeapon player) && CHECK_BOX_X(GEAR(9)) ) then {
			// this stops the weapon changing animation 
			{ player removeWeapon _x } forEach [handgunWeapon player, secondaryWeapon player];
		} else {
			removeAllWeapons player;
		};
		removeAllAssignedItems player;
		removeAllItems player;
		removeBackpack player;
		removeUniform player;
		removeVest player;
		removeHeadgear player;
		
		/*
		_meu_headgear = _gear select 0; //
		_meu_goggles = _gear select 1; //
		_meu_uniform = _gear select 2; //
		_meu_uniformitems = _gear select 3; //
		_meu_vest = _gear select 4; // 
		_meu_vestitems = _gear select 5; //
		_meu_asgnItems = _gear select 6; //
		_meu_backpack = _gear select 7; //
		_meu_packitems = _gear select 8; //
		_meu_PrimaryGun = _gear select 9; //
		_meu_pgunitems = _gear select 10; //
		_meu_SecondaryGun = _gear select 11; //
		_meu_sgunitems = _gear select 12; //
		_meu_handgun = _gear select 13; //
		_meu_handgunitems = _gear select 14; //
		_meu_swFreqs = _gear select 15; // <-- new
		_meu_lrFreqs = _gear select 16; // <-- new
		*/	
		
		// set radio variables
		if (TFAR_CHECK) then { // has tfar
			if (count _gear > 15) then { // has new array size
				private ["_swFreq","_lrFreq"];
				if CHECK_COUNT(15) then { // has freqs saved
					if (isNil {TF_MAX_CHANNELS}) then {
						TF_MAX_CHANNELS = 8; // not defined in Eden
					};
					_swFreq = false call TFAR_fnc_generateSwSettings;
					_swFreq set [2,GEAR(15)]; // set 3rd element with saved freqs array
					(group player) setVariable ["tf_sw_frequency", _swFreq];
				};
				if CHECK_COUNT(16) then {
					if (isNil {TF_MAX_CHANNELS}) then {
						TF_MAX_LR_CHANNELS = 9; // not defined in Eden
					};
					_lrFreq = false call TFAR_fnc_generateLrSettings;
					_lrFreq set [2,GEAR(16)];
					(group player) setVariable ["tf_lr_frequency",_lrFreq];
				};
			};
		};
		
		// add string items
		ADD_STRING(0,addHeadGear);
		ADD_STRING(1,addGoggles);
		ADD_STRING(2,forceAddUniform);
		ADD_STRING(4,addVest);
		ADD_STRING(7,addBackpack);
		ADD_STRING(9,addWeapon);
		ADD_STRING(11,addWeapon);
		ADD_STRING(13,addWeapon);
		
		// add array weapon items
		ADD_ITEMS(10,addPrimaryWeaponItem);
		ADD_ITEMS(12,addSecondaryWeaponItem);
		ADD_ITEMS(14,addHandgunItem);
		
		// add array outfit items
		ADD_TO_OUTFIT(3,addItemToUniform);
		ADD_TO_OUTFIT(5,addItemToVest);
		clearAllItemsFromBackpack player;
		ADD_TO_OUTFIT(8,addItemToBackpack);	
					
		// add assigned items
		if CHECK_COUNT(6) then {
			{
				GET_PARENT(_x);
				if CHECK_IS_RADIO then { 
					player linkItem "ItemRadio";
				} else {
					if CHECK_BOX_X(_x) then {
						if (([(configFile >> (CALL_FNC("config",[_x])) >> _x),"optics",0] call BIS_fnc_returnConfigEntry) > 0) then {
							player addWeapon _x;
							player assignItem _x; 
						} else {
							player linkItem _x;
						};
					} else { 
						ADD_RESTRICT_X(_x); 
					};
				};
			} foreach GEAR(6);
		};
		
		// finish grip sorcery
		if ((GEAR(10) select 3) find "_grip" > -1) then {
			if (!isNil {RHS_fnc_accGripod}) then {
				private _null = [_display] call RHS_fnc_accGripod;
			};
		};
		
		// error message
		if (count _restricted > 0) then {
			_m = [ERROR_TEXT("Gear Not Available:"),_restricted];
			_m = CALL_FNC("message",_m);
		} else { 
			// or clear any previous hint
			hint ""; 
		};
		if (cbChecked CTRL(MEU_CTRL_PREFAUTOBOX)) then {
			closeDialog 0;
		} else {
			// refresh list
			GUI_REFRESH(_listPos)
		};
		
		_r = true;	
	};
		
	case "loaddefault": {
		private ["_defaultList","_gearTree","_index","_file","_gear","_null"];
		// ctrls
		_defaultList = CTRL(MEU_CTRL_DEFAULTLIST);
		
		// get data string
		_index = lbCurSel _defaultList;
		_file = _defaultList lbData _index;
		
		// check
		if (_index < 0 || {_file == ""}) exitWith {
			_m = ["message",[WARN_TEXT("Please select a loadout."),[]]] call FUNCTION_NAME;
			_r = false	
		};
		
		// execVM file
		private _spawn = [_file,cbChecked CTRL(MEU_CTRL_PREFAUTOBOX)] spawn {
			params ["_file","_close"];
			private _handle = [nil,player] execVM (MEU_LOADOUT_ROOT + _file);
			waitUntil {scriptDone _handle};
			hint "";
			if _close then {
				closeDialog 0;
			} else {
				GUI_REFRESH(-1)
			};
		};
		
		/* // clear any hint
		Hint "";
		
		if (cbChecked CTRL(MEU_CTRL_PREFAUTOBOX)) then {
			closeDialog 0;
		} else {
			// refresh list
			GUI_REFRESH(-1)
		}; */
		
		_r = true;
	};
	
	case "config": {
		// return config class of item
		private["_item","_config"];
		_item = _params param [0,""];
		_config = call {
			if (isClass (configFile >> "CfgMagazines" >> _item)) exitWith {"CfgMagazines"};
			if (isClass (configFile >> "CfgWeapons" >> _item)) exitWith {"CfgWeapons"};
			if (isClass (configFile >> "CfgVehicles" >> _item)) exitWith {"CfgVehicles"};
			if (isClass (configFile >> "CfgGlasses" >> _item)) exitWith {"CfgGlasses"};
			""
		};
		_r = _config;
	};
	
	case "message": {
		// formatted messages
		private ["_message","_gear","_text"];
		_message = _params param [0,""];
		_gear = _params param [1,[]];

		private _unique = [];
		{ _unique pushBackUnique _x; } forEach _gear;
		
		_header = parseText MESSAGE_HEADER;
		_message = parseText format ["<t size='1.1' shadow='1'>%1</t>",_message];
		_text = composeText [_header,linebreak,linebreak,_message,linebreak];
		
		{
			private ["_config","_pic","_name"];
			_config = CALL_FNC("config",[_x]);
			if !(_config isEqualTo "") then {
				_name = getText (configFile >> _config >> _x >> "displayname");
				_pic = getText (configFile >> _config >> _x >> "picture");
			} else {
				_name = parseText format ["<t color='%2'>%1 - CLASS NOT FOUND</t>",_x,COLOR_ERROR_HTML];
				_pic = ICON_NOTINBOX;
			};
			_text = composeText [_text,lineBreak,image _pic," ",_name];
		} forEach _unique;
	
		hint _text; // add condition to sound
//		if true then { playSound "3DEN_notificationDefault"; };
		if (missionNamespace getVariable ["meu_Lo_settings",[0,0,0,0]] select 3 isEqualTo 0) then { playSound "RscDisplayCurator_ping06"; };
		
		_r = true;
	};
	
	case "favorites": {
		// handle favorites / positive number to add, negative to remove	
		private ["_num","_boxes"];
		// params
		_num = _params param [0,-1];
		_boxes = _params param [1,[]];
		
		// run on all sent boxes
		{
			private ["_box_favs"];
			// check for manager
			if ( _x getVariable ["meu_loadoutManager",false] ) then {
				// get favs array
				_box_favs = _x getVariable ["meu_loadoutManager_favs",[]];
				if (_num >= 0) then {
					// add to box
					private ["_var","_array","_text","_id"];
					_var = format ["meu_Lo_%1",_num];
					_array = profileNamespace getVariable [_var,[]];
					// check array size
					if (count _array == 2 && (count (_array select 1) > 0)) then {
						// add action
						_text = format ["<img image='%2' /> %1",(_array select 0),ICON_FAVORITE];
						_id = _x addAction [
							WARN_TEXT(_text),
							{ ["loadSaved",(_this select 3)] call FUNCTION_NAME; },
//							[(_num - 1),_x],
							[_num,_x,true], //<-- new method
							5,
							true,
							true,
							"",
							ACTION_CONDITION
						];
						_box_favs pushBack [_num,_id];
					};
					
				} else {
					// remove from boxes
					private ["_LO","_index","_id"];
					_LO = abs _num;
					// find loadout in favs
					_index = -1;
					{
						if (_x select 0 == _LO) then {_index = _forEachIndex};
					} forEach _box_favs;
					if (_index > -1) then {
						// remove loadout
						_id = (_box_favs select _index) select 1;
						_x removeAction _id;
						_box_favs deleteAt _index;
					};
				};
				// save new array
				_x setVariable ["meu_loadoutManager_favs",_box_favs];
				
			};
		} forEach _boxes;
		
		
		_r = true;
	};
	
	case "settings": {
		// handle saved preferences
		_params params [
			["_todo","test"],
			["_option",""],
			["_data",0],
			"_return","_construct","_settings"
		];
		_return = false;				
		// get preferences
		_settings = profileNamespace getVariable ["meu_Lo_settings",[]];
		_construct = ["autoclose","hidedefaults","expandgear","sounds"];
		// set defaults if not available - does init check make this redundent?
		if (count _settings < 4) then {
			_settings = [0,0,0,0];
			profileNamespace setVariable ["meu_Lo_settings",_settings];
			saveProfileNamespace;
			missionNamespace setVariable ["meu_Lo_settings",_settings];
		};		
		// save or load
		switch (toLower _todo) do {
			// save settings to profile
			case "save": {
				// save sent value
				if (_option isEqualto "") exitWith {_return = false;};
				_settings set [(_construct find _option),_data];
				profileNamespace setVariable ["meu_Lo_settings",_settings];
				saveProfileNamespace;
				missionNamespace setVariable ["meu_Lo_settings",_settings];
				_return = true;
			};
			// export loadouts to clipboard
			case "export": {
				private _bookends = "15th LOM Data - DO NOT MAKE EDITS TO THIS";
				copyToClipboard str [
					_bookends,
					meuLoadOuts,
					profileNamespace getVariable ["meu_LO_favs",[]],
					profileNamespace getVariable ["meu_Lo_settings",[]],
					_bookends
				];
				private _msg = CALL_FNC("message",["Data Saved To Clipboard. Be Careful Not To Overwrite. Change To Another Profile And Select Import in LOM Preferences."]);
				_return = true;
			};
			// import loadouts to profile
			case "import": {					
				switch ( toLower (ctrlText CTRL(MEU_CTRL_PREFBUTIMPORT)) ) do {
					// handle button based on text - easier than setting buttonAction
					case "import": {				
						// get data
						private _clipData = copyFromClipboard;
//						private _bookends = "15th LOM Data - DO NOT MAKE EDITS TO THIS";						
						private _bookends = CHECK_BOOKENDS;						
						// first string tests
						private _test1 = (_clipData select [0,44]) isEqualTo ("[" + str _bookends);
						private _test2 = (_clipData select [(count _clipData) - 44,count _clipData]) isEqualTo (str _bookends + "]");
						if (!_test1 || !_test2) exitWith {
							private _msg = CALL_FNC("message",[ERROR_TEXT("Clipboard Data Incompatable.")]);
						};					
						// compile string & second size test
						_clipData = call compile _clipData;
						private _test3 = count (_clipData select 1) isEqualTo 20;
						if ( _test1 && _test2 && _test3 ) then {
							// change button to confirmation
							private _msg = CALL_FNC("message",[WARN_TEXT("WARNING: This Will Overwrite Current Loadouts And Settings. Confirm To Continue Or Exit To Cancel.")]);
							CTRL(MEU_CTRL_PREFBUTIMPORT) ctrlSetText "Confirm";
							CTRL(MEU_CTRL_PREFBUTIMPORT) ctrlSetTextColor (COLOR_ERROR_ARRAY);
						} else {
							private _msg = CALL_FNC("message",[ERROR_TEXT("Clipboard Data Incompatable.")]);
						};
						_return = true;					
					};
					// handle all import processing 
					case "confirm": {
						// change button back to normal
						CTRL(MEU_CTRL_PREFBUTIMPORT) ctrlSetText "Import";
						CTRL(MEU_CTRL_PREFBUTIMPORT) ctrlSetTextColor [1,1,1,1];
						// format data
						private _clipData = copyFromClipboard;
						private _bookends = "15th LOM Data - DO NOT MAKE EDITS TO THIS";
						// test again - just in case fucktards copy something before confirmation
						private _test1 = (_clipData select [0,44]) isEqualTo ("[" + str _bookends);
						private _test2 = (_clipData select [(count _clipData) - 44,count _clipData]) isEqualTo (str _bookends + "]");
						if (!_test1 || !_test2) exitWith {
							private _msg = CALL_FNC("message",[ERROR_TEXT("Clipboard Data Incompatable.")]);
						};
						// format data
						_clipData = call compile _clipData;
						_clipData params [
							"",
							["_importLoadouts",[]],
							["_importFavs",[]],
							["_importSettings",[]]
						];
						// remove current favorites from boxes
						private _favs = profileNamespace getVariable ["meu_LO_favs",[]];
						{
							private _loadoutNo = _x * -1;
							private _fnc = [_loadoutNo,(allMissionObjects "Reammobox_F")];
							private _remove = CALL_FNC("favorites",_fnc); 
						} forEach _favs;						
						// loop through loadouts saving to variables. save all to  meuLoadOuts
						meuLoadOuts = [];
						for "_num" from 1 to 20 do {
							call compile format [
								"
									meu_LO_%1 = if (count _importLoadouts >= _num) then [{_importLoadouts select (_num - 1)},{[]}];
									if ((count meu_Lo_%1) isEqualTo 0) then {
										meu_LO_%1 = [""Empty Loadout %1""];
									};
									profileNamespace setVariable [""meu_LO_%1"",meu_LO_%1];
									meuLoadOuts pushBack meu_LO_%1;
								",
								_num
							];
						};						
						// save favorites and loop send to "favorites"
						profileNamespace setVariable ["meu_LO_favs",_importFavs];
						{
							private _fnc = [_x,(allMissionObjects "Reammobox_F")];
							private _add = CALL_FNC("favorites",_fnc);
						} forEach _importFavs;						
						// save settings
						profileNamespace setVariable ["meu_Lo_settings",_importSettings];
						saveProfileNamespace;
						missionNamespace setVariable ["meu_Lo_settings",_importSettings];
						// refreash or reload LOM 
						_box = missionNamespace getVariable "meu_managerBOX";
						closeDialog 0;
						private _diag = CALL_FNC("open",[_box]);
						// maybe reopen settings panel here??
						private _msg = CALL_FNC("message",[WARN_TEXT("Profile Imported!")]);
						
						_return = true;
					};				
					
					default {
						private _error = format ["ERROR: Button text was %1",ctrlText CTRL(MEU_CTRL_PREFBUTIMPORT)];
						private _msg = CALL_FNC("message",[ERROR_TEXT(_error)]);
					};
				};			
			};
			
			default {
				private _error = format ["ERROR: _todo was %1",_todo];
				private _msg = CALL_FNC("message",[ERROR_TEXT(_error)]);
			};
		};		
		_r = _return;
	};
	
	case "keydown": {
		// handle the various keyboard short cuts
		_params params ["_ctrl","_key","_shftHeld","_ctrlHeld","_altHeld"];
		
		switch true do {
			// gear key open inventory
			case (_key in actionKeys "Gear"): {
				// ['buttons',[(uiNamespace getVariable 'meu_managerGUI') displayCtrl 15027]] call meu_fnc_manager;
				private _gear = CALL_FNC("buttons",CTRL(MEU_CTRL_INVICON));
			};
			// S key to export in scripted format
			case (_key in [31]): {
				if _ctrlHeld then {
					private _script = [player,"script",false] call BIS_fnc_exportInventory;
					private _msg = CALL_FNC("message",["Scripted Loadout Copied To Clipboard. Open Text Document And Use 'CTRL+V' To Paste."])
				};
			};
			// H key to open Field Manual
			case (_key in actionKeys "Help"): {
				if _ctrlHeld then {
					private _manual = ["meu_manager","meu_usage",_display] call BIS_fnc_openFieldManual;
				};
			};
			// D key for default spawn loadout
			case (_key in [32]): {
				if _ctrlHeld then {
					private _index = parseNumber (CTRL(MEU_CTRL_SAVEDLIST) lbData (lbCurSel CTRL(MEU_CTRL_SAVEDLIST)));
					private _type = typeOf meu_managerBOX;
					profileNamespace setVariable ["meu_Lo_spawn",[_index,_type]];
					saveProfileNamespace;
					
					if (_index > 0) then {
						private _loadout = format ["meu_Lo_%1",_index];
						private _name = (profileNamespace getVariable [_loadout,[]]) select 0;
						private _m = ["message",[format ["Default Spawn Loadout Was Saved:<br/><br/>%1",WARN_TEXT(_name)],[]]] call FUNCTION_NAME;
					} else {
						private _m = CALL_FNC("message",["Default Spawn Loadout Removed."]);
					};
					GUI_REFRESH(lbCurSel CTRL(MEU_CTRL_SAVEDLIST))
				};
			};
			
		};
		_r = false;
	};
	
	case "spawngear": {
		// handle the management of default spawn loadout
		/* 
			- Manage save seletion
				1. get savedlist index data
				2. get type of crate
				3. set var - [index,type]
				4. add icon to savedlist index
			- handle initial loading of gear
				1. check(s) if first to run 
				2. get conditions from attributes window
				3. get data and call loadSaved
				4. set variables
				5. create respawn EH
		*/
		_params params [
			["_load",false,[false]]
		];
		
		if _load then {
			// load gear, set variables, add respawn EH
		} else {
			// add, change, or remove the saved default loadout
		};
		
	};
	
};

_r 