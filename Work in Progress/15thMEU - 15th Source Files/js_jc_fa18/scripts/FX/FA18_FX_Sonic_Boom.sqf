// ---------------------
// Original FX scripts by Lethal
// Modified by Gnat
// ---------------------
private ["_emitter","_nowspeed", "_lastspeed"];

_plane = _this select 0;
_boom = "F18sonicboom";
_lastspeed = speed _plane;

{if (typeOf _x == "#particlesource") then {deleteVehicle _x}} forEach (_plane nearObjects 3);  // on INIT, remove duplicate emitters (in case of JIP); the only #particlesource object(s) it finds will be the _emitter.

_emitter = "#particlesource" createVehicle position _plane;
_emitter setParticleRandom [0.05,[2,2,2],[5,5,5],0,0.5,[0.1,0.1,0.1,0.5],0,0];
_emitter setDropInterval 0;

while {alive _plane} do {
	_nowspeed = speed _plane;
	if ((_nowspeed > 850) and (_nowspeed < 860) and (_lastspeed < _nowspeed)) then {
		_emitter setDropInterval 0.0005;
		for "_i" from 1 to 100 do {
			_emitter setParticleRandom [0.4,[10*(_i/100),10*(_i/100),10*(_i/100)],[10*(_i/100),10*(_i/100),10*(_i/100)],0,0.3,[0.1,0.15,0.3,0.5],0,0];
			_emitter setParticleParams [["\15th_F18_Fixes\data\Universal.p3d", 16, 12, 1],"","Billboard",1,0.1,[0,(-12*(_i/100)+8),-0.5],(velocity _plane),1,1.2745,1,0.001,[20*(_i/100)],[[1,1,1,0],[1,1,1,0.1*(_i/100)],[1,1,1,0]],[0],0,0,"","",_plane];
			sleep 0.005;
			if (_i == 50) then { // play boom sound only 1x
				_plane say _boom;  // A3 sound syntax: [[_plane, "EA18Gsonicboom"], {(_this select 0) say3D (_this select 1);}, true, false] spawn BIS_fnc_MP;
			};
		};
		_emitter setDropInterval 0;
		sleep ((random 5)+ 5);
	} else {
		_emitter setDropInterval 0
	};

	_emitter setParticleParams [["\15th_F18_Fixes\data\Universal.p3d", 16, 12, 1],"","Billboard",1,0.05,(_plane selectionposition "zamerny"),(velocity _plane),1,1.2745,1,0.001,[10,20],[[1,1,1,0],[1,1,1,0.1],[1,1,1,0]],[0],0,0,"","",_plane];
	_emitter setpos (getpos _plane);
	_lastspeed = speed _plane;
	sleep 0.356759;
};

deleteVehicle _emitter; // clean up on death of plane
