	_veh = vehicle player;
	_veh_pilot = driver _veh;
	_default_sleep_time = 3;
	_sleep_time = 3;		
	_veh_stop_speed = 1;
	_Fuel_veh = fuel _veh;
  	If (!Local Player) ExitWith {};

	if (speed _veh > _veh_stop_speed) exitWith {if ((player == _veh_pilot)) then {titleText ["Refueling canceled...", "PLAIN DOWN",0.3];};};
	if ((player == _veh_pilot)) then {titleText ["Refueling...", "PLAIN DOWN",0.3];};

	while {alive _veh} do 
	{
	
		_Fuel_veh = fuel _veh;
		_veh setfuel (_Fuel_veh + 0.015);	
		If ((_Fuel_veh > 0.98) and (player == _veh_pilot)) exitWith {titleText ["Refueling complete...", "PLAIN DOWN",0.6];};
		if (speed _veh > _veh_stop_speed) exitWith {if ((player == _veh_pilot)) then {titleText ["Refueling canceled...", "PLAIN DOWN",0.3];};};
		if ((player == _veh_pilot)) then {titleText ["Refueling...", "PLAIN DOWN",0.3];};
		sleep _sleep_time;
	};