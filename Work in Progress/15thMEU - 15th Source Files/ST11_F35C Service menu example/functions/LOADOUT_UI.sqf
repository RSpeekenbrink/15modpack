	If (!Local Player) ExitWith {};  
	Private ["_veh"];
	_veh = vehicle player;

	closeDialog 0;

	sleep 0.01;

	CreateDialog "LOADOUTS_UI";

           {lbAdd[3101,_x]} forEach [ 
				"EMPTY", 
             	"M274 Rockets (Smoke / Practice)",
             	"M151 Rockets (HE)",
				"M247 Rockets (HEAT)",
				"AGM - 114 Hellfire"
           ];

           {lbAdd[3102,_x]} forEach [
              	"EMPTY",
				"M274 Rockets (Smoke / Practice)",
             	"M151 Rockets (HE)",
				"M247 Rockets (HEAT)",
				"AGM - 114 Hellfire"
            ];

           {lbAdd[3103,_x]} forEach [
              	"EMPTY",
				"M274 Rockets (Smoke / Practice)",
             	"M151 Rockets (HE)",
				"M247 Rockets (HEAT)",
				"AGM - 114 Hellfire"
            ];

           {lbAdd[3104,_x]} forEach [
              	"EMPTY",
				"M274 Rockets (Smoke / Practice)",
             	"M151 Rockets (HE)",
				"M247 Rockets (HEAT)",
				"AGM - 114 Hellfire"
            ];

         	((uiNamespace getVariable "myDisplay") displayCtrl 3101) lbSetCurSel 1;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3102) lbSetCurSel 4;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3103) lbSetCurSel 4;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3104) lbSetCurSel 1;
    
	WaitUntil {!Dialog};
 
	_veh animate ["service_menu_switch",0];