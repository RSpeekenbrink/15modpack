class CfgAmmo 
{
	class MissileBase;						
	class a_empty : MissileBase 
	{
		hit = 0;
		indirectHit = 0;
		indirectHitRange = 0;
		thrustTime = 0;
		thrust = 0;
		maxspeed = 0;	
		timetolive = 0;
		maneuvrability = 0;
		model = "\path to model\m_empty";
		proxyShape = "\path to model\m_empty";
		airlock = false;
		laserLock = false;
		irLock = false;
		initTime = 0.0;
		minRange = 0;
		minRangeProbab = 0;
		midRange = 0;
		midRangeProbab = 0;
		maxRange = 0;
		maxRangeProbab = 0;
		sideAirFriction = 0;
	};
};