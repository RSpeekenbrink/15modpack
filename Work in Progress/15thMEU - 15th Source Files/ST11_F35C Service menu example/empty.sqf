/*
	Author: John_Spartan & Saul
        Edited by: Peral

	Description:
	Empty loadout function for "Your vehicle"

	Exucution:
	_veh execVM "\path too function\empty.sqf";
	via unit init line in editor or via instance of mission script [INIT.sqf for example]
	
	Parameter(s):
		NONE


	Returns: nothing
	Result: aircrfat stripped of all weapons/magazines

*/



//BASIC DEFINITIONS
_veh = _this;				//name of the unit we are playing with


//WEAPON CLASSNAME DEFINITIONS
//LIST OF ALL POSSIBLE WEAPONS USED WITH THIS PLANE
//IN CASE WE NEED TO ADAPT THIS TO SOME OTHER MOD
_SAFE_weapon = "w_master_arms_safe"; 
_Your_weapon = "Weapon Name"; 


//MAGAZINE CLASSNAME DEFINITIONS
//LIST OF ALL POSSIBLE MAGAZINES USED WITH THIS PLANE
//IN CASE WE NEED TO ADAPT THIS TO SOME OTHER MOD
_Your_magazine = "Magazine Name";
_EMPTY_magazine = "m_empty"; 
_EMPTY_FAKE_magazine = "m_fake_empty";


//SWITCH PILOTS WEAPON TO SAFE
_veh selectWeapon _SAFE_weapon;
_veh animate ["switch_rearming_done",0];


//ROMEVE ALL POSSIBLE DEFAULT WEAPONS
_veh removeWeapon _Your_weapon; 


//ROMEVE ALL POSSIBLE DEFAULT MAGAZINES
_veh removeMagazines _Your_magazine;
_veh removeMagazines _EMPTY_magazine; 
_veh removeMagazines _EMPTY_FAKE_magazine;

//FINALIZING FUNCTION 
_veh setVehicleAmmo 1;
_veh selectWeapon _SAFE_weapon;
_veh animate ["switch_rearming_done",1];

