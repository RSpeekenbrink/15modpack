class SKINS_UI 
{
	idd = 3000;
	movingenable = "false";
	onLoad = "uiNamespace setVariable ['myDisplay', (_this select 0)]";
	
	class Controls 
	{

		class MAIN_BOX_1200 : RscPicture 
		{
			idc = 1200;
			x = safezoneX;
			y = safezoneY;
			w = 0.1 + safezoneW;
			h = 0.1 + safezoneH;
			text = "\path to paa\UI\GUI_background.paa";
		};
		class MAIN_PIC_LOADOUTS: RscPicture 
		{
			idc = 1204;
			text = "\path to paa\UI\GUI_main_skins.paa";
			x = 0 * safezoneW + safezoneX;
			y = 0.052 * safezoneH + safezoneY;
			w = 1 * safezoneW;
			h = 0.84 * safezoneH;
		};
		class MAIN_FRAME: RscFrame
		{
			idc = 1205;
			x = 0.005 * safezoneW + safezoneX;
			y = 0.005 * safezoneH + safezoneY;
			w = 0.99 * safezoneW;
			h = 0.99 * safezoneH;
		};

		class SKIN_VFA14CAG_1601: Custom_RscButtonMenu
		{
			idc = 1601;
			x = 0.0944777 * safezoneW + safezoneX;
			y = 0.222737 * safezoneH + safezoneY;
			w = 0.149175 * safezoneW;
			h = 0.14163 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "PAINT_SCHEME_1";
			action = "[]Spawn SERVICE_MENU_fnc_PAINT_SCHEME_1";
		};
		
		class SKIN_VFA14LOW_1602: Custom_RscButtonMenu
		{
			idc = 1602;
			x = 0.313644 * safezoneW + safezoneX;
			y = 0.222739 * safezoneH + safezoneY;
			w = 0.149175 * safezoneW;
			h = 0.14163 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "PAINT_SCHEME_2";
			action = "[]Spawn SERVICE_MENU_fnc_PAINT_SCHEME_2";
		};
		
		class SKIN_VFA27CAG_1603: Custom_RscButtonMenu
		{
			idc = 1603;
			x = 0.522308 * safezoneW + safezoneX;
			y = 0.222739 * safezoneH + safezoneY;
			w = 0.149175 * safezoneW;
			h = 0.14163 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "PAINT_SCHEME_3";
			action = "[]Spawn SERVICE_MENU_fnc_PAINT_SCHEME_3";
		};

		class SKIN_VFA27LOW_1604: Custom_RscButtonMenu
		{
			idc = 1604;
			x = 0.734915 * safezoneW + safezoneX;
			y = 0.222739 * safezoneH + safezoneY;
			w = 0.149175 * safezoneW;
			h = 0.14163 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "PAINT_SCHEME_4";
			action = "[]Spawn SERVICE_MENU_fnc_PAINT_SCHEME_4";
		};

		class RscTextCheckbox_2501: RscPicture
		{
			idc = 2501;
			text = "\path to paa\UI\GUI_button_tickbox_off.paa";
			x = "0.198159 * safezoneW + safezoneX";
			y = "0.191929 * safezoneH + safezoneY";
			w = "0.0393711 * safezoneW";
			h = "0.067215 * safezoneH";
		};

		class RscTextCheckbox_2502: RscPicture
		{
			idc = 2502;
			text = "\path to paa\UI\GUI_button_tickbox_off.paa";
			x = "0.198155 * safezoneW + safezoneX";
			y = "0.514005 * safezoneH + safezoneY";
			w = "0.0393711 * safezoneW";
			h = "0.067215 * safezoneH";
		};

		class RscTextCheckbox_2503: RscPicture
		{
			idc = 2503;
			text = "\path to paa\UI\GUI_button_tickbox_off.paa";
			x = "0.515748 * safezoneW + safezoneX";
			y = "0.191931 * safezoneH + safezoneY";
			w = "0.0393711 * safezoneW";
			h = "0.067215 * safezoneH";
		};

		class RscTextCheckbox_2504: RscPicture
		{
			idc = 2504;
			text = "\path to paa\UI\GUI_button_tickbox_off.paa";
			x = "0.515748 * safezoneW + safezoneX";
			y = "0.514003 * safezoneH + safezoneY";
			w = "0.0393711 * safezoneW";
			h = "0.067215 * safezoneH";
		};

		class RscTextCheckbox_2505: RscPicture
		{
			idc = 2505;
			text = "\path to paa\UI\GUI_button_tickbox_off.paa";
			x = "0.198159 * safezoneW + safezoneX";
			y = "0.191929 * safezoneH + safezoneY";
			w = "0.0393711 * safezoneW";
			h = "0.067215 * safezoneH";
		};

		class RscTextCheckbox_2506: RscPicture
		{
			idc = 2506;
			text = "\path to paa\UI\GUI_button_tickbox_off.paa";
			x = "0.198155 * safezoneW + safezoneX";
			y = "0.514005 * safezoneH + safezoneY";
			w = "0.0393711 * safezoneW";
			h = "0.067215 * safezoneH";
		};

		class RscTextCheckbox_2507: RscPicture
		{
			idc = 2507;
			text = "\path to paa\UI\GUI_button_tickbox_off.paa";
			x = "0.515748 * safezoneW + safezoneX";
			y = "0.191931 * safezoneH + safezoneY";
			w = "0.0393711 * safezoneW";
			h = "0.067215 * safezoneH";
		};

		class RscTextCheckbox_2508: RscPicture
		{
			idc = 2508;
			text = "\path to paa\UI\GUI_button_tickbox_off.paa";
			x = "0.515748 * safezoneW + safezoneX";
			y = "0.514003 * safezoneH + safezoneY";
			w = "0.0393711 * safezoneW";
			h = "0.067215 * safezoneH";
		};

		class RETURN_TO_MAIN_BUTTON_1612: Custom_RscButtonMenu
		{
			idc = 1612;
			x = 0.787409 * safezoneW + safezoneX;
			y = 0.110712 * safezoneH + safezoneY;
			w = 0.0918668 * safezoneW;
			h = 0.0280066 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Return to main menu";
			action = "[]Spawn SERVICE_MENU_fnc_RETURN_MAIN";
		};


		class CLOSE_DIALOG_BUTTON_1613: Custom_RscButtonMenu
		{
			idc = 1613;
			x = 0.877963 * safezoneW + safezoneX;
			y = 0.110712 * safezoneH + safezoneY;
			w = 0.094491 * safezoneW;
			h = 0.0255 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Close this menu";
			action = "closeDialog 0";
		};
	
		
	};
};






