class CfgWeapons
{
        class RocketPods;      
        class w_master_arms_safe: RocketPods
        {
			    scope=1;
                CanLock=0;
                displayName="MASTER ARM - SAFE";
                displayNameMagazine="MASTER ARM - SAFE";
                shortNameMagazine="MASTER ARM - SAFE";
                nameSound="";
                cursor="EmptyCursor";
                cursorAim="EmptyCursor";
                magazines[] = {"FakeWeapon"};
				sounds[] = {};
                burst=0;
                reloadTime=0.0099999998;
                magazineReloadTime=0.1;
        };
};