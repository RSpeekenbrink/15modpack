/*
	Author: John_Spartan & Saul
        Edited by: Peral 
		Edited for AH1Z Cobra by: Genesi

	Description:
	Dynamic loadout selection/rearming function for "Your vehicle"

	Exucution:
	veh_dynamic_loadoutscript = [_veh,_station_1,_station_2,_station_3,_station_4,_station_5,_station_6,_station_7,_station_8,_station_9,_station_10,_station_11] execvm "\path too function\dynamic_loadouts.sqf";
	via unit init line in editor or via instance of mission script [INIT.sqf for example]
	
	Parameter(s):
		_this select 0: mode (Scalar)
		
		0: plane/object
		1: magazine classname to be equipped on station 1 ["my_magazine_classname"] 
		2: magazine classname to be equipped on station 2 ["my_magazine_classname"]
		3: magazine classname to be equipped on station 3 ["my_magazine_classname"]
		4: magazine classname to be equipped on station 4 ["my_magazine_classname"]
		5: magazine classname to be equipped on station 5 ["my_magazine_classname"]
		6: magazine classname to be equipped on station 6 ["my_magazine_classname"]
		7: magazine classname to be equipped on station 7 ["my_magazine_classname"]
		8: magazine classname to be equipped on station 8 ["my_magazine_classname"]
		9: magazine classname to be equipped on station 9 ["my_magazine_classname"]
		10: magazine classname to be equipped on station 10 ["my_magazine_classname"]
		11: magazine classname to be equipped on station 11 ["my_magazine_classname"]


	Returns: nothing
	Result: rearmed with desiered loadout

*/



//BASIC DEFINITIONS
_veh = _this select 0;		        //name of the unit we are playing with
_veh_pilot = driver _veh;		//pilot
_default_sleep_time = 2;		//actual time to wait/delay before loading every next magazine
_sleep_time = 3;			//time to wait/delay before loading every next magazine
_veh_stop_speed = 1;			//maximum speed of vehicle during the rearming 
_x = 1;					//first magazine index in array passed to this function
_w = 20;				//total number of weapon proxies on model


//WEAPON CLASSNAME DEFINITIONS
//LIST OF ALL POSSIBLE WEAPONS USED WITH THIS PLANE
//IN CASE WE NEED TO ADAPT THIS TO SOME OTHER MOD
_Your_weapon = "Weapon Name"; 



//MAGAZINE CLASSNAME DEFINITIONS
//LIST OF ALL POSSIBLE MAGAZINES USED WITH THIS PLANE
//IN CASE WE NEED TO ADAPT THIS TO SOME OTHER MOD
_EMPTY_magazine = "m_empty"; 
_EMPTY_FAKE_magazine = "m_fake_empty";



//REARMING SWITCH, CAN BE SUBSTITUDED BY VARIABLE, USED TO PREVENT
//MULTIPLE INSTANCES OF SAME FUNCTION AT ONCE
WaitUntil {(_veh animationPhase "rearming_done_switch") == 1};



//SWITCH PILOTS WEAPON TO SAFE
_veh selectWeapon _SAFE_weapon;
_veh animate ["rearming_done_switch",0];



//CHECK FOR PLANE SPEED, IF IN MOTION WE CANCEL REARMING WITH PENALTY
if (speed _veh > _veh_stop_speed) exitWith {veh_cancel_rearm_penalty = _veh execVM "\path too function\empty.sqf";if (player == _veh_pilot) then {titleText ["Rearming canceled...", "PLAIN DOWN",0.3];};};
if (player == _veh_pilot) then {titleText ["Rearming...", "PLAIN DOWN",0.3];};



//ROMEVE ALL POSSIBLE DEFAULT WEAPONS
_veh removeWeapon _Your_weapon; 


//ROMEVE ALL POSSIBLE DEFAULT MAGAZINES
_veh removeMagazines _Your_magazine;
_veh removeMagazines _EMPTY_magazine; 
_veh removeMagazines _EMPTY_FAKE_magazine;


//ADDING NEW MAGAZINES TO PLANE 
//ONLY STATIONS 1-11 ARE USED FOR CONVENTIONAL LOADOUTS
//replace "path to function" with actual path and F18
for "_i" from 1 to 20 do //or however many slots you have
{
	 
	_veh removeMagazines _EMPTY_FAKE_magazine; 
	_new_magazine = _this select _x;
	_veh addMagazine _new_magazine;
	for "_y" from 1 to _w do {_veh addMagazine _EMPTY_FAKE_magazine;};
	if (_new_magazine == _EMPTY_magazine) then {_sleep_time = 0;} else {_sleep_time = _default_sleep_time;}; 
	if (speed _veh > _veh_stop_speed) exitWith {veh_cancel_rearm_penalty = _veh execVM "\path too function\empty.sqf";if (player == _veh_pilot) then {titleText ["Rearming canceled...", "PLAIN DOWN",0.3];};};
	if (player == _veh_pilot) then {titleText ["Rearming...", "PLAIN DOWN",0.3];};
	_x= _x +1;
	_w= _w -1;
	sleep _sleep_time;
	//hintsilent format["Station Number: %1\nNew magazine: %2\nMax proxies: %3\nSleep time: %4\n",_station_number,_veh_new_magazine_station,_max_proxies,_sleep_time];

};



//BLANKING OUT REMAINING 11 EWP LOADOUT COMPATIBLE PROXIES
//_veh removeMagazines _EMPTY_FAKE_magazine; 
//for "_z" from 1 to 20 do {_veh addMagazine _EMPTY_magazine;};



//ADD NEW WEAPONS FOR PRELOADED MAGAZINES
_loadout = magazines _veh;
sleep 0.5;
if ((_yourmaghere in _loadout)) then {_veh addWeapon "YOUR WEAPON HERE";};
if ((_yourmaghere in _loadout)) then {_veh addWeapon "YOUR WEAPON HERE";};


//CHECK FOR PLANE SPEED, IF IN MOTION WE CANCEL REARMING WITH PENALTY
if (speed _veh > _veh_stop_speed) exitWith {veh_cancel_rearm_penalty = _veh execVM "\path too function\empty.sqf";if (player == _veh_pilot) then {titleText ["Rearming canceled...", "PLAIN DOWN",0.3];};};
if (player == _veh_pilot) then {titleText ["Rearming...", "PLAIN DOWN",0.3];};



//FINALIZING LOADOUT 
_veh setVehicleAmmo 1;
_veh selectWeapon _SAFE_weapon;
_veh animate ["rearming_done_switch",1];
if (player == _veh_pilot) then {titleText ["Rearming complete...", "PLAIN DOWN",0.6];};
