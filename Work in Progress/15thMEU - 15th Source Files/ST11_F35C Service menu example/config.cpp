#include "CfgService_dialog.hpp"
#include "CfgFunctions.hpp"

class AnimationSources
{


		class service_menu_switch 
		 {
		 source = "user";
		 animPeriod = 0.0001;
		 initPhase = 0;
		 };
		 class rearming_done_switch 
		 {
		 source = "user";
		 animPeriod = 0.0001;
		 initPhase = 0;
		 };
		 class paint_scheme_switch
	     {
		 source = "user";
		 animPeriod = 0.0001;
		 initPhase = 0;
	     };
};

class UserActions
{
			class service_menu
			{
				priority = 10;
				displayName = "Service Menu";
				position = "pilotcontrol";
				onlyforplayer=1;
				showWindow = 0;
		       	shortcut="User4";
				hideOnUse = 1;
				radius = 5;
				condition = "((this distance (nearestObject [this, ""B_Truck_01_ammo_F""]) < 25) and (damage (nearestObject [this, ""B_Truck_01_ammo_F""]) < 1) and player == driver this and this animationPhase ""service_menu_switch"" < 0.5 and speed this < 1) or ((this distance (nearestObject [this, ""Land_TentHangar_V1_F""]) < 55) and (damage (nearestObject [this, ""Land_TentHangar_V1_F""]) < 1) and player == driver this and this animationPhase ""service_menu_switch"" < 0.5 and speed this < 1) or ((this distance (nearestObject [this, ""Land_Hangar_F""]) < 55) and (damage (nearestObject [this, ""Land_Hangar_F""]) < 1) and player == driver this and this animationPhase ""service_menu_switch"" < 0.5 and speed this < 1)";
				statement = "[] spawn SERVICE_MENU_fnc_MAIN_UI;";
			};
};