class CfgMagazines 
{
	class VehicleMagazine;	
	class m_empty : VehicleMagazine 
	{
		scope = 2;
		displayName = "";
		count = 1;
		ammo = "a_empty";
		initSpeed = 0;
		sound[] = {};
		reloadSound[] = {};
		nameSound = "";
	};
	class m_fake_empty : VehicleMagazine 
	{
		scope = 2;
		displayName = "";
		count = 1;
		ammo = "a_empty";
		initSpeed = 0;
		sound[] = {};
		reloadSound[] = {};
		nameSound = "";
	};
};