class IGUIBack;			
class RscFrame;			
class RscPicture;		
class RscCombo;			
class RscButtonMenu;		
class RscObject;		
class RscText;			
class RscTextCheckBox;	    
class RscCheckbox;	   
class RscHTML;
class RscShortcutButton;

class Custom_RscButtonMenu : RscButtonMenu 
{

	animTextureNormal = "\path to paa\UI\GUI_main_button_normal_ca.paa";
	animTextureDisabled = "path to paa\UI\GUI_main_button_disabled_ca.paa";
	animTextureOver = "\path to paa\UI\GUI_main_button_over_ca.paa";
	animTextureFocused = "\path to paa\UI\GUI_main_button_focus_ca.paa";
	animTexturePressed = "\path to paa\UI\GUI_main_button_down_ca.paa";
	animTextureDefault = "\path to paa\UI\GUI_main_button_default_ca.paa";
	colorBackground[] = {0, 0, 0, 0.8};
	colorBackground2[] = {1, 1, 1, 0.5};
	color[] = {1, 1, 1, 1};
	color2[] = {1, 1, 1, 1};
	colorText[] = {1, 1, 1, 1};
	colorDisabled[] = {1, 1, 1, 0.25};
	
};
class Custom_RscCheckbox : RscCheckbox 
{

	animTextureNormal = "\path to paa\UI\GUI_main_button_normal_ca.paa";
	animTextureDisabled = "\path to paa\UI\GUI_main_button_disabled_ca.paa";
	animTextureOver = "\path to paa\UI\GUI_main_button_over_ca.paa";
	animTextureFocused = "\path to paa\UI\GUI_main_button_focus_ca.paa";
	animTexturePressed = "\path to paa\UI\GUI_main_button_down_ca.paa";
	animTextureDefault = "\path to paa\UI\GUI_main_button_default_ca.paa";
	colorBackground[] = {0, 0, 0, 0.8};
	colorBackground2[] = {1, 1, 1, 0.5};
	color[] = {1, 1, 1, 1};
	color2[] = {1, 1, 1, 1};
	colorText[] = {1, 1, 1, 1};
	colorDisabled[] = {1, 1, 1, 0.25};
	
};