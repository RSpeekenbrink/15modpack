class LOADOUTS_UI 
{
	idd = 3010;
	movingenable = "false";
	onLoad = "uiNamespace setVariable ['myDisplay', (_this select 0)]";
	
	class Controls 
	{
		class MAIN_BOX_1200 : RscPicture 
		{
			idc = 1200;
			x = safezoneX;
			y = safezoneY;
			w = 0.1 + safezoneW;
			h = 0.1 + safezoneH;
			text = "\path to paa\UI\GUI_background.paa";
		};
		class MAIN_PIC_LOADOUTS: RscPicture 
		{
			idc = 1205;
			text = "\path to paa\UI\GUI_main_loadouts.paa";
			x = 0 * safezoneW + safezoneX;
			y = 0.052 * safezoneH + safezoneY;
			w = 1 * safezoneW;
			h = 0.84 * safezoneH;
		};
		class MAIN_FRAME: RscFrame
		{
			idc = 1204;
			x = 0.005 * safezoneW + safezoneX;
			y = 0.005 * safezoneH + safezoneY;
			w = 0.99 * safezoneW;
			h = 0.99 * safezoneH;
		};
		class LOAD_CUSTOM_BUTTON_1600: Custom_RscButtonMenu
		{
			idc = 1600;
			x = 0.422572 * safezoneW + safezoneX;
			y = 0.634429 * safezoneH + safezoneY;
			w = 0.1490 * safezoneW;
			h = 0.0898 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Apply custom loadout";
			action = "[]Spawn SERVICE_MENU_fnc_LOADOUT_APPLY";
		};
		class STATION_1_COMBO_3101: RscCombo
		{
			idc = 3101;
			x = 0.741475 * safezoneW + safezoneX;
			y = 0.36277 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 1";
		};
		class STATION_2_COMBO_3102: RscCombo
		{
			idc = 3102;
			x = 0.687668 * safezoneW + safezoneX;
			y = 0.404779 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 2";
		};
		class STATION_3_COMBO_3103: RscCombo
		{
			idc = 3103;
			x = 0.643049 * safezoneW + safezoneX;
			y = 0.443988 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 3";
		};
		class STATION_4_COMBO_3104: RscCombo
		{
			idc = 3104;
			x = 0.528873 * safezoneW + safezoneX;
			y = 0.488798 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 4";
		};
		class STATION_5_COMBO_3105: RscCombo
		{
			idc = 3105;
			x = 0.454067 * safezoneW + safezoneX;
			y = 0.536409 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 5";
		};
		class STATION_6_COMBO_3106: RscCombo
		{
			idc = 3106;
			x = 0.454067 * safezoneW + safezoneX;
			y = 0.387975 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 6";
		};
		class STATION_7_COMBO_3107: RscCombo
		{
			idc = 3107;
			x = 0.454067 * safezoneW + safezoneX;
			y = 0.581218 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 7";
		};
		class STATION_8_COMBO_3108: RscCombo
		{
			idc = 3108;
			x = 0.380577 * safezoneW + safezoneX;
			y = 0.488798 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 8";
		};
		class STATION_9_COMBO_3109: RscCombo
		{
			idc = 3109;
			x = 0.263773 * safezoneW + safezoneX;
			y = 0.443988 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 9";
		};
		class STATION_10_COMBO_3110: RscCombo
		{
			idc = 3110;
			x = 0.212591 * safezoneW + safezoneX;
			y = 0.401978 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 10";
		};
		class STATION_11_COMBO_3111: RscCombo
		{
			idc = 3111;
			x = 0.160096 * safezoneW + safezoneX;
			y = 0.362768 * safezoneH + safezoneY;
			w = 0.0879288 * safezoneW;
			h = 0.0280062 * safezoneH;
			tooltip = "Weapon station 11";
		};
		

		class A10C_REARM_BUTTON_1601: Custom_RscButtonMenu
		{
			idc = 1601;
			x = 0.160096 * safezoneW + safezoneX;
			y = 0.743653 * safezoneH + safezoneY;
			w = 0.0813676 * safezoneW;
			h = 0.137235 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Rearm aircraft";
			action = "[]Spawn SERVICE_MENU_fnc_REARM";
		};
		class REPAIR_BUTTON_1602: Custom_RscButtonMenu
		{
			idc = 1602;
			x = 0.257212 * safezoneW + safezoneX;
			y = 0.743653 * safezoneH + safezoneY;
			w = 0.0813676 * safezoneW;
			h = 0.137235 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Repair aircraft";
			action = "[]Spawn SERVICE_MENU_fnc_REPAIR";
		};
		
		class REFUEL_BUTTON_1603: Custom_RscButtonMenu
		{
			idc = 1603;
			x = 0.356953 * safezoneW + safezoneX;
			y = 0.743653 * safezoneH + safezoneY;
			w = 0.0813676 * safezoneW;
			h = 0.137235 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Refuel aircraft";
			action = "[]Spawn SERVICE_MENU_fnc_REFUEL";
		};
		
		class 1_LOADOUT_BUTTON_1604: Custom_RscButtonMenu
		{
			idc = 1604;
			x = 0.552495 * safezoneW + safezoneX;
			y = 0.743659 * safezoneH + safezoneY;
			w = 0.0813676 * safezoneW;
			h = 0.137235 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Custom Loadout 1";
			action = "[]Spawn SERVICE_MENU_fnc_LOADOUT_1";
		};

		class 2_LOADOUT_BUTTON_1605: Custom_RscButtonMenu
		{
			idc = 1605;
			x = 0.649609 * safezoneW + safezoneX;
			y = 0.743655 * safezoneH + safezoneY;
			w = 0.0813676 * safezoneW;
			h = 0.137235 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Custom Loadout 2";
			action = "[]Spawn SERVICE_MENU_fnc_LOADOUT_2";
		};

		class 3_LOADOUT_BUTTON_1606: Custom_RscButtonMenu
		{
			idc = 1606;
			x = 0.455379 * safezoneW + safezoneX;
			y = 0.743653 * safezoneH + safezoneY;
			w = 0.0813676 * safezoneW;
			h = 0.137235 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Custom Loadout 3";
			action = "[]Spawn SERVICE_MENU_fnc_LOADOUT_3";
		};
		
		class EMPTY_LOADOUT_BUTTON_1607: Custom_RscButtonMenu
		{
			idc = 1607;
			x = 0.748036 * safezoneW + safezoneX;
			y = 0.743653 * safezoneH + safezoneY;
			w = 0.0813676 * safezoneW;
			h = 0.137235 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Empty loadout, no weapons";
			action = "[]Spawn SERVICE_MENU_fnc_LOADOUT_EMPTY";
		};

		class RETURN_TO_MAIN_BUTTON_1608: Custom_RscButtonMenu
		{
			idc = 1608;
			x = 0.787409 * safezoneW + safezoneX;
			y = 0.110712 * safezoneH + safezoneY;
			w = 0.0918668 * safezoneW;
			h = 0.0280066 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Return to main menu";
			action = "[]Spawn SERVICE_MENU_fnc_RETURN_MAIN";
		};

		class CLOSE_DIALOG_BUTTON_1609: Custom_RscButtonMenu
		{
			idc = 1609;
			x = 0.877963 * safezoneW + safezoneX;
			y = 0.110712 * safezoneH + safezoneY;
			w = 0.094491 * safezoneW;
			h = 0.0255 * safezoneH;
			text = "";
			size = 0.023;
			tooltip = "Close this menu";
			action = "closeDialog 0";
		};
	
		
	};
};
