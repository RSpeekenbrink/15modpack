class defaultEventhandlers;
class CfgPatches
{
	class 15thrhsacefix
	{
		units[] = {"rhsusf_CH53E_USMCedit","RHS_UH1Yedit","RHS_AH1Z_wdedit"};
		weapons[] = {};
		author[] = {"15thMEU"};
		requiredVersion = 1.32;
		requiredAddons[] = {"A3_Weapons_F","rhsusf_c_ch53"};
	};
};
class CfgFunctions
{
	class RHS
	{
		tag = "RHS";
		class functions
		{
			class 15thuh1_toggleCam
			{
				file = "meu_rhs_aircraft\scripts\uh1_toggleCam.sqf";
				description = "UH1Y monitor toggle";
			};
		};
	};
};
class RCWSOptics;
class CfgVehicles
{
	// Fix for RHS AH1, UH1 and CH53
	class Air;
	class Helicopter: Air
	{
		class Turrets;
		class HitPoints
		{
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitGlass6;
			class HitHull;
			class HitEngine;
			class HitAvionics;
		};
	};
	class Helicopter_Base_F: Helicopter
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
		class HitPoints: HitPoints
		{
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitGlass6;
			class HitMissiles;
			class HitHull;
			class HitEngine;
			class HitAvionics;
			class HitVRotor;
			class HitHRotor;
		};
		class AnimationSources;
		class Eventhandlers;
		class CargoTurret;
		class ViewOptics;
	};
	class Heli_Attack_01_base_F: Helicopter_Base_F
	{
		class HitPoints: HitPoints
		{
			class HitHull;
			class HitFuel;
			class HitAvionics;
			class HitMissiles;
			class HitEngine1;
			class HitEngine2;
			class HitEngine;
			class HitHRotor;
			class HitVRotor;
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitGlass6;
			class HitGlass7;
			class HitGlass8;
		};
		class Sounds;
	};
	class Helicopter_Base_H: Helicopter_Base_F
	{
		class Turrets: Turrets
		{
			class CopilotTurret;
		};
		class AnimationSources;
		class Eventhandlers;
		class Viewoptics;
		class Reflectors
		{
			class Right;
		};
		class SoundsExt;
	};
	class Heli_Transport_01_base_F: Helicopter_Base_H
	{
		class Sounds;
		class SoundsExt: SoundsExt
		{
			class Sounds;
		};
	};
	class Heli_Transport_02_base_F: Helicopter_Base_H
	{
	};
	class Heli_Transport_03_base_F: Helicopter_Base_H
	{
	};
	class Heli_Light_03_base_F: Helicopter_Base_F
	{
		class ViewPilot;
	};
	class HelicopterWreck;
	class ThingX;
	class RHS_AH1Z_base: Heli_Attack_01_base_F {};
	class RHS_AH1Z: RHS_AH1Z_base {};
	class RHS_AH1Z_wd: RHS_AH1Z {};
	class RHS_AH1Z_wdedit: RHS_AH1Z_wd
	{
	displayName = "15th RHS AH-1Z";
	faction = "rhs_faction_usmc_wd";
	vehicleClass = "Air";
	scope = 2;
	crew = "rhsusf_usmc_marpat_wd_helipilot";
	typicalCargo[] = {"rhsusf_usmc_marpat_wd_helicrew"};
	driverCanEject = 1;
	weapons[] = {"ah1safe","rhs_weap_FFARLauncher","CMFlareLauncher"};
	magazines[]= {"rhs_mag_M151_38","240Rnd_CMFlare_Chaff_Magazine"};
		LockDetectionSystem="1 + 2 + 8 + 4";
		incomingMissileDetectionSystem=16;
		radarType=4; // Air radar
		gunnerCanSee="1+2+4+8+16";
		driverCanSee="1+2+4+8+16";
		class Eventhandlers: defaultEventhandlers
		{
			fired = "_this spawn RWCO_fnc_hellfire";
		};
		class UserActions
		{
			class SAFEMODE
			{
				displayName=;
				condition="";
				statement="";
				position="";
				radius=10;
				priority=10.5;
				onlyforplayer=1;
				showWindow=0;
				shortcut="";
				hideOnUse=1;
			};
			class trajLOALHI
			{
				displayName = "Lock-on After Launch, High (LOAL-HI)";
				position = "pos_gunner";
				radius = 15;
				showwindow = 0;
				condition = "(player==gunner this) && ((this getvariable ['Mode',0]) != 3)";
				statement = "this setVariable ['Mode', 3, false];";
				onlyforplayer = 1;
			};
			class trajLOALLO
			{
				displayName = "Lock-on After Launch, Low (LOAL-LO)";
				position = "pos_gunner";
				radius = 15;
				showwindow = 0;
				condition = "(player==gunner this) && ((this getvariable ['Mode',0]) != 2)";
				statement = "this setVariable ['Mode', 2, false];";
				onlyforplayer = 1;
			};
			class trajLOALDIR
			{
				displayName = "Lock-on After Launch, Direct (LOAL-DIR)";
				position = "pos_gunner";
				radius = 15;
				showwindow = 0;
				condition = "(player==gunner this) && ((this getvariable ['Mode',0]) != 1)";
				statement = "this setVariable ['Mode', 1, false];";
				onlyforplayer = 1;
			};
			class trajLOBL
			{
				displayName = "Lock-on Before Launch (LOBL)";
				position = "pos_gunner";
				radius = 15;
				showwindow = 0;
				condition = "(player==gunner this) && ((this getvariable ['Mode',0]) != 0)";
				statement = "this setVariable ['Mode', 0, false];";
				onlyforplayer = 1;
			};
			class remote
			{
				displayName = "Remote";
				position = "pos_gunner";
				radius = 15;
				showwindow = 0;
				condition = "(player==gunner this)";
				statement = "this setVariable ['TargetMode', 1, false];";
				onlyforplayer = 1;
			};
			class self
			{
				displayName = "Self";
				position = "pos_gunner";
				radius = 15;
				showwindow = 0;
				condition = "(player==gunner this)";
				statement = "this setVariable ['TargetMode', 0, false];";
				onlyforplayer = 1;
			};
			class broadcast
			{
				displayName = "Broadcast";
				position = "pos_gunner";
				radius = 15;
				showwindow = 0;
				condition = "(player==gunner this)";
				statement = "this execVM '\RWCO_Main\scripts\userActions\broadcast_laser.sqf';";
				onlyforplayer = 1;
			};
		};
		class pilotCamera
		{
			turretInfoType = "RscOptics_UAV_gunner";
			showHMD = 0;
			stabilizedInAxes = 3;
			minElev = -20;
			maxElev = 90;
			initElev = 0;
			minTurn = -110;
			maxTurn = 110;
			initTurn = 0;
			maxXRotSpeed = 0.5;
			maxYRotSpeed = 0.5;
			pilotOpticsShowCursor = 1;
			controllable = 1;
			class OpticsIn
			{
				class Wide
				{
					opticsDisplayName = "W";
					initAngleX = 0;
					minAngleX = -30;
					maxAngleX = 30;
					initAngleY = 0;
					minAngleY = -100;
					maxAngleY = 100;
					initFov = 0.1;
					minFov = 0.0022;
					maxFov = 1.1;
					directionStabilized = 1;
					visionMode[] = {"Normal","NVG","TI"};
					thermalMode[] = {0,1};
					gunnerOpticsModel = "\A3\Weapons_F_Beta\Reticle\Optics_Commander_01_F";
				};
				showMiniMapInOptics = 1;
				showUAVViewpInOptics = 0;
				showSlingLoadManagerInOptics = 1;
			};
		};
		memoryPointDriverOptics = "gunnerview";
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				ace_fcs_Enabled = 0;
				ace_laser_CanLockLaser = 0;
				ace_laser_selfdesignate_Enabled = 0;
				discretedistance[] = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000};
				discretedistanceinitindex = 3;
				isCopilot = 1;
				minElev = -60;
				maxElev = 10;
				initElev = 0;
				minTurn = -70;
				maxTurn = 70;
				initTurn = 0;
				weapons[] = {"ah1safe","rhs_weap_M197","rhs_weap_HellfireLauncher","rhs_weap_SidewinderLauncher","Laserdesignator_mounted"};
				magazines[] = {"rhs_mag_M197_750","rhs_mag_Sidewinder_heli_2","rhs_pod_hellfire_4","rhs_pod_hellfire_4","rhs_pod_FFAR_19_green","rhs_pod_FFAR_19_green","rhs_mag_Hellfire_8","rhs_pod_empty_8","Laserbatteries"};
				showHMD = 1;
				laser = 1;
				gunnerHasFlares = 1;
				gunnerCanEject=1;
				primaryGunner= 1;
				turretInfoType = "RHS_RscOptics_Heli_Attack_01_gunner";
				gunnerOpticsModel = "\rhsusf\addons\rhsusf_a2port_air\ah64\gunnerOptics_ah64";
				class OpticsIn
				{
					class Wide
					{
						opticsDisplayName = "W";
						initAngleX = 0;
						minAngleX = -30;
						maxAngleX = 30;
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = 100;
						initFov = 0.1;
						minFov = 0.0022;
						maxFov = 0.65;
						directionStabilized = 0;
						visionMode[] = {"Normal","NVG","Ti"};
						thermalMode[] = {0,1};
						gunnerOpticsModel = "\rhsusf\addons\rhsusf_a2port_air\ah64\gunnerOptics_ah64";
					};
				};
				class OpticsOut
				{
					class Monocular
					{
						initAngleX = 0;
						minAngleX = -30;
						maxAngleX = 30;
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = 100;
						initFov = 1.1;
						minFov = 0.133;
						maxFov = 1.1;
						visionMode[] = {"Normal","NVG"};
						gunnerOpticsModel = "";
						gunnerOpticsEffect[] = {};
					};
				};
			};
		};
	};
	class RHS_UH1_Base: Heli_Light_03_base_F {};
	class RHS_UH1Y_base: RHS_UH1_Base {};
	class RHS_UH1Y_US_base: RHS_UH1Y_base {};
	class RHS_UH1Y: RHS_UH1Y_US_base {};
	class RHS_UH1Yedit: RHS_UH1Y
	{
		faction = "rhs_faction_usmc_wd";
		scope = 2;
		vehicleClass = "Air";
		displayName = "15th RHS UH-1Y";
		LockDetectionSystem = "1 + 2 + 8 + 4";
		incomingMissileDetectionSystem = 16;
		radarType = 4; // Air radar
		commanderCanSee="1+2+4+8+16";
		gunnerCanSee="1+2+4+8+16";
		driverCanSee="1+2+4+8+16";
		cargoCanEject = 1;
		ace_refuel_fuelCapacity = 1447;
		ace_fastroping_enabled = 2;
        ace_fastroping_friesType = "ACE_friesAnchorBar";
        ace_fastroping_friesAttachmentPoint[] = {0, 2.38, -0.135};
        ace_fastroping_onCut = "ace_fastroping_fnc_onCutCommon";
	    ace_fastroping_onPrepare = "ace_fastroping_fnc_onPrepareCommon";
        ace_fastroping_ropeOrigins[] = {"ropeOriginLeft", "ropeOriginRight"};
		driverCanEject = 1;
		helmetMountedDisplay = 1;
		weapons[] = {"uh1safe","rhs_weap_FFARLauncher","CMFlareLauncher"};
		magazines[] = {"240Rnd_CMFlare_Chaff_Magazine","rhs_pod_FFAR_7_green","rhs_pod_FFAR_7_green","rhs_mag_M151_14","rhs_19_pod_empty","rhs_19_pod_empty"};
        	class pilotCamera
		{
			turretInfoType = "RscOptics_UAV_gunner";
			showHMD = 0;
			stabilizedInAxes = 3;
			minElev = -20;
			maxElev = 90;
			initElev = 0;
			minTurn = -110;
			maxTurn = 110;
			initTurn = 0;
			maxXRotSpeed = 0.5;
			maxYRotSpeed = 0.5;
			pilotOpticsShowCursor = 1;
			controllable = 1;
			class OpticsIn
			{
				class Wide
				{
					opticsDisplayName = "W";
					initAngleX = 0;
					minAngleX = -30;
					maxAngleX = 30;
					initAngleY = 0;
					minAngleY = -100;
					maxAngleY = 100;
					initFov = 0.1;
					minFov = 0.0022;
					maxFov = 1.1;
					directionStabilized = 1;
					visionMode[] = {"Normal","NVG","TI"};
					thermalMode[] = {0,1};
					gunnerOpticsModel = "\A3\Weapons_F_Beta\Reticle\Optics_Commander_01_F";
				};
				showMiniMapInOptics = 1;
				showUAVViewpInOptics = 0;
				showSlingLoadManagerInOptics = 1;
			};
		};
		memoryPointDriverOptics = "gun_end";
		class UserActions
		{		
			class HUDoff
			{
				displayName = "HUD on";
				displayNameDefault = "HUD on";
				position = "zamerny";
				radius = 1;
				onlyForPlayer = 1;
				condition = "(player==driver this)and(this animationphase 'HUDAction' !=1)";
				statement = "this animate ['HUDAction',1];this animate ['HUDAction_1',1]";
			};
			class HUDon: HUDoff
			{
				displayName = "HUD off";
				displayNameDefault = "HUD off";
				condition = "(player==driver this)and(this animationphase 'HUDAction' !=0)";
				statement = "this animate ['HUDAction',0];this animate ['HUDAction_1',0]";
			};	
			class TogglePIP: HUDoff
			{
				displayName = "Toggle monitor";
				displayNameDefault = "Toggle monitor";
				condition = "( (call rhsusf_fnc_findPlayer)==driver this) or ((call rhsusf_fnc_findPlayer)==this turretUnit [0]) ";
				statement = "call rhs_fnc_15thuh1_toggleCam";
			};
			
		};
		class Turrets: Turrets
		{
			class CopilotTurret: MainTurret
			{
				primaryGunner = 1;
				playerPosition = 0;
				isCopilot = 1;
				usePiP = 1;
				gunnerHasFlares = 1;
				gunnerCanEject=1;
				showHMD = 1;
				gunnerName = "Observer";
				weapons[] = {"Laserdesignator_mounted"};
				magazines[] = {"Laserbatteries"};
				gunnerAction = "RHS_UH1Y_Gunner02";
				gunnerInAction = "RHS_UH1Y_Gunner02";
				body = "ObsTurret";
				gun = "ObsGun";
				animationSourceBody = "ObsTurret";
				animationSourceGun = "ObsGun";
				gunBeg = "gun_end";
				gunEnd = "gun_begin";
				memoryPointGun = "gun_end";
				memoryPointGunnerOptics = "gun_end";
				turretInfoType = "RscOptics_UAV_gunner";
				initElev = 0;
				minElev = -80;
				maxElev = 30;
				minTurn = -180;
				maxTurn = 180;
				CanEject = 1;
				memoryPointsGetInGunner = "pos codriver";
				memoryPointsGetInGunnerDir = "pos codriver dir";
				gunnerGetInAction = "copilot_Heli_Light_02_Enter";
				gunnerGetOutAction = "copilot_Heli_Light_02_Exit";
				selectionFireAnim = "";
				preciseGetInOut = 1;
				GunnerDoor = "DoorL";
				gunnerLeftHandAnimName = "lever_copilot";
				gunnerRightHandAnimName = "stick_copilot";
				proxyIndex = 3;
				commanding = -2;
				laser = 1;
				class OpticsIn
				{
					class Wide
					{
						opticsDisplayName = "W";
						initAngleX = 0;
						minAngleX = -30;
						maxAngleX = 30;
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = 100;
						initFov = 0.1;
						minFov = 0.0022;
						maxFov = 0.65;
						visionMode[] = {"Normal","NVG","Ti"};
						directionStabilized = 1;
						stabilizedInAxes = 3;
						thermalMode[] = {0,1};
						gunnerOpticsModel = "\A3\Weapons_F_Beta\Reticle\Optics_Commander_01_F";
					};
				};
				class OpticsOut
				{
					class Monocular
					{
						initAngleX = 0;
						minAngleX = -30;
						maxAngleX = 30;
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = 100;
						initFov = 1.1;
						minFov = 0.33;
						maxFov = 1.1;
						visionMode[] = {"Normal","NVG"};
						gunnerOpticsModel = "";
						gunnerOpticsEffect[] = {};
					};
				};
				class Reflectors{};
			};
			class MainTurret: MainTurret
			{
				CanEject = 1;
				isCopilot = 0;
				animationSourceBody = "mainTurret";
				animationSourceGun = "mainGun";
				body = "mainTurret";
				gun = "mainGun";
				minElev = -60;
				maxElev = 30;
				initElev = 0;
				minTurn = -7;
				maxTurn = 183;
				initTurn = 0;
				soundServo[] = {"",0.01,1};
				gunnerLeftHandAnimName = "";
				gunnerRightHandAnimName = "";
				gunnerLeftLegAnimName = "";
				gunnerRightLegAnimName = "";
				animationSourceHatch = "";
				stabilizedInAxes = "StabilizedInAxesNone";
				gunBeg = "muzzle_1";
				gunEnd = "chamber_1";
				selectionFireAnim = "zasleh";
				weapons[] = {"rhs_weap_m134_minigun_1"};
				magazines[] = {"5000Rnd_762x51_Belt"};
				gunnerName = "Left door gunner";
				gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
				gunnerOutOpticsShowCursor = 1;
				gunnerOpticsShowCursor = 1;
				gunnerAction = "RHS_UH1Y_Gunner";
				gunnerInAction = "RHS_UH1Y_Gunner";
				gunnerGetInAction = "GetInHeli_Transport_01Cargo";
				gunnerGetOutAction = "RHS_HIND_Cargo_Exit";
				preciseGetInOut = 0;
				commanding = -3;
				playerPosition = 3;
				primaryGunner = 0;
				class ViewOptics
				{
					initAngleX = 0;
					minAngleX = -30;
					maxAngleX = 30;
					initAngleY = 0;
					minAngleY = -100;
					maxAngleY = 100;
					initFov = 0.7;
					minFov = 0.25;
					maxFov = 1.1;
				};
				class ViewGunner: ViewOptics
				{
					initAngleX = -15;
					minAngleX = -45;
					maxAngleX = 45;
					initFov = 0.75;
					minFov = 0.375;
					maxFov = 0.75;
					visionMode[] = {};
				};
				class OpticsIn
				{
					class ViewOptics: ViewGunner
					{
						gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
						gunnerOutOpticsModel = "\A3\weapons_f\reticle\optics_empty";
					};
				};
				gunnerCompartments = "Compartment2";
				memoryPointGun = "machinegun";
				memoryPointGunnerOptics = "gunnerview";
				soundAttenuationTurret = "HeliAttenuationGunner";
				class Reflectors{};
			};
			class RightDoorGun: MainTurret
			{
				CanEject = 1;
				body = "Turret_2";
				gun = "Gun_2";
				animationSourceBody = "Turret_2";
				animationSourceGun = "Gun_2";
				weapons[] = {"rhs_weap_m134_minigun_2"};
				stabilizedInAxes = "StabilizedInAxesNone";
				selectionFireAnim = "zasleh_1";
				proxyIndex = 2;
				playerPosition = 1;
				gunnerName = "Right door gunner";
				commanding = -3;
				minElev = -60;
				maxElev = 30;
				initElev = 0;
				minTurn = -183;
				maxTurn = 7;
				initTurn = 0;
				gunBeg = "muzzle_2";
				gunEnd = "chamber_2";
				primaryGunner = 0;
				memoryPointGun = "machinegun_1";
				memoryPointGunnerOptics = "gunnerview_2";
			};
			class CargoTurret_01: CargoTurret
			{
				gunnerAction = "passenger_inside_2";
				gunnerGetInAction = "GetInHeli_Transport_01Cargo";
				gunnerGetOutAction = "RHS_HIND_Cargo_Exit";
				memoryPointsGetInGunner = "pos cargo R";
				memoryPointsGetInGunnerDir = "pos cargo R dir";
				gunnerName = "Passenger (Right Bench 1)";
				gunnerCompartments = "Compartment2";
				memoryPointGunnerOptics = "";
				proxyIndex = 1;
				maxElev = 15;
				minElev = -45;
				maxTurn = -48;
				minTurn = -84;
				isPersonTurret = 1;
				gunnerUsesPilotView = 1;
				selectionFireAnim = "";
				gunnerCanEject = 1;
				cantCreateAI = 1;
				commanding = -1;
				playerPosition = 2;
				soundAttenuationTurret = "HeliAttenuationRamp";
				disableSoundAttenuation = 0;
			};
			class CargoTurret_02: CargoTurret_01
			{
				gunnerName = "Passenger (Right Bench 2)";
				memoryPointsGetInGunner = "pos cargo R4";
				memoryPointsGetInGunnerDir = "pos cargo R4 dir";
				proxyIndex = 3;
				maxTurn = 47;
				minTurn = 22;
			};
			class CargoTurret_03: CargoTurret_01
			{
				gunnerName = "Passenger (Right Bench 3)";
				memoryPointsGetInGunner = "pos cargo R2";
				memoryPointsGetInGunnerDir = "pos cargo R2 dir";
				proxyIndex = 6;
				maxTurn = 70;
				minTurn = 22;
			};
			class CargoTurret_04: CargoTurret_01
			{
				memoryPointsGetInGunner = "pos cargo L";
				memoryPointsGetInGunnerDir = "pos cargo L dir";
				gunnerName = "Passenger (Left Bench 1)";
				proxyIndex = 2;
				maxTurn = 84;
				minTurn = 58;
			};
			class CargoTurret_05: CargoTurret_04
			{
				gunnerName = "Passenger (Left Bench 2)";
				memoryPointsGetInGunner = "pos cargo L4";
				memoryPointsGetInGunnerDir = "pos cargo L4 dir";
				proxyIndex = 4;
				maxTurn = -16;
				minTurn = -40;
			};
			class CargoTurret_06: CargoTurret_04
			{
				gunnerName = "Passenger (Left Bench 3)";
				memoryPointsGetInGunner = "pos cargo L2";
				memoryPointsGetInGunnerDir = "pos cargo L2 dir";
				proxyIndex = 7;
				maxTurn = 3;
				minTurn = -35;
			};
		};
		class MarkerLights
		{
			class WhiteStill
			{
				name = "bily pozicni";
				color[] = {1,1,1};
				ambient[] = {0.1,0.1,0.1};
				blinking = 1;
				intensity = 65;
				blinkingPattern[] = {0.1,0.9};
				blinkingPatternGuarantee = 0;
				drawLightSize = 0.2;
				drawLightCenterSize = 0.04;
			};
			class RedStill
			{
				name = "cerveny pozicni";
				color[] = {0.8,0,0};
				ambient[] = {0.08,0,0};
				intensity = 70;
				drawLight = 1;
				drawLightSize = 0.15;
				drawLightCenterSize = 0.04;
				activeLight = 0;
				blinking = 0;
				dayLight = 0;
				useFlare = 0;
			};
			class GreenStill: RedStill
			{
				name = "zeleny pozicni";
				color[] = {0,0.8,0};
				ambient[] = {0,0.08,0};
			};
			class RedBlinking
			{
				name = "bily pozicni blik";
				color[] = {0.9,0.15,0.1};
				ambient[] = {0.09,0.015,0.01};
				intensity = 65;
				blinking = 1;
				blinkingPattern[] = {0.1,0.9};
				blinkingPatternGuarantee = 0;
				drawLightSize = 0.2;
				drawLightCenterSize = 0.04;
			};
			class WhiteBlinking: RedBlinking
			{
				name = "cerveny pozicni blik";
				blinkingPattern[] = {0.2,1.3};
				drawLightSize = 0.25;
				drawLightCenterSize = 0.08;
			};
		};
class MFD
		{
			class AirplaneHUD
			{
				class Bones{};
				class Draw{};
				topLeft = "HUD_top_left";
				topRight = "HUD_top_right";
				bottomLeft = "HUD_bottom_left";
				borderLeft = 0;
				borderRight = 0;
				borderTop = 0;
				borderBottom = 0;
				color[] = {0.15,1,0.15,1};
				enableParallax = 0;
				helmetMountedDisplay = 1;
				helmetPosition[] = {0,0,0};
				helmetRight[] = {0,0,0};
				helmetDown[] = {0,0,0};
			};
			class Kimi_HUD_1
			{
				topLeft = "HUD_top_left";
				topRight = "HUD_top_right";
				bottomLeft = "HUD_bottom_left";
				borderLeft = 0;
				borderRight = 0;
				borderTop = 0;
				borderBottom = 0;
				color[] = {0.15,1,0.15,1};
				enableParallax = 0;
				class Bones
				{
					class GunnerAim
					{
						type = "vector";
						source = "weapon";
						pos0[] = {0.5,"0.9 - 0.04 + 0.012"};
						pos10[] = {"0.5 + 0.0111","0.9 - 0.04 + 0.012 + 0.0133"};
					};
					class Target
					{
						source = "target";
						type = "vector";
						pos0[] = {0.5,0.5};
						pos10[] = {0.85,0.85};
					};
					class Velocity
					{
						type = "vector";
						source = "velocity";
						pos0[] = {0.5,0.5};
						pos10[] = {0.65,0.65};
					};
					class Velocity_slip
					{
						type = "vector";
						source = "velocity";
						pos0[] = {0.5,0.845};
						pos10[] = {0.53,0.845};
					};
					class VspeedBone
					{
						type = "linear";
						source = "vspeed";
						sourceScale = 1;
						min = -10;
						max = 10;
						minPos[] = {0.93,0.2};
						maxPos[] = {0.93,0.8};
					};
					class RadarAltitudeBone
					{
						type = "linear";
						source = "altitudeAGL";
						sourceScale = 1;
						min = 0;
						max = 60;
						minPos[] = {0.965,0.2};
						maxPos[] = {0.965,0.8};
					};
					class HorizonBankRot
					{
						type = "rotational";
						source = "horizonBank";
						center[] = {0.5,0.5};
						min = -3.1416;
						max = 3.1416;
						minAngle = -180;
						maxAngle = 180;
						aspectRatio = 1;
					};
					class ForwardVec
					{
						type = "vector";
						source = "forward";
						pos0[] = {0,0};
						pos10[] = {0.25,0.25};
					};
					class WeaponAim
					{
						type = "vector";
						source = "weapon";
						pos0[] = {0.5,0.5};
						pos10[] = {0.75,0.75};
					};
					class Level0
					{
						type = "horizon";
						pos0[] = {0.5,0.5};
						pos10[] = {0.78,0.78};
						angle = 0;
					};
					class LevelP5: Level0
					{
						angle = 5;
					};
					class LevelM5: Level0
					{
						angle = -5;
					};
					class LevelP10: Level0
					{
						angle = 10;
					};
					class LevelM10: Level0
					{
						angle = -10;
					};
					class LevelP15: Level0
					{
						angle = 15;
					};
					class LevelM15: Level0
					{
						angle = -15;
					};
					class LevelP20: Level0
					{
						angle = 20;
					};
					class LevelM20: Level0
					{
						angle = -20;
					};
					class LevelP25: Level0
					{
						angle = 25;
					};
					class LevelM25: Level0
					{
						angle = -25;
					};
					class LevelP30: Level0
					{
						angle = 30;
					};
					class LevelM30: Level0
					{
						angle = -30;
					};
					class LevelP35: Level0
					{
						angle = 35;
					};
					class LevelM35: Level0
					{
						angle = -35;
					};
					class LevelP40: Level0
					{
						angle = 40;
					};
					class LevelM40: Level0
					{
						angle = -40;
					};
					class LevelP45: Level0
					{
						angle = 45;
					};
					class LevelM45: Level0
					{
						angle = -45;
					};
					class LevelP50: Level0
					{
						angle = 50;
					};
					class LevelM50: Level0
					{
						angle = -50;
					};
				};
				class Draw
				{
					color[] = {0.18,1,0.18};
					alpha = 1;
					condition = "on";
					class Horizont
					{
						clipTL[] = {0.15,0.15};
						clipBR[] = {0.85,0.85};
						class Dimmed
						{
							class Level0
							{
								type = "line";
								points[] = {{ "Level0",{ -0.42,0 },1 },{ "Level0",{ -0.38,0 },1 },{  },{ "Level0",{ -0.37,0 },1 },{ "Level0",{ -0.33,0 },1 },{  },{ "Level0",{ -0.32,0 },1 },{ "Level0",{ -0.28,0 },1 },{  },{ "Level0",{ -0.27,0 },1 },{ "Level0",{ -0.23,0 },1 },{  },{ "Level0",{ -0.22,0 },1 },{ "Level0",{ -0.18,0 },1 },{  },{ "Level0",{ -0.17,0 },1 },{ "Level0",{ -0.13,0 },1 },{  },{ "Level0",{ -0.12,0 },1 },{ "Level0",{ -0.08,0 },1 },{  },{ "Level0",{ 0.42,0 },1 },{ "Level0",{ 0.38,0 },1 },{  },{ "Level0",{ 0.37,0 },1 },{ "Level0",{ 0.33,0 },1 },{  },{ "Level0",{ 0.32,0 },1 },{ "Level0",{ 0.28,0 },1 },{  },{ "Level0",{ 0.27,0 },1 },{ "Level0",{ 0.23,0 },1 },{  },{ "Level0",{ 0.22,0 },1 },{ "Level0",{ 0.18,0 },1 },{  },{ "Level0",{ 0.17,0 },1 },{ "Level0",{ 0.13,0 },1 },{  },{ "Level0",{ 0.12,0 },1 },{ "Level0",{ 0.08,0 },1 }};
							};
						};
					};
					class HorizonBankRot
					{
						type = "line";
						width = 3;
						points[] = {{ "HorizonBankRot",{ 0,0.25 },1 },{ "HorizonBankRot",{ -0.01,0.23 },1 },{ "HorizonBankRot",{ 0.01,0.23 },1 },{ "HorizonBankRot",{ 0,0.25 },1 }};
					};
					class Static_HAD_BOX
					{
						clipTL[] = {0,1};
						clipBR[] = {1,0};
						type = "line";
						width = 5;
						points[] = {{ { "0.5-0.1","0.9-0.04" },1 },{ { "0.5-0.1","0.9+0.04" },1 },{ { "0.5+0.1","0.9+0.04" },1 },{ { "0.5+0.1","0.9-0.04" },1 },{ { "0.5-0.1","0.9-0.04" },1 },{  },{ { "0.5-0.1","0.9-0.04+0.012" },1 },{ { "0.5-0.092","0.9-0.04+0.012" },1 },{  },{ { "0.5+0.1","0.9-0.04+0.012" },1 },{ { "0.5+0.092","0.9-0.04+0.012" },1 },{  },{ { 0.5,"0.9-0.04" },1 },{ { 0.5,"0.9-0.032" },1 },{  },{ { 0.5,"0.9+0.04" },1 },{ { 0.5,"0.9+0.032" },1 },{  }};
					};
					class Gunner_HAD
					{
						type = "line";
						width = 6;
						points[] = {{ "GunnerAim",{ -0.015,-0.008 },1 },{ "GunnerAim",{ -0.015,0.008 },1 },{ "GunnerAim",{ 0.015,0.008 },1 },{ "GunnerAim",{ 0.015,-0.008 },1 },{ "GunnerAim",{ -0.015,-0.008 },1 }};
					};
					class Slip_ball_group
					{
						class Slip_bars
						{
							type = "line";
							width = 4;
							points[] = {{ { "0.5-0.018","0.9-0.04" },1 },{ { "0.5-0.018","0.9-0.075" },1 },{  },{ { "0.5+0.018","0.9-0.04" },1 },{ { "0.5+0.018","0.9-0.075" },1 }};
						};
						class Slip_ball
						{
							type = "line";
							width = 6.0;
							points[] = {{ "Velocity_slip",1,{ "0 * 0.75","-0.02 * 0.75" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.75","-0.01732 * 0.75" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.75","-0.0099999998 * 0.75" },1 },{ "Velocity_slip",1,{ "0.02 * 0.75","0 * 0.75" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.75","0.0099999998 * 0.75" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.75","0.01732 * 0.75" },1 },{ "Velocity_slip",1,{ "0 * 0.75","0.02 * 0.75" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.75","0.01732 * 0.75" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.75","0.0099999998 * 0.75" },1 },{ "Velocity_slip",1,{ "-0.02 * 0.75","0 * 0.75" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.75","-0.0099999998 * 0.75" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.75","-0.01732 * 0.75" },1 },{ "Velocity_slip",1,{ "0 * 0.75","-0.02 * 0.75" },1 },{  },{ "Velocity_slip",1,{ "0 * 0.6","-0.02 * 0.6" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.6","-0.01732 * 0.6" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.6","-0.0099999998 * 0.6" },1 },{ "Velocity_slip",1,{ "0.02 * 0.6","0 * 0.6" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.6","0.0099999998 * 0.6" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.6","0.01732 * 0.6" },1 },{ "Velocity_slip",1,{ "0 * 0.6","0.02 * 0.6" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.6","0.01732 * 0.6" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.6","0.0099999998 * 0.6" },1 },{ "Velocity_slip",1,{ "-0.02 * 0.6","0 * 0.6" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.6","-0.0099999998 * 0.6" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.6","-0.01732 * 0.6" },1 },{ "Velocity_slip",1,{ "0 * 0.6","-0.02 * 0.6" },1 },{  },{ "Velocity_slip",1,{ "0 * 0.5","-0.02 * 0.5" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.5","-0.01732 * 0.5" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.5","-0.0099999998 * 0.5" },1 },{ "Velocity_slip",1,{ "0.02 * 0.5","0 * 0.5" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.5","0.0099999998 * 0.5" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.5","0.01732 * 0.5" },1 },{ "Velocity_slip",1,{ "0 * 0.5","0.02 * 0.5" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.5","0.01732 * 0.5" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.5","0.0099999998 * 0.5" },1 },{ "Velocity_slip",1,{ "-0.02 * 0.5","0 * 0.5" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.5","-0.0099999998 * 0.5" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.5","-0.01732 * 0.5" },1 },{ "Velocity_slip",1,{ "0 * 0.5","-0.02 * 0.5" },1 },{  },{ "Velocity_slip",1,{ "0 * 0.4","-0.02 * 0.4" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.4","-0.01732 * 0.4" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.4","-0.0099999998 * 0.4" },1 },{ "Velocity_slip",1,{ "0.02 * 0.4","0 * 0.4" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.4","0.0099999998 * 0.4" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.4","0.01732 * 0.4" },1 },{ "Velocity_slip",1,{ "0 * 0.4","0.02 * 0.4" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.4","0.01732 * 0.4" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.4","0.0099999998 * 0.4" },1 },{ "Velocity_slip",1,{ "-0.02 * 0.4","0 * 0.4" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.4","-0.0099999998 * 0.4" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.4","-0.01732 * 0.4" },1 },{ "Velocity_slip",1,{ "0 * 0.4","-0.02 * 0.4" },1 },{  },{ "Velocity_slip",1,{ "0 * 0.30","-0.02 * 0.30" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.30","-0.01732 * 0.30" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.30","-0.0099999998 * 0.30" },1 },{ "Velocity_slip",1,{ "0.02 * 0.30","0 * 0.30" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.30","0.0099999998 * 0.30" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.30","0.01732 * 0.30" },1 },{ "Velocity_slip",1,{ "0 * 0.30","0.02 * 0.30" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.30","0.01732 * 0.30" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.30","0.0099999998 * 0.30" },1 },{ "Velocity_slip",1,{ "-0.02 * 0.30","0 * 0.30" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.30","-0.0099999998 * 0.30" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.30","-0.01732 * 0.30" },1 },{ "Velocity_slip",1,{ "0 * 0.30","-0.02 * 0.30" },1 },{  },{ "Velocity_slip",1,{ "0 * 0.20","-0.02 * 0.20" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.20","-0.01732 * 0.20" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.20","-0.0099999998 * 0.20" },1 },{ "Velocity_slip",1,{ "0.02 * 0.20","0 * 0.20" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.20","0.0099999998 * 0.20" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.20","0.01732 * 0.20" },1 },{ "Velocity_slip",1,{ "0 * 0.20","0.02 * 0.20" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.20","0.01732 * 0.20" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.20","0.0099999998 * 0.20" },1 },{ "Velocity_slip",1,{ "-0.02 * 0.20","0 * 0.20" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.20","-0.0099999998 * 0.20" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.20","-0.01732 * 0.20" },1 },{ "Velocity_slip",1,{ "0 * 0.20","-0.02 * 0.20" },1 },{  },{ "Velocity_slip",1,{ "0 * 0.1","-0.02 * 0.1" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.1","-0.01732 * 0.1" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.1","-0.0099999998 * 0.1" },1 },{ "Velocity_slip",1,{ "0.02 * 0.1","0 * 0.1" },1 },{ "Velocity_slip",1,{ "0.01732 * 0.1","0.0099999998 * 0.1" },1 },{ "Velocity_slip",1,{ "0.0099999998 * 0.1","0.01732 * 0.1" },1 },{ "Velocity_slip",1,{ "0 * 0.1","0.02 * 0.1" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.1","0.01732 * 0.1" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.1","0.0099999998 * 0.1" },1 },{ "Velocity_slip",1,{ "-0.02 * 0.1","0 * 0.1" },1 },{ "Velocity_slip",1,{ "-0.01732 * 0.1","-0.0099999998 * 0.1" },1 },{ "Velocity_slip",1,{ "-0.0099999998 * 0.1","-0.01732 * 0.1" },1 },{ "Velocity_slip",1,{ "0 * 0.1","-0.02 * 0.1" },1 }};
						};
					};
					class Centerline
					{
						type = "line";
						width = 5;
						points[] = {{ { 0.5,0.48 },1 },{ { 0.5,0.45 },1 },{  },{ { 0.5,0.52 },1 },{ { 0.5,0.55 },1 },{  },{ { 0.48,0.5 },1 },{ { 0.45,0.5 },1 },{  },{ { 0.52,0.5 },1 },{ { 0.55,0.5 },1 },{  }};
					};
					class WeaponName
					{
						type = "text";
						source = "weapon";
						sourceScale = 1;
						align = "right";
						scale = 1;
						pos[] = {{ 0.61,0.86 },1};
						right[] = {{ 0.65,0.86 },1};
						down[] = {{ 0.61,0.9 },1};
					};
					class Ammo_GUN
					{
						type = "group";
						condition = "mgun";
						class Ammo_count_GUN
						{
							type = "text";
							source = "ammo";
							sourceScale = 1;
							align = "right";
							scale = 1;
							pos[] = {{ 0.61,0.89 },1};
							right[] = {{ 0.65,0.89 },1};
							down[] = {{ 0.61,0.93 },1};
						};
					};
					class Ammo_RKT
					{
						type = "group";
						condition = "rocket";
						class Ammo_count_RKT
						{
							type = "text";
							source = "ammo";
							sourceScale = 1;
							align = "right";
							scale = 1;
							pos[] = {{ 0.61,0.89 },1};
							right[] = {{ 0.65,0.89 },1};
							down[] = {{ 0.61,0.93 },1};
						};
					};
					class Ammo_AGM
					{
						type = "group";
						condition = "AAmissile";
						class Ammo_count_AGM
						{
							type = "text";
							source = "ammo";
							sourceScale = 1;
							align = "right";
							scale = 1;
							pos[] = {{ 0.61,0.89 },1};
							right[] = {{ 0.65,0.89 },1};
							down[] = {{ 0.61,0.93 },1};
						};
					};
					class Ammo_AAM
					{
						type = "group";
						condition = "ATmissile";
						class Ammo_count_AAM
						{
							type = "text";
							source = "ammo";
							sourceScale = 1;
							align = "right";
							scale = 1;
							pos[] = {{ 0.61,0.89 },1};
							right[] = {{ 0.65,0.89 },1};
							down[] = {{ 0.61,0.93 },1};
						};
					};
					class Ammo_Bomb
					{
						type = "group";
						condition = "Bomb";
						class Ammo_count_Bomb
						{
							type = "text";
							source = "ammo";
							sourceScale = 1;
							align = "right";
							scale = 1;
							pos[] = {{ 0.61,0.89 },1};
							right[] = {{ 0.65,0.89 },1};
							down[] = {{ 0.61,0.93 },1};
						};
					};
					class LightsGroup
					{
						type = "group";
						condition = "lights";
						class LightsText
						{
							type = "text";
							source = "static";
							text = "LIGHTS";
							align = "right";
							scale = 1;
							pos[] = {{ 0.03,"0.53 + 0.055" },1};
							right[] = {{ 0.07,"0.53 + 0.055" },1};
							down[] = {{ 0.03,"0.53 + 0.095" },1};
						};
					};
					class CollisionLightsGroup
					{
						type = "group";
						condition = "collisionlights";
						class CollisionLightsText
						{
							type = "text";
							source = "static";
							text = "A-COL";
							align = "right";
							scale = 1;
							pos[] = {{ 0.03,"0.53 + 0.105" },1};
							right[] = {{ 0.07,"0.53 + 0.105" },1};
							down[] = {{ 0.03,"0.53 + 0.145" },1};
						};
					};
					class ATMissileTOFGroup
					{
						condition = "ATmissile";
						type = "group";
						class TOFtext
						{
							type = "text";
							align = "right";
							source = "static";
							text = "TOF=";
							scale = 1;
							pos[] = {{ 0.61,0.92 },1};
							right[] = {{ 0.65,0.92 },1};
							down[] = {{ 0.61,0.96 },1};
						};
						class TOFnumber
						{
							type = "text";
							source = "targetDist";
							sourcescale = 0.0025;
							align = "right";
							scale = 1;
							pos[] = {{ 0.69,0.92 },1};
							right[] = {{ 0.73,0.92 },1};
							down[] = {{ 0.69,0.96 },1};
						};
					};
					class LaserTOFGroup
					{
						condition = "Bomb";
						type = "group";
						class TOFtext
						{
							type = "text";
							align = "right";
							source = "static";
							text = "TOF=";
							scale = 1;
							pos[] = {{ 0.61,0.92 },1};
							right[] = {{ 0.65,0.92 },1};
							down[] = {{ 0.61,0.96 },1};
						};
						class TOFnumber
						{
							type = "text";
							source = "targetDist";
							sourcescale = 0.0025;
							align = "right";
							scale = 1;
							pos[] = {{ 0.69,0.92 },1};
							right[] = {{ 0.73,0.92 },1};
							down[] = {{ 0.69,0.96 },1};
						};
					};
					class RocketTOFGroup
					{
						condition = "Rocket";
						type = "group";
						class TOFtext
						{
							type = "text";
							align = "right";
							source = "static";
							text = "TOF=";
							scale = 1;
							pos[] = {{ 0.61,0.92 },1};
							right[] = {{ 0.65,0.92 },1};
							down[] = {{ 0.61,0.96 },1};
						};
						class TOFnumber
						{
							type = "text";
							source = "targetDist";
							sourcescale = 0.0025;
							align = "right";
							scale = 1;
							pos[] = {{ 0.69,0.92 },1};
							right[] = {{ 0.73,0.92 },1};
							down[] = {{ 0.69,0.96 },1};
						};
					};
					class RangeNumber
					{
						type = "text";
						source = "targetDist";
						sourceScale = 1;
						align = "left";
						scale = 1;
						pos[] = {{ 0.39,0.89 },1};
						right[] = {{ 0.43,0.89 },1};
						down[] = {{ 0.39,0.93 },1};
					};
					class RangeText
					{
						type = "text";
						source = "static";
						text = "RNG";
						align = "left";
						scale = 1;
						pos[] = {{ 0.39,0.86 },1};
						right[] = {{ 0.43,0.86 },1};
						down[] = {{ 0.39,0.9 },1};
					};
					class SpeedNumber
					{
						type = "text";
						align = "right";
						scale = 1;
						source = "speed";
						sourceScale = 3.6;
						pos[] = {{ 0.03,0.475 },1};
						right[] = {{ 0.08,0.475 },1};
						down[] = {{ 0.03,0.525 },1};
					};
					class TorqueNumber
					{
						condition = "simulRTD";
						class Torque_number
						{
							type = "text";
							align = "left";
							scale = 1;
							source = "rtdRotorTorque";
							sourceScale = 290;
							pos[] = {{ 0.065,0.175 },1};
							right[] = {{ 0.115,0.175 },1};
							down[] = {{ 0.065,0.225 },1};
						};
						class Torquetext
						{
							type = "text";
							source = "static";
							text = "%";
							align = "right";
							scale = 1;
							pos[] = {{ 0.07,0.175 },1};
							right[] = {{ 0.12,0.175 },1};
							down[] = {{ 0.07,0.225 },1};
						};
					};
					class AltNumber: SpeedNumber
					{
						align = "right";
						source = "altitudeAGL";
						sourceScale = 1;
						pos[] = {{ 0.83,0.475 },1};
						right[] = {{ 0.88,0.475 },1};
						down[] = {{ 0.83,0.525 },1};
					};
					class ASLNumber
					{
						type = "text";
						source = "altitudeASL";
						sourceScale = 1;
						align = "right";
						scale = 1;
						pos[] = {{ 0.835,0.18 },1};
						right[] = {{ 0.875,0.18 },1};
						down[] = {{ 0.835,0.22 },1};
					};
					class VspeedScalePosta
					{
						type = "line";
						width = 5;
						points[] = {{ { 0.98,0.2 },1 },{ { 1.0,0.2 },1 },{  },{ { 0.93,0.2 },1 },{ { 0.95,0.2 },1 },{  },{ { 0.98,0.35 },1 },{ { 1.0,0.35 },1 },{  },{ { 0.93,0.35 },1 },{ { 0.95,0.35 },1 },{  },{ { 0.94,0.38 },1 },{ { 0.95,0.38 },1 },{  },{ { 0.94,0.41 },1 },{ { 0.95,0.41 },1 },{  },{ { 0.94,0.44 },1 },{ { 0.95,0.44 },1 },{  },{ { 0.94,0.47 },1 },{ { 0.95,0.47 },1 },{  },{ { 0.98,0.5 },1 },{ { 1.0,0.5 },1 },{  },{ { 0.93,0.5 },1 },{ { 0.95,0.5 },1 },{  },{ { 0.94,0.53 },1 },{ { 0.95,0.53 },1 },{  },{ { 0.94,0.56 },1 },{ { 0.95,0.56 },1 },{  },{ { 0.94,0.59 },1 },{ { 0.95,0.59 },1 },{  },{ { 0.94,0.62 },1 },{ { 0.95,0.62 },1 },{  },{ { 0.98,0.65 },1 },{ { 1.0,0.65 },1 },{  },{ { 0.93,0.65 },1 },{ { 0.95,0.65 },1 },{  },{ { 0.99,0.68 },1 },{ { 0.98,0.68 },1 },{  },{ { 0.99,0.71 },1 },{ { 0.98,0.71 },1 },{  },{ { 0.99,0.74 },1 },{ { 0.98,0.74 },1 },{  },{ { 0.99,0.77 },1 },{ { 0.98,0.77 },1 },{  },{ { 0.98,0.8 },1 },{ { 1.0,0.8 },1 },{  },{ { 0.93,0.8 },1 },{ { 0.95,0.8 },1 },{  }};
					};
					class RadarAltitudeBand
					{
						clipTL[] = {0,0.2};
						clipBR[] = {1,0.8};
						hideValue = 201;
						class radarbanda
						{
							type = "line";
							width = 17;
							points[] = {{ "RadarAltitudeBone",{ 0,0 },1 },{ "RadarAltitudeBone",{ 0,0.6 },1 }};
						};
					};
					class VspeedBand
					{
						type = "line";
						width = 3;
						points[] = {{ "VspeedBone",{ -0.01,0.0 },1 },{ "VspeedBone",{ -0.025,-0.015 },1 },{ "VspeedBone",{ -0.025,0.015 },1 },{ "VspeedBone",{ -0.01,0.0 },1 },{  }};
					};
					class HeadingNumber: SpeedNumber
					{
						source = "heading";
						sourceScale = 1;
						align = "center";
						pos[] = {{ 0.5,0.045 },1};
						right[] = {{ 0.56,0.045 },1};
						down[] = {{ 0.5,"0.045 + 0.06" },1};
					};
					class Center_box
					{
						type = "line";
						width = 1.5;
						points[] = {{ { 0.45,"0.02 + 0.085 - 0.06" },1 },{ { "0.45 + 0.10","0.02 + 0.085 - 0.06" },1 },{ { "0.45 + 0.10","0.02 + 0.085" },1 },{ { 0.45,"0.02 + 0.085" },1 },{ { 0.45,"0.02 + 0.085 - 0.06" },1 }};
					};
					class HeadingArrow
					{
						type = "line";
						width = 7;
						points[] = {{ { "0.5","0.128 + 0.03" },1 },{ { 0.5,0.128 },1 }};
					};
					class HeadingScale_LEFT
					{
						clipTL[] = {0,0};
						clipBR[] = {0.45,1};
						class Heading_group
						{
							type = "scale";
							horizontal = 1;
							source = "heading";
							sourceScale = 1;
							width = 5;
							top = 0.12;
							center = 0.5;
							bottom = 0.88;
							lineXleft = "0.03 + 0.085";
							lineYright = "0.02 + 0.085";
							lineXleftMajor = "0.04 + 0.085";
							lineYrightMajor = "0.02 + 0.085";
							majorLineEach = 3;
							numberEach = 3;
							step = 10;
							stepSize = "0.05";
							align = "center";
							scale = 1;
							pos[] = {0.12,"0.0 + 0.065"};
							right[] = {0.16,"0.0 + 0.065"};
							down[] = {0.12,"0.04 + 0.065"};
						};
					};
					class HeadingScale_RIGHT
					{
						clipTL[] = {0.55,0};
						clipBR[] = {1,1};
						class Heading_group
						{
							type = "scale";
							horizontal = 1;
							source = "heading";
							sourceScale = 1;
							width = 5;
							top = 0.12;
							center = 0.5;
							bottom = 0.88;
							lineXleft = "0.03 + 0.085";
							lineYright = "0.02 + 0.085";
							lineXleftMajor = "0.04 + 0.085";
							lineYrightMajor = "0.02 + 0.085";
							majorLineEach = 3;
							numberEach = 3;
							step = 10;
							stepSize = "0.05";
							align = "center";
							scale = 1;
							pos[] = {0.12,"0.0 + 0.065"};
							right[] = {0.16,"0.0 + 0.065"};
							down[] = {0.12,"0.04 + 0.065"};
						};
					};
					class HeadingScale_BOTTOM
					{
						clipTL[] = {0.45,"0.02 + 0.085"};
						clipBR[] = {"0.45 + 0.10",1};
						class Heading_group
						{
							type = "scale";
							horizontal = 1;
							source = "heading";
							sourceScale = 1;
							width = 5;
							top = 0.12;
							center = 0.5;
							bottom = 0.88;
							lineXleft = "0.03 + 0.085";
							lineYright = "0.02 + 0.085";
							lineXleftMajor = "0.04 + 0.085";
							lineYrightMajor = "0.02 + 0.085";
							majorLineEach = 3;
							numberEach = 3;
							step = 10;
							stepSize = "0.05";
							align = "center";
							scale = 1;
							pos[] = {0.12,"0.0 + 0.065"};
							right[] = {0.16,"0.0 + 0.065"};
							down[] = {0.12,"0.04 + 0.065"};
						};
					};
					class Fuel_Text
					{
						type = "text";
						source = "static";
						text = "Fuel";
						align = "right";
						scale = 1;
						pos[] = {{ 0.03,0.9 },1};
						right[] = {{ 0.07,0.9 },1};
						down[] = {{ 0.03,0.94 },1};
					};
					class Fuel_Number
					{
						type = "text";
						source = "fuel";
						sourceScale = 100;
						align = "right";
						scale = 1;
						pos[] = {{ 0.1,0.9 },1};
						right[] = {{ 0.14,0.9 },1};
						down[] = {{ 0.1,0.94 },1};
					};
				};
				helmetMountedDisplay = 1;
				helmetPosition[] = {-0.04,0.04,0.1};
				helmetRight[] = {0.08,0,0};
				helmetDown[] = {0,-0.08,0};
			};
			class Kimi_HUD_2
			{
				topLeft = "HUD_top_left";
				topRight = "HUD_top_right";
				bottomLeft = "HUD_bottom_left";
				borderLeft = 0;
				borderRight = 0;
				borderTop = 0;
				borderBottom = 0;
				color[] = {0.15,1,0.15,1};
				enableParallax = 0;
				class Bones
				{
					class Velocity
					{
						type = "vector";
						source = "velocity";
						pos0[] = {0.5,0.5};
						pos10[] = {0.75,0.75};
					};
					class ForwardVec1
					{
						type = "vector";
						source = "forward";
						pos0[] = {0,0};
						pos10[] = {0.25,0.25};
					};
					class ForwardVec
					{
						type = "vector";
						source = "forward";
						pos0[] = {0,0};
						pos10[] = {0.253,0.253};
					};
					class WeaponAim
					{
						type = "vector";
						source = "weapon";
						pos0[] = {0.5,0.5};
						pos10[] = {0.753,0.753};
					};
					class WeaponAim1
					{
						type = "vector";
						source = "weapon";
						pos0[] = {0,0};
						pos10[] = {0.253,0.23};
					};
					class Target
					{
						type = "vector";
						source = "target";
						pos0[] = {0.5,0.5};
						pos10[] = {0.753,0.753};
					};
					class RadarContact
					{
						type = "fixed";
						pos[] = {0,0};
					};
				};
				class Draw
				{
					color[] = {0.18,1,0.18};
					alpha = 1;
					condition = "on";
					class PlaneMovementCrosshair
					{
						type = "line";
						width = 7;
						points[] = {{ "ForwardVec1",1,"Velocity",1,{ 0,-0.02 },1 },{ "ForwardVec1",1,"Velocity",1,{ 0.01,-0.01732 },1 },{ "ForwardVec1",1,"Velocity",1,{ 0.01732,-0.01 },1 },{ "ForwardVec1",1,"Velocity",1,{ 0.02,0 },1 },{ "ForwardVec1",1,"Velocity",1,{ 0.01732,0.01 },1 },{ "ForwardVec1",1,"Velocity",1,{ 0.01,0.01732 },1 },{ "ForwardVec1",1,"Velocity",1,{ 0,0.02 },1 },{ "ForwardVec1",1,"Velocity",1,{ -0.01,0.01732 },1 },{ "ForwardVec1",1,"Velocity",1,{ -0.01732,0.01 },1 },{ "ForwardVec1",1,"Velocity",1,{ -0.02,0 },1 },{ "ForwardVec1",1,"Velocity",1,{ -0.01732,-0.01 },1 },{ "ForwardVec1",1,"Velocity",1,{ -0.01,-0.01732 },1 },{ "ForwardVec1",1,"Velocity",1,{ 0,-0.02 },1 },{  },{ "ForwardVec1",1,"Velocity",1,{ 0.04,0 },1 },{ "ForwardVec1",1,"Velocity",1,{ 0.02,0 },1 },{  },{ "ForwardVec1",1,"Velocity",1,{ -0.04,0 },1 },{ "ForwardVec1",1,"Velocity",1,{ -0.02,0 },1 },{  },{ "ForwardVec1",1,"Velocity",1,{ 0,-0.04 },1 },{ "ForwardVec1",1,"Velocity",1,{ 0,-0.02 },1 }};
					};
					class Gunner_AIM
					{
						type = "group";
						class Circle
						{
							type = "line";
							width = 6.0;
							points[] = {{ "ForwardVec",1,"WeaponAim",1,{ 0,-0.015 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0,-0.03 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ 0,-0.0325 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0,-0.0475 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ 0,0.015 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0,0.03 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ 0,0.0325 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0,0.0475 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ -0.015,0 },1 },{ "ForwardVec",1,"WeaponAim",1,{ -0.03,0 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ -0.0325,0 },1 },{ "ForwardVec",1,"WeaponAim",1,{ -0.0475,0 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ 0.015,0 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0.03,0 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ 0.0325,0 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0.0475,0 },1 }};
						};
					};
					class GunCross
					{
						condition = "mgun";
						class Circle
						{
							type = "line";
							width = 9.0;
							points[] = {{ "ForwardVec",1,"WeaponAim",1,{ 0,-0.05 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0,-0.015 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ 0,0.015 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0,0.05 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ -0.05,0 },1 },{ "ForwardVec",1,"WeaponAim",1,{ -0.015,0 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ 0.015,0 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0.05,0 },1 },{  }};
						};
					};
					class RocketCross
					{
						condition = "rocket";
						width = 6.0;
						class Circle
						{
							type = "line";
							width = 6.0;
							points[] = {{ "ForwardVec",1,"WeaponAim",1,{ -0.05,-0.08 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0.05,-0.08 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ 0,-0.08 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0,0.08 },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ -0.05,0.08 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0.05,0.08 },1 },{  }};
						};
					};
					class AT_Aim
					{
						condition = "ATmissile";
						width = 2.0;
						class Circle
						{
							type = "line";
							width = 2.0;
							points[] = {{ "ForwardVec",1,"WeaponAim",1,{ -0.1,-0.1 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0.1,-0.1 },1 },{ "ForwardVec",1,"WeaponAim",1,{ 0.1,0.1 },1 },{ "ForwardVec",1,"WeaponAim",1,{ -0.1,0.1 },1 },{ "ForwardVec",1,"WeaponAim",1,{ -0.1,-0.1 },1 }};
						};
					};
					class AA_aim
					{
						condition = "AAmissile";
						class Circle
						{
							type = "line";
							width = 2.5;
							points[] = {{ "ForwardVec",1,"WeaponAim",1,{ "0 / 4","-0.248559 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.0434 / 4","-0.244781 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.0855 / 4","-0.233571 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.125 / 4","-0.215252 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.1607 / 4","-0.190396 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.1915 / 4","-0.159774 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.2165 / 4","-0.12428 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.234925 / 4","-0.0850072 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.2462 / 4","-0.0431499 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.25 / 4","0 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.2462 / 4","0.0431499 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.234925 / 4","0.0850072 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.2165 / 4","0.12428 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.1915 / 4","0.159774 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.1607 / 4","0.190396 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.125 / 4","0.215252 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.0855 / 4","0.233571 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.0434 / 4","0.244781 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0 / 4","0.248559 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.0434 / 4","0.244781 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.0855 / 4","0.233571 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.125 / 4","0.215252 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.1607 / 4","0.190396 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.1915 / 4","0.159774 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.2165 / 4","0.12428 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.234925 / 4","0.0850072 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.2462 / 4","0.0431499 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.25 / 4","0 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.2462 / 4","-0.0431499 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.234925 / 4","-0.0850072 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.2165 / 4","-0.12428 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.1915 / 4","-0.159774 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.1607 / 4","-0.190396 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.125 / 4","-0.215252 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.0855 / 4","-0.233571 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.0434 / 4","-0.244781 / 4" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0 / 4","-0.248559 / 4" },1 },{  },{ "ForwardVec",1,"WeaponAim",1,{ "0 / 2","-0.248559 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.0434 / 2","-0.244781 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.0855 / 2","-0.233571 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.125 / 2","-0.215252 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.1607 / 2","-0.190396 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.1915 / 2","-0.159774 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.2165 / 2","-0.12428 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.234925 / 2","-0.0850072 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.2462 / 2","-0.0431499 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.25 / 2","0 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.2462 / 2","0.0431499 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.234925 / 2","0.0850072 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.2165 / 2","0.12428 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.1915 / 2","0.159774 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.1607 / 2","0.190396 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.125 / 2","0.215252 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.0855 / 2","0.233571 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0.0434 / 2","0.244781 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0 / 2","0.248559 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.0434 / 2","0.244781 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.0855 / 2","0.233571 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.125 / 2","0.215252 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.1607 / 2","0.190396 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.1915 / 2","0.159774 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.2165 / 2","0.12428 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.234925 / 2","0.0850072 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.2462 / 2","0.0431499 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.25 / 2","0 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.2462 / 2","-0.0431499 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.234925 / 2","-0.0850072 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.2165 / 2","-0.12428 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.1915 / 2","-0.159774 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.1607 / 2","-0.190396 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.125 / 2","-0.215252 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.0855 / 2","-0.233571 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "-0.0434 / 2","-0.244781 / 2" },1 },{ "ForwardVec",1,"WeaponAim",1,{ "0 / 2","-0.248559 / 2" },1 }};
						};
					};
					class TargetACQ
					{
						type = "line";
						width = 2;
						points[] = {{ "ForwardVec",1,"target",{ 0,-0.06 },1 },{ "ForwardVec",1,"target",{ 0,-0.055 },1 },{  },{ "ForwardVec",1,"target",{ 0,-0.05 },1 },{ "ForwardVec",1,"target",{ 0,-0.045 },1 },{  },{ "ForwardVec",1,"target",{ 0,-0.04 },1 },{ "ForwardVec",1,"target",{ 0,-0.035 },1 },{  },{ "ForwardVec",1,"target",{ 0,-0.03 },1 },{ "ForwardVec",1,"target",{ 0,-0.025 },1 },{  },{ "ForwardVec",1,"target",{ 0,-0.02 },1 },{ "ForwardVec",1,"target",{ 0,-0.015 },1 },{  },{ "ForwardVec",1,"target",{ 0,-0.01 },1 },{ "ForwardVec",1,"target",{ 0,-0.005 },1 },{  },{ "ForwardVec",1,"target",{ 0,0 },1 },{ "ForwardVec",1,"target",{ 0,0 },1 },{  },{ "ForwardVec",1,"target",{ 0,0.06 },1 },{ "ForwardVec",1,"target",{ 0,0.055 },1 },{  },{ "ForwardVec",1,"target",{ 0,0.05 },1 },{ "ForwardVec",1,"target",{ 0,0.045 },1 },{  },{ "ForwardVec",1,"target",{ 0,0.04 },1 },{ "ForwardVec",1,"target",{ 0,0.035 },1 },{  },{ "ForwardVec",1,"target",{ 0,0.03 },1 },{ "ForwardVec",1,"target",{ 0,0.025 },1 },{  },{ "ForwardVec",1,"target",{ 0,0.02 },1 },{ "ForwardVec",1,"target",{ 0,0.015 },1 },{  },{ "ForwardVec",1,"target",{ 0,0.01 },1 },{ "ForwardVec",1,"target",{ 0,0.005 },1 },{  },{ "ForwardVec",1,"target",{ -0.06,0 },1 },{ "ForwardVec",1,"target",{ -0.055,0 },1 },{  },{ "ForwardVec",1,"target",{ -0.05,0 },1 },{ "ForwardVec",1,"target",{ -0.045,0 },1 },{  },{ "ForwardVec",1,"target",{ -0.04,0 },1 },{ "ForwardVec",1,"target",{ -0.035,0 },1 },{  },{ "ForwardVec",1,"target",{ -0.03,0 },1 },{ "ForwardVec",1,"target",{ -0.025,0 },1 },{  },{ "ForwardVec",1,"target",{ -0.02,0 },1 },{ "ForwardVec",1,"target",{ -0.015,0 },1 },{  },{ "ForwardVec",1,"target",{ -0.01,0 },1 },{ "ForwardVec",1,"target",{ -0.005,0 },1 },{  },{ "ForwardVec",1,"target",{ 0.06,0 },1 },{ "ForwardVec",1,"target",{ 0.055,0 },1 },{  },{ "ForwardVec",1,"target",{ 0.05,0 },1 },{ "ForwardVec",1,"target",{ 0.045,0 },1 },{  },{ "ForwardVec",1,"target",{ 0.04,0 },1 },{ "ForwardVec",1,"target",{ 0.035,0 },1 },{  },{ "ForwardVec",1,"target",{ 0.03,0 },1 },{ "ForwardVec",1,"target",{ 0.025,0 },1 },{  },{ "ForwardVec",1,"target",{ 0.02,0 },1 },{ "ForwardVec",1,"target",{ 0.015,0 },1 },{  },{ "ForwardVec",1,"target",{ 0.01,0 },1 },{ "ForwardVec",1,"target",{ 0.005,0 },1 },{  }};
					};
					class RadarTargets
					{
						type = "radar";
						pos0[] = {0.5,0.5};
						pos10[] = {0.753,0.753};
						width = 2.5;
						points[] = {{ "ForwardVec",1,"RadarContact",{ -0.01,-0.01 },1 },{ "ForwardVec",1,"RadarContact",{ 0.01,-0.01 },1 },{ "ForwardVec",1,"RadarContact",{ 0.01,0.01 },1 },{ "ForwardVec",1,"RadarContact",{ -0.01,0.01 },1 },{ "ForwardVec",1,"RadarContact",{ -0.01,-0.01 },1 }};
					};
				};
				helmetMountedDisplay = 1;
				helmetPosition[] = {-0.035,0.035,0.1};
				helmetRight[] = {0.07,0,0};
				helmetDown[] = {0,-0.07,0};
			};
		};
	};
	class rhsusf_CH53E_USMC: Helicopter_Base_H {};
	class rhsusf_CH53E_USMCedit: rhsusf_CH53E_USMC
	{
		faction = "rhs_faction_usmc_wd";
		vehicleClass = "Air";
		scope = 2;
		rhs_decalParameters[] = {"['Number', cCH53NumberPlaces, _typeNum,_randomNum]"};
		rhs_paraRamp = "ramp";
		rhs_paraOff = -15;
		rhs_rampAnim = "ramp_bottom";
		rhs_gearAnim = "Gear_Nose_1";
		rhs_paraScript = "rhs_fnc_vehPara";
		displayName = "15th RHS CH-53E";
		LockDetectionSystem="2 + 8 + 4";
		incomingMissileDetectionSystem=16;
		radarType=4; // Air radar
		gunnerCanSee="1+2+4+8+16";
		driverCanSee="1+2+4+8+16";
		camouflage = 100;
		sensitivity = 3;
		sensitivityear = 3;
		canBeShot = 1;
		audible = 20;
		maximumLoad = 20000;
		ace_fastroping_enabled = 1;
        ace_fastroping_onCut = "ace_fastroping_fnc_onCutCommon";
        ace_fastroping_onPrepare = "ace_fastroping_fnc_onPrepareCommon";
        ace_fastroping_ropeOrigins[] = {{0.5, -7.15, -0.95}, {-0.5, -7.15, -0.95}};
		driverCanEject = 1;
		cargoCanEject = 1;
		gunnercaneject =1;
		armor = 40;
		armorStructural = 2;
		damageResistance = 0.00555;
		slingLoadMaxCargoMass = 15000;
           class UserActions
		{
			class Pack
			{
				displayName = "<t color='#008000'>Pack";
				displayNameDefault = "Pack";
				position = "PackAction";
				radius = 10;
				onlyForPlayer = 1;
				condition = "(!isEngineOn this) AND {(this doorPhase 'mainRotor_folded' !=1) AND (driver this == player) AND (speed this == 0)}";
				statement = "[this,1] call rhsusf_fnc_ch53_fold";
				priority = -10;
			};
			class unPack
			{
				displayName = "<t color='#008000'>UnPack";
				displayNameDefault = "Unpack";
				position = "PackAction";
				radius = 10;
				onlyForPlayer = 1;
				condition = "this doorPhase 'mainRotor_folded' !=0 AND driver this == player";
				statement = "[this,0] call rhsusf_fnc_ch53_fold";
				priority = -10;
			};
			class RampOpen
			{
				displayName = "<t color='#FFD700'>Open Ramp";
				position = "ramp action";
				showWindow = 0;
				radius = 5;
				condition = "(this animationPhase ""ramp_bottom"" < 0.56) && (player in [driver this,gunner this]) && (alive this)";
				statement = "this animate ['ramp_bottom',1];this animate ['ramp_top',1];";
				onlyforplayer = 0;
				shortcut = "user12";
				priority = -9;
			};
			class RampLevel
			{
				displayName = "<t color='#FFD700'>Level Ramp";
				position = "ramp action";
				showWindow = 0;
				radius = 5;
				condition = "(this animationPhase ""ramp_bottom"" < 0.56) && (player in [driver this,gunner this]) && (alive this)";
				statement = "this animate ['ramp_bottom',0.56];this animate ['ramp_top',1];";
				onlyforplayer = 0;
				shortcut = "user13";
				priority = -9;
			};
			class RampClose
			{
				displayName = "<t color='#FFD700'>Close Ramp";
				position = "ramp action";
				showWindow = 0;
				radius = 5;
				condition = "(this animationPhase ""ramp_bottom"" > 0.56) && (player in [driver this,gunner this]) && (alive this)";
				statement = "this animate ['ramp_bottom',0];this animate ['ramp_top',0];";
				onlyforplayer = 0;
				shortcut = "user12";
				priority = -9;
			};
			class MoveInside: RampOpen
			{
				displayName = "Move inside";
				condition = "player in this and ((getpos this select 2)>50)";
				statement = "[this] spawn rhs_fnc_moveInside";
				shortcut = "";
			};
			class VehicleParadrop: MoveInside
			{
				displayName = "Paradrop cargo";
				condition = "(count (attachedObjects this) > 0) AND ('man' countType (attachedObjects this) == 0) AND Alive(this)";
				statement = "[this] spawn rhs_fnc_vehPara";
			};
		};
		class pilotCamera
		{
			turretInfoType = "RscOptics_UAV_gunner";
			showHMD = 0;
			stabilizedInAxes = 3;
			minElev = -20;
			maxElev = 90;
			initElev = 0;
			minTurn = -110;
			maxTurn = 110;
			initTurn = 0;
			maxXRotSpeed = 0.5;
			maxYRotSpeed = 0.5;
			pilotOpticsShowCursor = 1;
			controllable = 1;
			class OpticsIn
			{
				class Wide
				{
					opticsDisplayName = "W";
					initAngleX = 0;
					minAngleX = -30;
					maxAngleX = 30;
					initAngleY = 0;
					minAngleY = -100;
					maxAngleY = 100;
					initFov = 0.1;
					minFov = 0.0022;
					maxFov = 1.1;
					directionStabilized = 1;
					visionMode[] = {"Normal","NVG","TI"};
					thermalMode[] = {0,1};
					gunnerOpticsModel = "\A3\Weapons_F_Beta\Reticle\Optics_Commander_01_F";
				};
				showMiniMapInOptics = 1;
				showUAVViewpInOptics = 0;
				showSlingLoadManagerInOptics = 1;
			};
		};
		memoryPointDriverOptics = "flir_end";
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				gunnerAction = "RHS_CH53_Pilot";
				gunnerInAction = "RHS_CH53_Pilot";
				memoryPointsGetInGunner = "pos codriver";
				memoryPointsGetInGunnerDir = "pos codriver dir";
				gunnerGetInAction = "GetInLow";
				gunnerGetOutAction = "GetOutLow";
				GunnerDoor = "";
				LODTurnedIn = 1000;
				LODTurnedOut = 1000;
				body = "ObsTurret";
				gun = "ObsGun";
				animationSourceBody = "ObsTurret";
				animationSourceGun = "ObsGun";
				gunBeg = "flir_end";
				gunEnd = "flir_begin";
				memoryPointGun = "flir_end";
				memoryPointGunnerOptics = "commanderview";
				stabilizedInAxes = 3;
				minElev = -90;
				maxElev = 38.2;
				initElev = 0;
				minTurn = -70;
				maxTurn = 70;
				initTurn = 0;
				gunnerName = "$STR_A3_COPILOT";
				isCopilot = 1;
				turretInfoType = "RHS_RscUH1Y_Observer";
				soundServo[] = {"",0.01,1.0,30};
				weapons[] = {"Laserdesignator_mounted"};
				magazines[] = {"Laserbatteries"};
				inGunnerMayFire = 1;
				precisegetinout = 0;
				gunnerOpticsEffect[] = {};
				gunnerOpticsModel = "";
				gunnerLeftHandAnimName = "lever_copilot";
				gunnerRightHandAnimName = "stick_copilot";
				gunnerLeftLegAnimName = "pedalL";
				gunnerRightLegAnimName = "pedalR";
				usePiP = 1;
				CanEject = 1;
				primaryGunner = 1;
				proxyIndex = 1;
				commanding = -1;
				gunnerOpticsShowCursor = 1;
				showgunneroptics = 1;
				gunnerForceOptics = 0;
				gunnerOpticsColor[] = {0.227,0.769,0.24,1};
				gunnerForceOutOptics = 0;
				gunnerUsesPilotView = 0;
				hasGunner = 1;
				hideWeaponsGunner = 1;
				lockWhenDriverOut = 0;
				enableManualFire = 1;
				maxHorizontalRotSpeed = 3.2;
				maxVerticalRotSpeed = 3.2;
				outGunnerMayFire = 1;
				startEngine = 0;
				gunnerHasFlares = 1;
				showHMD = 0;
				class OpticsIn
				{
					class Wide
					{
						opticsDisplayName = "W";
						initAngleX = 0;
						minAngleX = -30;
						maxAngleX = 30;
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = 100;
						initFov = 0.1;
						minFov = 0.0022;
						maxFov = 0.65;
						visionMode[] = {"Normal","NVG","Ti"};
						thermalMode[] = {0,1};
						horizontallyStabilized = 1;
						verticallyStabilized = 1;
						stabilizedInAxes = 3;
						gunnerOpticsModel = "\A3\Weapons_F_Beta\Reticle\Optics_Commander_01_F";
					};
				};
				class OpticsOut
				{
					class Monocular
					{
						initAngleX = 0;
						minAngleX = -30;
						maxAngleX = 30;
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = 100;
						initFov = 1.1;
						minFov = 0.133;
						maxFov = 1.1;
						visionMode[] = {"Normal","NVG"};
						gunnerOpticsModel = "";
						gunnerOpticsEffect[] = {};
					};
				};
			};
		};
	};
};
class CfgWeapons
{
// Allow Weapon Safety for UH1 and AH1
	class RocketPods;
	class uh1safe: RocketPods
	{
		CanLock = 0;
		displayName = "MASTER ARM - SAFE";
		displayNameMagazine = "MASTER ARM - SAFE";
		shortNameMagazine = "MASTER ARM - SAFE";
		nameSound = "";
		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";
		magazines[] = {};
		burst = 0;
		reloadTime = 0.01;
		magazineReloadTime = 0.1;
	};
	class ah1safe: RocketPods
	{		
		CanLock = 0;
		displayName = "MASTER ARM - SAFE";
		displayNameMagazine = "MASTER ARM - SAFE";
		shortNameMagazine = "MASTER ARM - SAFE";
		nameSound = "";
		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";
		magazines[] = {};
		burst = 0;
		reloadTime = 0.01;
		magazineReloadTime = 0.1;
	};
};
