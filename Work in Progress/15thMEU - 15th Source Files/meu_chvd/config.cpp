
#define VERSION_DATE	05.08.2016 // version

class CfgPatches
{
	class meu_chvd
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
		versionDesc = "meu_chvd";
		version = VERSION_DATE;
		author[] = {"15th Mod Team"};
	};
};

class CfgVehicles
{	
	//-- CHVD as ACE Action --//
	#include "configs\aceVD.hpp"
};