class CfgPatches
{
	class meu_aav
	{
		units[] = {"AAVB","Burnes_aav_des"};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Armor_F"};
		versionDesc = "meu_aav";
		version = "09.01.2016";
		author[] = {"15th Mod Team"};
	};
};
class CfgFunctions
{
	class meu
	{
		class aav
		{
			file = "meu_aav\functions";
			class amphibious{};
		};
	};
};
class DefaultEventHandlers;
class CfgVehicles
{
	class Tank_F;
	class AAVB: Tank_F
	{
		crew = "B_crew_F";
		scopeCurator = 2;
		armor = 200;
		class EventHandlers: DefaultEventHandlers
		{
			init = "(_this select 0) lockTurret [[2],true];";
		};
		class Damage
		{
			tex[] = {};
			mat[] = {};
		};
		class UserActions
		{
			class amphibious
			{
				displayName = "<t color='#ff0000'>Engage Amphibious Drive";
				displayNameDefault = "<t color='#ff0000'>Engage Amphibious Drive";
				condition = "(player in [driver this]) && (alive this) && !(this getVariable ['meu_fnc_amphibious_engaged',false])";
				onlyforplayer = 1;
				priority = 10;
				position = "camo1";
				radius = 5;
				showWindow = 0;
				statement = "this call meu_fnc_amphibious;";
			};
			class leftHatchOpen
			{
				displayName = "<t color='#6A98FA'>Open Left Hatch";
				displayNameDefault = "<t color='#6A98FA'>Open Left Hatch";
				condition = "player in [driver this,commander this,gunner this] && this animationPhase ""leftHatch"" == 0";
				priority = 0.1;
			};
			class leftHatchClose
			{
				displayName = "<t color='#6A98FA'>Close Left Hatch";
				displayNameDefault = "<t color='#6A98FA'>Close Left Hatch";
				condition = "player in [driver this,commander this,gunner this] && this animationPhase ""leftHatch"" == 1";
				priority = 0.1;
			};
			class RightHatchOpen
			{
				displayName = "<t color='#6A98FA'>Open Right Hatch";
				displayNameDefault = "<t color='#6A98FA'>Open Right Hatch";
				condition = "player in [driver this,commander this,gunner this] && this animationPhase ""rightHatch"" == 0";
				priority = 0;
			};
			class RightHatchClose
			{
				displayName = "<t color='#6A98FA'>Close Right Hatch";
				displayNameDefault = "<t color='#6A98FA'>Close Right Hatch";
				condition = "player in [driver this,commander this,gunner this] && this animationPhase ""RightHatch"" == 1";
				priority = 0;
			};
			class rampOpen
			{
				displayName = "<t color='#45FE2F'>Open Ramp";
				displayNameDefault = "<t color='#45FE2F'>Open Ramp";
				condition = "player in [driver this,commander this,gunner this] && this animationPhase ""rearRamp"" == 0";
				priority = 4;
			};
			class rampClose
			{
				displayName = "<t color='#45FE2F'>Close Ramp";
				displayNameDefault = "<t color='#45FE2F'>Close Ramp";
				condition = "player in [driver this,commander this,gunner this] && this animationPhase ""rearRamp"" == 1";
				priority = 4;
			};
			class Backward
			{
				condition = "false";
			};
			class Left
			{
				condition = "false";
			};
			class Right
			{
				condition = "false";
			};
			class Stop
			{
				condition = "false";
			};
			class Forward
			{
				condition = "false";
			};
		};
	};
	class Burnes_aav_des: AAVB
	{
		displayName = "AAV Desert";
		model = "\AAVB\AAVdes";
	};
};