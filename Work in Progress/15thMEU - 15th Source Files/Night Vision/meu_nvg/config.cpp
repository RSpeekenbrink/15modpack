
class CfgPatches
{
	class meu_nvg_cs
	{
		units[] = {};
		weapons[] = {"meu_ANPVS_14","meu_ANPVS_15"};
		magazine[] = {};
		ammo[] = {};
		author[] = {"15thMEU"};
		requiredVersion = 1.0;
		requiredAddons[] = {"rhs_c_troops"};
		version = 1.0;
        versionStr = "1.0";
        versionAr[] = {1.0};
        versionDesc = "15th MEU NVG Modification";
        versionAct = "";
	};
};
class CfgSettings 
{
   class CBA 
   {
      class Versioning 
      {
         class meu_nvg {};
      };
   };
};
class CfgWeapons
{
class rhsusf_ANPVS_14;
class meu_ANPVS_14: rhsusf_ANPVS_14
{
	dlc = "RHS_USAF";
	Scope=2;
	ScopeCurator=2;
	author = "RHS/15thMEU";
	displayName="AN/PVS-14 (MEU)";
	modelOptics="\meu_nvg\meu_nvg_anpvs14";
	model="\rhsusf\addons\rhsusf_infantry\gear\nvg\ANPVS_14_UP";
	picture ="\rhsusf\addons\rhsusf_infantry\ui\ico_anpvs14.paa";
	descriptionShort="AN/PVS-14 Monocular Night Vision Device";
	class ItemInfo
	{
		type=616;
		hmdType=0;
		uniformModel="\rhsusf\addons\rhsusf_infantry\gear\nvg\ANPVS_14_DOWN";
		modelOff="\rhsusf\addons\rhsusf_infantry\gear\nvg\ANPVS_14_UP";
		mass=20;
	};
};
class meu_ANPVS_15: rhsusf_ANPVS_14
{
	displayName="AN/PVS-15 (MEU)";
	author = "RHS/15thMEU";
	modelOptics = "\meu_nvg\meu_nvg_anpvs15";
	model="\rhsusf\addons\rhsusf_infantry\gear\nvg\ANPVS_15_UP";
	picture ="\rhsusf\addons\rhsusf_infantry\ui\ico_anpvs15.paa";
	descriptionShort="AN/PVS-15 Binocular Night Vision Device";
	class ItemInfo
	{
		type=616;
		hmdType=0;
		uniformModel="\rhsusf\addons\rhsusf_infantry\gear\nvg\ANPVS_15_DOWN";
		modelOff="\rhsusf\addons\rhsusf_infantry\gear\nvg\ANPVS_15_UP";
		mass=20;
	};
};
};