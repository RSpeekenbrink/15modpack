
#define VERSION_DATE	05.28.2016 // version

class CfgPatches
{
	class meu_checkmod
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
		versionDesc = "meu_checkmod";
		version = VERSION_DATE;
		author[] = {"15th Mod Team"};
	};
};
class CfgFunctions 
{
	class meu
	{
		class checkmod
		{
			file = "meu_checkmod\functions";
			class checkMods {};
			class moduleCheckMods{};
		};
	};
};
class cfgFactionClasses
{
	class NO_CATEGORY;
	class meu_modules: NO_CATEGORY
	{
		displayName = "[15th] Modules";
	};
};

class CfgVehicles
{	
	//-- check mods module --//
	#include "configs\moduleCheckMods.hpp"
};