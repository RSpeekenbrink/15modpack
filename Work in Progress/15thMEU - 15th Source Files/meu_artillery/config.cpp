class CfgPatches
{
	class A3_Modules_F_Curator_Ordnance
	{
		units[] = {"ModuleOrdnance_F","ModuleOrdnanceMortar_F","ModuleOrdnanceHowitzer_F","ModuleOrdnanceRocket_F","ModuleOrdnanceDummyMortar_F","ModuleOrdnanceDummyHowitzer_F","ModuleOrdnanceDummyRocket_F"};
		weapons[] = {};
		requiredVersion = 1;
		requiredAddons[] = {"A3_Modules_F_Curator","A3_Modules_F_Effects"};
	};
};
class CfgAmmo
{
	class ModuleOrdnanceMortar_F_ammo;
	class ModuleOrdnanceDummyMortar_F_ammo: ModuleOrdnanceMortar_F_ammo
	{
		hit = 0;
		indirectHit = 0;
	};
	class ModuleOrdnanceHowitzer_F_ammo;
	class ModuleOrdnanceDummyHowitzer_F_ammo: ModuleOrdnanceHowitzer_F_ammo
	{
		hit = 0;
		indirectHit = 0;
	};
	class ModuleOrdnanceRocket_F_ammo;
	class ModuleOrdnanceDummyRocket_F_ammo: ModuleOrdnanceRocket_F_ammo
	{
		submunitionAmmo = "ModuleOrdnanceDummyRocket_F_subammo";
		submunitionConeType[] = {"poissondisccenter",10};
		submunitionConeAngle = 25;
		triggerDistance = 100;
	};
	class ModuleOrdnanceRocket_F_subammo;
	class ModuleOrdnanceDummyRocket_F_subammo: ModuleOrdnanceRocket_F_subammo
	{
		indirectHit = 0;
		indirectHitRange = 0;
	};
};
class CfgVehicles
{
	class Logic;
	class Module_F: Logic
	{
		class ModuleDescription
		{
			class AnyPlayer;
		};
	};
	class ModuleOrdnance_F: Module_F
	{
		author = "$STR_A3_Bohemia_Interactive";
		_generalMacro = "ModuleOrdnance_F";
		scope = 2;
		category = "Effects";
		function = "BIS_fnc_moduleProjectile";
		isGlobal = 0;
		isTriggerActivated = 1;
		displayName = "$STR_A3_CfgVehicles_ModuleOrdnance_F";
		icon = "\a3\Modules_F_Curator\Data\iconOrdnance_ca.paa";
		portrait = "\a3\Modules_F_Curator\Data\portraitOrdnance_ca.paa";
		class Arguments
		{
			class Type
			{
				displayName = "$STR_A3_CfgVehicles_ModuleOrdnance_F_Arguments_Type";
				description = "";
				class values
				{
					class Mortar
					{
						name = "$STR_A3_CfgVehicles_ModuleOrdnanceMortar_F";
						value = "ModuleOrdnanceMortar_F_Ammo";
						default = 1;
					};
					class Howitzer
					{
						name = "$STR_A3_CfgVehicles_ModuleOrdnanceHowitzer_F";
						value = "ModuleOrdnanceHowitzer_F_Ammo";
					};
					class Rocket
					{
						name = "$STR_A3_CfgVehicles_ModuleOrdnanceRocket_F";
						value = "ModuleOrdnanceRocket_F_Ammo";
					};
					class DummyMortar
					{
						name = "DummyMortar";
						value = "ModuleOrdnanceDummyMortar_F_Ammo";
					};
					class DummyHowitzer
					{
						name = "DummyHowitzer";
						value = "ModuleOrdnanceDummyHowitzer_F_Ammo";
					};
					class DummyRocket
					{
						name = "DummyRocket";
						value = "ModuleOrdnanceDummyRocket_F_Ammo";
					};
				};
			};
		};
		class ModuleDescription: ModuleDescription
		{
			description = "$STR_A3_CfgVehicles_ModuleOrdnance_F_ModuleDescription";
			position = 1;
		};
	};
	class ModuleOrdnanceMortar_F: ModuleOrdnance_F
	{
		author = "$STR_A3_Bohemia_Interactive";
		_generalMacro = "ModuleOrdnanceMortar_F";
		scope = 1;
		scopeCurator = 2;
		isGlobal = 1;
		category = "Ordnance";
		displayName = "$STR_A3_CfgVehicles_ModuleOrdnanceMortar_F";
		portrait = "\a3\Modules_F_Curator\Data\portraitOrdnanceMortar_ca.paa";
		ammo = "ModuleOrdnanceMortar_F_Ammo";
		delete Arguments;
		simulation = "house";
		model = "\a3\Modules_F_Curator\Ordnance\surfaceMortar.p3d";
		curatorCost = 1;
	};
	class ModuleOrdnanceHowitzer_F: ModuleOrdnanceMortar_F
	{
		author = "$STR_A3_Bohemia_Interactive";
		_generalMacro = "ModuleOrdnanceHowitzer_F";
		displayName = "$STR_A3_CfgVehicles_ModuleOrdnanceHowitzer_F";
		portrait = "\a3\Modules_F_Curator\Data\portraitOrdnanceHowitzer_ca.paa";
		ammo = "ModuleOrdnanceHowitzer_F_Ammo";
		model = "\a3\Modules_F_Curator\Ordnance\surfaceHowitzer.p3d";
		curatorCost = 3;
	};
	class ModuleOrdnanceRocket_F: ModuleOrdnanceMortar_F
	{
		author = "$STR_A3_Bohemia_Interactive";
		_generalMacro = "ModuleOrdnanceRocket_F";
		displayName = "$STR_A3_CfgVehicles_ModuleOrdnanceRocket_F";
		portrait = "\a3\Modules_F_Curator\Data\portraitOrdnanceRocket_ca.paa";
		ammo = "ModuleOrdnanceRocket_F_Ammo";
		model = "\a3\Modules_F_Curator\Ordnance\surfaceRocket.p3d";
		curatorCost = 9;
	};
	class ModuleOrdnanceDummyMortar_F: ModuleOrdnanceMortar_F
	{
		displayName = "DummyMortar";
		ammo = "ModuleOrdnanceDummyMortar_F_Ammo";
	};
	class ModuleOrdnanceDummyHowitzer_F: ModuleOrdnanceHowitzer_F
	{
		displayName = "DummyHowitzer";
		ammo = "ModuleOrdnanceDummyHowitzer_F_Ammo";
	};
	class ModuleOrdnanceDummyRocket_F: ModuleOrdnanceRocket_F
	{
		displayName = "DummyRocket";
		ammo = "ModuleOrdnanceDummyRocket_F_Ammo";
	};
};
class PreloadTextures
{
	class CfgVehicles
	{
		class ModuleOrdnanceMortar_F
		{
			model = "@*";
		};
		class ModuleOrdnanceHowitzer_F
		{
			model = "@*";
		};
		class ModuleOrdnanceRocket_F
		{
			model = "@*";
		};
		class ModuleOrdnanceDummyMortar_F
		{
			model = "@*";
		};
		class ModuleOrdnanceDummyHowitzer_F
		{
			model = "@*";
		};
		class ModuleOrdnanceDummyRocket_F
		{
			model = "@*";
		};
	};
};
