
class CfgPatches
{
	class MEU_c2
	{
		units[] = {"meu_c2_woodland","meu_c2_desert"};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {"A3_Soft_F_MRAP_01"};
		version = 1.0;
        versionStr = "1.0";
        versionAr[] = {1.0};
        versionDesc = "15th MEU C2";
        versionAct = "";
	};
};
class CfgSettings 
{
   class CBA 
   {
      class Versioning 
      {
         class MEU_c2 {};
      };
   };
};
class CfgVehicles
{
	

	class rhsusf_m998_w_s_2dr_fulltop;
	class meu_c2_woodland: rhsusf_m998_w_s_2dr_fulltop
	{
		faction = "rhs_faction_usmc_wd";
		scope = 2;
		vehicleClass = "rhs_vehclass_car";
		crew = "rhsusf_usmc_marpat_wd_rifleman_m4";
		author = "RHS/15thMEU";
		maxFordingDepth = 0.5;
		tf_range = 80000;
		tf_hasLRradio_api = 1;
	    tf_isolatedAmount = 0.1;
        tf_subtype_api = "digital_lr"; // Radio subtype – affects DSP effects. Valid values are – digital_lr, digital, airborne
		displayname = "M1123 C2 Woodland";
		HiddenSelectionsTextures[] = {"rhsusf\addons\rhsusf_hmmwv\textures\m998_exterior_w_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\m998_interior_w_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\A2_parts_WD_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\wheel_wranglermt_b_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\m998_mainbody_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\gratting_w_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\m998_interior_wood_w_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\m998_2drcargo_w_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\tile_exmetal_co.paa","rhsusf\addons\rhsusf_hmmwv\unitdecals\usmc_210321_ca.paa",""};
	};
	class meu_c2_desert: meu_c2_woodland
	{
		faction = "rhs_faction_usmc_d";
		crew = "rhsusf_usmc_marpat_d_rifleman_m4";
		displayname = "M1123 C2 Desert";
		HiddenSelectionsTextures[] = {"rhsusf\addons\rhsusf_hmmwv\textures\m998_exterior_d_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\m998_interior_d_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\A2_parts_D_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\wheel_wranglermt_d_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\m998_mainbody_d_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\gratting_d_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\m998_interior_wood_d_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\m998_2drcargo_d_co.paa","rhsusf\addons\rhsusf_hmmwv\textures\tile_exmetal_d_co.paa","rhsusf\addons\rhsusf_hmmwv\unitdecals\usmc_210321_ca.paa",""};
	};
};