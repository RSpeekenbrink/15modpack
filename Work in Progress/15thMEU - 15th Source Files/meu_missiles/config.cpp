//15thMissiles
//Temporary modification of in-game missile speed and acceleration for testing purposes

class CfgPatches
{
	class 15thmissiles
	{
		units[] = {};
		weapons[] = {};
		author[] = {"15thMEU"};
		requiredVersion = 1.32;
		requiredAddons[] = {"A3_Weapons_F"};
	};
};

//cfgAmmo Define
class CfgAmmo 
{
	class M_Titan_AA; //loading base class
	//Modify the top speed of the missile only.
	class 15meu_missile_100as : M_Titan_AA //default: maxSpeed = 850, thrustTime = 2.5, thrust = 385
	{
		maxSpeed = 850;
	};
	class 15meu_missile_75as : M_Titan_AA //default: maxSpeed = 850, thrustTime = 2.5, thrust = 385
	{
		maxSpeed = 638;
	};
	class 15meu_missile_50as : M_Titan_AA //default: maxSpeed = 850, thrustTime = 2.5, thrust = 385
	{
		maxSpeed = 425;
	};
	//Modify the rate of acceleration of the missile only. Thrust time increased to match decrease in total thrust.
	class 15meu_missile_100at : M_Titan_AA //default: maxSpeed = 850, thrustTime = 2.5, thrust = 385
	{
		thrustTime = 2.5;
		thrust = 385;
	};
	class 15meu_missile_75at : M_Titan_AA //default: maxSpeed = 850, thrustTime = 2.5, thrust = 385
	{
		thrustTime = 3.3;
		thrust = 288;
	};
	class 15meu_missile_50at : M_Titan_AA //default: maxSpeed = 850, thrustTime = 2.5, thrust = 385
	{
		thrustTime = 5.0;
		thrust = 192;
	};
};
	
//cfgMagazines Define
class CfgMagazines 
{
class Titan_AA; //loading base class
	//Creating Missile to be fired by Titan Launcher
	class 15meu_missile_100ms : Titan_AA
	{
		author = "15thMEU";
		scope = 2;
		displayName = "MEU Missile 100 percent Speed";
		displayNameShort = "MEU Missile 100s";
		ammo = "15meu_missile_100as";
		descriptionShort = "100 percent Speed";
	};
	class 15meu_missile_750ms : Titan_AA
	{
		author = "15thMEU";
		scope = 2;
		displayName = "MEU Missile 75 percent Speed";
		displayNameShort = "MEU Missile 75s";
		ammo = "15meu_missile_75as";
		descriptionShort = "75 percent Speed";
	};
	class 15meu_missile_50ms : Titan_AA
	{
		author = "15thMEU";
		scope = 2;
		displayName = "MEU Missile 50 percent Speed";
		displayNameShort = "MEU Missile 50s";
		ammo = "15meu_missile_50as";
		descriptionShort = "50 percent Speed";
	};
	class 15meu_missile_100mt : Titan_AA
	{
		author = "15thMEU";
		scope = 2;
		displayName = "MEU Missile 100 percent Thrust";
		displayNameShort = "MEU Missile 100t";
		ammo = "15meu_missile_100at";
		descriptionShort = "100 percent Thrust";
	};
	class 15meu_missile_750mt : Titan_AA
	{
		author = "15thMEU";
		scope = 2;
		displayName = "MEU Missile 75 percent Thrust";
		displayNameShort = "MEU Missile 75t";
		ammo = "15meu_missile_75at";
		descriptionShort = "75 percent Thrust";
	};
	class 15meu_missile_50mt : Titan_AA
	{
		author = "15thMEU";
		scope = 2;
		displayName = "MEU Missile 50 percent Thrust";
		displayNameShort = "MEU Missile 50t";
		ammo = "15meu_missile_50at";
		descriptionShort = "50 percent Thrust";
	};
};

class CfgWeapons
{
	class Launcher_Base_F;
	class launch_Titan_base: Launcher_Base_F
	{
		magazines[] = {"Titan_AA","15meu_missile_100ms","15meu_missile_750ms","15meu_missile_50ms","15meu_missile_100mt","15meu_missile_750mt","15meu_missile_50mt"};
	};
};
//end