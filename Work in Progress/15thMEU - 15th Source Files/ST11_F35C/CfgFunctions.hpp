class CfgFunctions
{
	class SERVICE_MENU
	{
		class SERVICE_MENU
		{
			class CREDITS_EULA_UI
			{
				file = "\ST11_F35C\functions\CREDITS_EULA_UI.sqf";
			};
			class LOADOUT_APPLY
			{
				file = "\ST11_F35C\functions\LOADOUT_APPLY.sqf";
			};
			class LOADOUT_UI
			{
				file = "\ST11_F35C\functions\LOADOUT_UI.sqf";
			};
			class LOADOUT_1
			{
				file = "\ST11_F35C\functions\LOADOUT_1.sqf";
			};
			class LOADOUT_2
			{
				file = "\ST11_F35C\functions\LOADOUT_2.sqf";
			};
			class LOADOUT_3
			{
				file = "\ST11_F35C\functions\LOADOUT_3.sqf";
			};
			class PAINT_SCHEME_1
			{
				file = "\ST11_F35C\functions\PAINT_SCHEME_1.sqf";
			};
			class PAINT_SCHEME_2
			{
				file = "\ST11_F35C\functions\PAINT_SCHEME_2.sqf";
			};
			class PAINT_SCHEME_3
			{
				file = "\ST11_F35C\functions\PAINT_SCHEME_3.sqf";
			};
			class PAINT_SCHEME_4
			{
				file = "\ST11_F35C\functions\PAINT_SCHEME_4.sqf";
			};
			class LOADOUT_EMPTY
			{
				file = "\ST11_F35C\functions\LOADOUT_EMPTY.sqf";
			};
			class MAIN_UI
			{
				file = "\ST11_F35C\functions\MAIN_UI.sqf";
			};
			class MANUAL_UI
			{
				file = "\ST11_F35C\functions\MANUAL_UI.sqf";
			};
			class RETURN_MAIN
			{
				file = "\ST11_F35C\functions\RETURN_MAIN.sqf";
			};
			class SETTINGS_UI
			{
				file = "\ST11_F35C\functions\SETTINGS_UI.sqf";
			};
			class SKINS_UI
			{
				file = "\ST11_F35C\functions\SKINS_UI.sqf";
			};
			class REARM
			{
				file = "\ST11_F35C\functions\REARM.sqf";
			};
			class REFUEL
			{
				file = "\ST11_F35C\functions\REFUEL.sqf";
			};
			class GPS_TARGETING_INIT
			{
				file="\ST11_F35C\functions\GPS_TARGETING_INIT.sqf";
			};
			class GPS_TARGETING_SYS
			{
				file="\ST11_F35C\functions\GPS_TARGETING_SYS.sqf";
			};
			class GPS_TARGETING_CANCEL
			{
				file="\ST11_F35C\functions\GPS_TARGETING_CANCEL.sqf";
			};
			class REPAIR
			{
				file = "\ST11_F35C\functions\REPAIR.sqf";
			};
		};
	};
};