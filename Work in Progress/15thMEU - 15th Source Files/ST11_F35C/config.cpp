#include "basicdefines_A3.hpp"

class CfgPatches
{
	class F_35C
	{
		units[] = { "F_35C" };
		weapons[] = {};
		requiredVersion = 0.1;
	};
};
class CfgMovesBasic
{
	class DefaultDie;
	class ManActions
	{
		pilot_plane_OK_F_35C = "Pilot_Plane_OK_F_35C";
		pilot_plane_OK_F_35C_Exit = "Pilot_Plane_OK_F_35C_getout";
		pilot_plane_OK_F_35C_Enter = "Pilot_Plane_OK_F_35C_getin";
	};
};
class CfgMovesMaleSdr : CfgMovesBasic
{
	skeletonName = "OFP2_ManSkeleton";
	gestures = "CfgGesturesMale";
	class States
	{
		class Crew;
		class pilot_plane_cas_01;
		class pilot_plane_cas_01_getin;
		class pilot_plane_cas_01_getout;
		class pilot_plane_OK_F_35C : pilot_plane_cas_01
		{
			file = "\ST11_F35C\data\Anim\F_35C_Pilot_static.rtm";
			speed = 0.33;
		};
		class pilot_plane_OK_F_35C_getout : pilot_plane_cas_01_getout
		{
			file = "\ST11_F35C\data\Anim\F_35C_Pilot_getout.rtm";
		};
		class pilot_plane_OK_F_35C_getin : pilot_plane_cas_01_getin
		{
			file = "\ST11_F35C\data\Anim\F_35C_Pilot_getin.rtm";
		};
	};
};

		#include "CfgFunctions.hpp"
		#include "CfgService_dialog.hpp"
		
		
		class RscMapControl;
class TARGETING_UI
{
	idd=3000;
	movingenable="false";
	onLoad="uiNamespace setVariable ['GPS_TGT_Display', (_this select 0)]";
	class controlsBackground
	{
		class TARGETING_MAP: RscMapControl
		{
			idc=1200;
			maxSatelliteAlpha=0;
			x="0.290021 * safezoneW + safezoneX";
			y="0.177928 * safezoneH + safezoneY";
			w="0.406835 * safezoneW";
			h="0.588131 * safezoneH";
		};
	};
	class Controls
	{
		class MFD_OVERLAY_BACKGROUND: RscPicture
		{
			idc=1201;
			text="#(argb,8,8,3)color(0,0,0,0.15)";
			x="0.275585 * safezoneW + safezoneX";
			y="0.155524 * safezoneH + safezoneY";
			w="0.438334 * safezoneW";
			h="0.688954 * safezoneH";
		};
		class MFD_OVERLAY_GPS: RscPicture
		{
			idc=1201;
			text="\ST11_F35C\UI\GPS_dialog_bg_ca.paa";
			x="0.275585 * safezoneW + safezoneX";
			y="0.155524 * safezoneH + safezoneY";
			w="0.438334 * safezoneW";
			h="0.688954 * safezoneH";
		};
		class MAIN_FRAME: RscFrame
		{
			idc=1205;
			x="0.275585 * safezoneW + safezoneX";
			y="0.155524 * safezoneH + safezoneY";
			w="0.438334 * safezoneW";
			h="0.688954 * safezoneH";
		};
		class SET_TGT_BUTTON_1610: RscButtonMenu
		{
			idc=1610;
			x="0.287395 * safezoneW + safezoneX";
			y="0.796867 * safezoneH + safezoneY";
			w="0.129925 * safezoneW";
			h="0.0336076 * safezoneH";
			text="";
			size=0.023;
			tooltip="Set GPS/INS target position";
			action="[]Spawn SERVICE_MENU_fnc_GPS_TARGETING_SYS";
		};
		class CLEAR_TGT_BUTTON_1611: RscButtonMenu
		{
			idc=1611;
			x="0.429132 * safezoneW + safezoneX";
			y="0.796867 * safezoneH + safezoneY";
			w="0.129925 * safezoneW";
			h="0.0336076 * safezoneH";
			text="";
			size=0.023;
			tooltip="Clear current GPS/INS target";
			action="[]Spawn SERVICE_MENU_fnc_GPS_TARGETING_CANCEL";
		};
		class CLOSE_DIALOG_BUTTON_1613: RscButtonMenu
		{
			idc=1613;
			x="0.570868 * safezoneW + safezoneX";
			y="0.796867 * safezoneH + safezoneY";
			w="0.129925 * safezoneW";
			h="0.0336076 * safezoneH";
			text="";
			size=0.023;
			tooltip="Close this menu";
			action="closeDialog 0";
		};
		class INFO_TXT_1: RscText
		{
			idc=5004;
			colorText[]={0,0,0,1};
			text="GPS TGT NOT SET/ACTIVE";
			x="0.355637 * safezoneW + safezoneX";
			y="0.710047 * safezoneH + safezoneY";
			w="0.275598 * safezoneW";
			h="0.0420094 * safezoneH";
		};
	};
};

class CfgCloudlets
{
	class BodyTrail{};
	class BodyTrailMed{};
	//class ExhaustSmoke1Jet {};
	//class OK_F_35C_Afterburn1 : ExhaustSmoke1Jet
	//{
	//	particleShape = "\OK_F_35C\data\particles\afterburn";
	//	particleFSNtieth = 1;
	//	particleFSIndex = 0;
	//	particleFSFrameCount = 1;
	//	particleFSLoop = 1;
	//	rotationVelocity = 0;
	//	rotationVelocityVar = 0;
	//	lifeTime = "0.1";
	//	lifeTimeVar = 0;
	//	interval = "0.003";
	//	circleRadius = 0;
	//	circleVelocity[] = { 0, 0, 0 };
	//	angleVar = 0;
	//	animationName = "";
	//	particleType = "Billboard";
	//	timerPeriod = 1;
	//	moveVelocity[] =
	//	{
	//		"positionX",
	//		"positionY",
	//		"positionZ"
	//	};
	//	weight = 0;
	//	volume = 0;
	//	rubbing = 0;
	//	size[] = { 1, 1 };
	//	sizeVar = 0;
	//	sizeCoef = "1";
	//	color[] = { { 1, 1, 1, 1 }, { 1, 1, 1, 0 } };
	//	colorCoef[] =
	//	{
	//		"0.2",
	//		"0.5",
	//		"1",
	//		"1"
	//	};
	//	animationSpeed[] = { 1 };
	//	animationSpeedCoef = 1;
	//	randomDirectionPeriod = 0;
	//	randomDirectionIntensity = 0;
	//	onTimerScript = "";
	//	beforeDestroyScript = "";
	//	blockAIVisibility = 0;
	//	destroyOnWaterSurface = 1;
	//	destroyOnWaterSurfaceOffset = 0;
	//	position[] =
	//	{
	//		"positionX",
	//		"positionY",
	//		"positionZ"
	//	};
	//	positionVar[] = { 0, 0, 0 };
	//	MoveVelocityVar[] =
	//	{
	//		"0",
	//		"0",
	//		"0"
	//	};
	//	
	//	
	//	colorVar[] = { 0, 0, 0, 0 };
	//	randomDirectionPeriodVar = 0;
	//	randomDirectionIntensityVar = 0;
	//};
	class OK_F_35C_BodyTrail : BodyTrail
	{
		lifeTime = 0.0002;
		lifeTimeVar = 0.0001;
		size[] = { 1.5 };
		sizeVar = 0.75;
		colorCoef[] =
		{
			1,
			1,
			1,
			"(accelY interpolate [80,250,0,1]) * ((altitude*altitude) interpolate [400,10000,2,0.5])"
		};
	};
	class OK_F_35C_BodyTrailMed : BodyTrailMed
	{
		lifeTime = 0.0001;
		lifeTimeVar = 0.00005;
		size[] = { 1.5 };
		sizeVar = 0.75;
		colorCoef[] =
		{
			1,
			1,
			1,
			"(accelY interpolate [80,250,0,1]) * ((altitude*altitude) interpolate [400,10000,2,0.5])"
		};
	};
};
class BodyVortices {};
//class ExhaustEffectJet {};
class OK_F_35C_BodyVortices : BodyVortices
{
	class BodyTrail1
	{
		simulation = "particles";
		type = "OK_F_35C_BodyTrail";
		qualityLevel = 2;
		enabled = "accelY interpolate [79.9999999,80,-1,1]";
	};
	class BodyTrail1Med
	{
		simulation = "particles";
		type = "OK_F_35C_BodyTrailMed";
		qualityLevel = 1;
		enabled = "accelY interpolate [79.9999999,80,-1,1]";
	};
};
//class OK_F_35C_ExhaustsEffectJet : ExhaustEffectJet
//{
//	class ExhaustsEffect02 {
//		simulation = "particles";
//		type = "OK_F_35C_Afterburn1";
//		qualitylevel = 2;
//		enabled = "intensity interpolate [0.199999,0.2,0,1]";
//	};
//};
class CfgAmmo
{
	class MissileBase;
	class BombCore;
	class LaserBombCore;
	class M_Air_AA;
	class OK_F_35C_gunpod : BombCore
	{
//		model = "\OK_F_35C\bomberino\F_35C_gunpod";
		proxyShape = "\ST11_F35C\bomberino\F_35C_gunpod";
		hit = 0;
		indirectHit = 0;
	};
		class ST11_F35_Station1: M_Air_AA
	{
		hit=0;
		indirectHit=0;
		indirectHitRange=0;
		model="\ST11_F35C\data\f_35_station_01.p3d";
		proxyShape="\ST11_F35C\data\f_35_station_01.p3d";
	};
	
		class USAF_F35A_GBU53_A: LaserBombCore
	{
		model="\ST11_F35C\bomberino\F_35C_GBU53";
		proxyShape="\ST11_F35C\bomberino\F_35C_GBU53";
		maverickWeaponIndexOffset=0;
		hit=3000;
		indirectHit=1000;
		indirectHitRange=5;
		maxSpeed=850;
		airFriction=0.0099999998;
		sideAirFriction=0.18000001;
		maneuvrability=6;
		simulationStep=0.0020000001;
		lockType=0;
		irlock=1;
		airlock=0;
		laserlock=1;
		nvLock=1;
		cmImmunity=0.60000002;
		cost=1400;
		timeToLive=120;
		initTime=0;
		thrustTime=0;
		thrust=0;
		fuseDistance=0;
		CraterEffects="BombCrater";
		explosionEffects="BombExplosion";
		soundHit1[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_big_1",
			2.5118864,
			1,
			2400
		};
		soundHit2[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_big_2",
			2.5118864,
			1,
			2400
		};
		soundHit3[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_big_3",
			2.5118864,
			1,
			2400
		};
		soundHit4[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_shell_1",
			2.5118864,
			1,
			2400
		};
		soundHit5[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_shell_2",
			2.5118864,
			1,
			2400
		};
		multiSoundHit[]=
		{
			"soundHit1",
			0.2,
			"soundHit2",
			0.2,
			"soundHit3",
			0.2,
			"soundHit4",
			0.2,
			"soundHit5",
			0.2
		};
	};
	
	class ST11_F35_Station2: M_Air_AA
	{
		hit=0;
		indirectHit=0;
		indirectHitRange=0;
		model="\ST11_F35C\data\f_35_station_02.p3d";
		proxyShape="\ST11_F35C\data\f_35_station_02.p3d";
	};
	class ST11_F35_StationL: M_Air_AA
	{
		hit=0;
		indirectHit=0;
		indirectHitRange=0;
		model="\ST11_F35C\data\f_35_station_tip_left.p3d";
		proxyShape="\ST11_F35C\data\f_35_station_tip_left.p3d";
	};
	class ST11_F35_StationR: M_Air_AA
	{
		hit=0;
		indirectHit=0;
		indirectHitRange=0;
		model="\ST11_F35C\data\f_35_station_tip_right.p3d";
		proxyShape="\ST11_F35C\data\f_35_station_tip_right.p3d";
	};
	
	class USAF_F35A_gunpod: BombCore
	{
		proxyShape="\ST11_F35C\bomberino\F_35C_gunpod";
		hit=0;
		indirectHit=0;
	};
	class USAF_F35A_EMPTY: BombCore
	{
		model="";
		proxyShape="";
		hit=0;
		indirectHit=0;
	};
	class OK_F_35C_EMPTY : BombCore
	{
		model = "";
		proxyShape = "";
		hit = 0;
		indirectHit = 0;
	};
};
class CfgMagazines
{
	class VehicleMagazine;
	class USAF_MagazineBase_LGB;
	class OK_F_35C_CANNON_M : VehicleMagazine
	{
		scope = protected;
		displayName = "GAU-22/A";
		displayNameShort = "Cannon";
		ammo = "B_30mm_AP";
		count = 180;
		initSpeed = 1036;
		tracersEvery = 2;
		nameSound = "cannon";
	};
	
	class USAF_F35A_8Rnd_GBU53_M: VehicleMagazine
	{
		scope=1;
		displayName="GBU-53 SDB";
		descriptionShort="Small Diameter Bomb";
		displayNameShort="SDB";
		ammo="USAF_F35A_GBU53_A";
		initSpeed=0;
		maxLeadSpeed=1000;
		sound[]=
		{
			"",
			1,
			1
		};
		reloadSound[]=
		{
			"",
			0.00031622776,
			1
		};
		count=1;
		nameSound="cannon";
	};
	
		class ST11_1Rnd_Station1: USAF_MagazineBase_LGB
	{
		scope=2;
		ammo="ST11_F35_Station1";
		displayName="BOMB RACK X3";
	};
	class ST11_1Rnd_Station2: USAF_MagazineBase_LGB
	{
		scope=2;
		ammo="ST11_F35_Station2";
		displayName="BOMB RACK X3";
	};
	class ST11_1Rnd_StationL: USAF_MagazineBase_LGB
	{
		scope=2;
		ammo="ST11_F35_StationL";
		displayName="BOMB RACK X3";
	};
	class ST11_1Rnd_StationR: USAF_MagazineBase_LGB
	{
		scope=2;
		ammo="ST11_F35_StationR";
		displayName="BOMB RACK X3";
	};
	class USAF_F35A_CANNON_M: VehicleMagazine
	{
		scope=1;
		displayName="GAU-22/A";
		displayNameShort="Cannon";
		ammo="B_30mm_HE";
		count=500;
		initSpeed=1036;
		tracersEvery=1;
		nameSound="cannon";
	};
	class OK_F_35C_GUNPOD_M : VehicleMagazine
	{
		scope = protected;
		displayName = "FC-35 External Gun Pod";
		displayNameShort = "Gunpod";
		ammo = "OK_F_35C_gunpod";
		count = "1";
	};
};
class CfgWeapons
{
	class MissileLauncher;
	class CannonCore;
	class RocketPods;
		class USAF_F35A_CANNON_W: CannonCore
	{
		holdsterAnimValue=1;
		scope=1;
		displayName="GAU-22/A";
		displayNameMagazine="GAU-22/A 25mm";
		shortNameMagazine="GAU-22/A 25mm";
		nameSound="cannon";
		cursor="EmptyCursor";
		cursorAim="EmptyCursor";
		cursorSize=1;
		magazines[]=
		{
			"USAF_F35A_CANNON_M"
		};
		canLock=2;
		ballisticsComputer=1;
		missileLockCone=20;
		weaponLockSystem=3;
		modes[]=
		{
			"manual",
			"close",
			"near",
			"medium",
			"far"
		};
		class GunParticles
		{
			class Effect
			{
				effectName="MachineGun1";
				positionName="muzzle";
				directionName="muzzle_dir";
			};
		};
		class manual: CannonCore
		{
			displayName="GAU-22/A";
			autoFire=1;
			sounds[]=
			{
				"StandardSound"
			};
			class StandardSound
			{
				begin1[]=
				{
					"A3\sounds_f\weapons\gatling\minigun2_m134",
					1.7782794,
					1,
					2000
				};
				begin2[]=
				{
					"A3\sounds_f\weapons\gatling\minigun3_m134",
					1.7782794,
					1,
					2000
				};
				begin3[]=
				{
					"A3\sounds_f\weapons\gatling\minigun4_m134",
					1.7782794,
					1,
					2000
				};
				begin4[]=
				{
					"A3\sounds_f\weapons\gatling\minigun5_m134",
					1.7782794,
					1,
					2000
				};
				begin5[]=
				{
					"A3\sounds_f\weapons\gatling\minigun6_m134",
					1.7782794,
					1,
					2000
				};
				soundBegin[]=
				{
					"begin1",
					0.5,
					"begin2",
					0.5,
					"begin3",
					0.5,
					"begin4",
					0.5,
					"begin5",
					0.5
				};
				weaponSoundEffect="DefaultRifle";
			};
			reloadTime=0.02;
			dispersion=0.0035999999;
			soundContinuous=0;
			soundBurst=0;
			showToPlayer=1;
			burst=1;
			aiRateOfFire=0.5;
			aiRateOfFireDistance=50;
			minRange=1;
			minRangeProbab=0.0099999998;
			midRange=2;
			midRangeProbab=0.0099999998;
			maxRange=3;
			maxRangeProbab=0.0099999998;
			textureType="fullAuto";
		};
		class close: manual
		{
			showToPlayer=0;
			burst=15;
			aiRateOfFire=0.25;
			aiRateOfFireDistance=400;
			minRange=0;
			minRangeProbab=0.050000001;
			midRange=200;
			midRangeProbab=0.69999999;
			maxRange=400;
			maxRangeProbab=0.2;
		};
		class near: close
		{
			burst=10;
			aiRateOfFire=0.5;
			aiRateOfFireDistance=500;
			minRange=300;
			minRangeProbab=0.2;
			midRange=400;
			midRangeProbab=0.69999999;
			maxRange=500;
			maxRangeProbab=0.2;
		};
		class medium: close
		{
			burst=7;
			aiRateOfFire=1;
			aiRateOfFireDistance=900;
			minRange=400;
			minRangeProbab=0.2;
			midRange=700;
			midRangeProbab=0.69999999;
			maxRange=900;
			maxRangeProbab=0.2;
		};
		class far: close
		{
			burst=4;
			aiRateOfFire=1.5;
			aiRateOfFireDistance=1500;
			minRange=800;
			minRangeProbab=0.2;
			midRange=1000;
			midRangeProbab=0.40000001;
			maxRange=1500;
			maxRangeProbab=0.0099999998;
		};
	};
	
	class OK_F_35C_GBU53_int_W : RocketPods
	{
		holdsterAnimValue = 7;
		autoFire = 0;
		displayName = "GBU-53";
		reloadTime = 0.1;
		magazineReloadTime = 0.1;
		magazines[] =
		{
			"OK_F_35C_8Rnd_GBU53_M"
		};
		aiRateOfFire = 5;
		aiRateOfFireDistance = 500;
		nameSound = "";
		cursorAim = "EmptyCursor";
		textureType = "fullAuto";
		missileLockCone = 180;
		weaponLockDelay = 0.0;
		weaponLockSystem = 15;
	};
	
	class OK_F_35C_AIM120D_W : MissileLauncher
	{
		holdsterAnimValue = 3;
		canLock = 2;
		weaponLockDelay = 3;
		weaponLockSystem = "2 + 4 + 8";
//		cmImmunity = 0.6;
		lockingTargetSound[] =
		{
			"A3\Sounds_F\weapons\Rockets\locked_1",
			0.50118721,
			1
		};
		lockedTargetSound[] =
		{
			"A3\Sounds_F\weapons\Rockets\locked_3",
			0.56234133,
			2.5
		};
		displayName = "AIM-120D";
		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";
		cursorSize = 1;
		minRange = 300;
		minRangeProbab = 0.025;
		midRange = 2500;
		midRangeProbab = 0.09;
		maxRange = 9000;
		maxRangeProbab = 0.01;
		reloadTime = 0.1;
		magazineReloadTime = 0.1;
		sounds[] =
		{
			"StandardSound"
		};
		class StandardSound
		{
			begin1[] =
			{
				"A3\Sounds_F_EPC\Weapons\missile_epc_1",
				1.7782794,
				1,
				3500
			};
			soundBegin[] =
			{
				"begin1",
				1
			};
			weaponSoundEffect = "DefaultRifle";
		};
		soundFly[] =
		{
			"A3\Sounds_F\weapons\Rockets\rocket_fly_2",
			1.9952624,
			1,
			1700
		};
		magazines[] =
		{
			"OK_F_35C_2Rnd_AIM120D_M"
		};
		aiRateOfFire = 5;
		aiRateOfFireDistance = 4000;
//		textureType = "semi";
	};
	class OK_F_35C_AIM120D_int_W : OK_F_35C_AIM120D_W
	{
		displayName = "AIM-120D Internal";
		holdsterAnimValue = 5;
		magazines[] =
		{
			"OK_F_35C_2Rnd_AIM120D_int_M"
		};
	};
	class OK_F_35C_4Rnd_AIM120D_W : OK_F_35C_AIM120D_W {
		magazines[] =
		{
			"OK_F_35C_4Rnd_AIM120D_M"
		};
	};
	class OK_F_35C_4Rnd_AIM120D_int_W : OK_F_35C_AIM120D_int_W {
		magazines[] =
		{
			"OK_F_35C_4Rnd_AIM120D_int_M"
		};
	};
	class OK_F_35C_GBU32_W : RocketPods
	{
		holdsterAnimValue = 4;
		autoFire = 0;
		displayName = "GBU-32";
		reloadTime = 0.1;
		magazineReloadTime = 0.1;
		magazines[] =
		{
			"OK_F_35C_2Rnd_GBU32_M"
		};
		aiRateOfFire = 5;
		aiRateOfFireDistance = 500;
		nameSound = "";
		cursorAim = "EmptyCursor";
		textureType = "fullAuto";
		missileLockCone = 180;
		weaponLockDelay = 1;
	};
	class OK_F_35C_4Rnd_GBU32_W : OK_F_35C_GBU32_W
	{
		magazines[] =
		{
			"OK_F_35C_4Rnd_GBU32_M"
		};
	};
	class OK_F_35C_GBU32_int_W : OK_F_35C_GBU32_W
	{
		holdsterAnimValue = 6;
		displayName = "GBU-32 Internal";
		magazines[] =
		{
			"OK_F_35C_2Rnd_GBU32_int_M"
		};
		weaponLockDelay = 1;
	};
	
		class USAF_F35A_Master_Arm_Switch: RocketPods
	{
		canLock=0;
		displayName="MASTER ARM Switch OFF";
		displayNameMagazine="MASTER ARM Switch OFF";
		shortNameMagazine="MASTER ARM Switch OFF";
		nameSound="";
		cursor="EmptyCursor";
		cursorAim="EmptyCursor";
		magazines[]=
		{
			"FakeWeapon"
		};
		burst=0;
		reloadTime=1;
		magazineReloadTime=1;
	};
		class OK_F_35C_AIM9X_W : MissileLauncher
	{
		holdsterAnimValue = 2;
		weaponLockDelay = 1;
		weaponLockSystem = 15;
		lockingTargetSound[] =
		{
			"\A3\Sounds_F\weapons\Rockets\locked_1",
			0.31622776,
			1
		};
		lockedTargetSound[] =
		{
			"\A3\Sounds_F\weapons\Rockets\locked_3",
			0.31622776,
			2.5
		};
		displayName = "AIM-9X";
		displayNameMagazine = "AIM-9X Sidewinder";
		shortNameMagazine = "AIM-9X";
		cursorAim = "EmptyCursor";
		cursor = "EmptyCursor";
		minRange = 50;
		minRangeProbab = 0.025;
		midRange = 2500;
		midRangeProbab = 0.1;
		maxRange = 9000;
		maxRangeProbab = 0.01;
		sounds[] =
		{
			"StandardSound"
		};
		class StandardSound
		{
			begin1[] =
			{
				"A3\Sounds_F\weapons\Rockets\missile_2",
				1.1220185,
				1.3,
				1000
			};
			soundBegin[] =
			{
				"begin1",
				1
			};
			weaponSoundEffect = "DefaultRifle";
		};
		soundFly[] =
		{
			"A3\Sounds_F\weapons\Rockets\rocket_fly_2",
			1,
			1.5,
			700
		};
		weaponSoundEffect = "DefaultRifle";
		reloadTime = 0.1;
		magazineReloadTime = 0.1;
		magazines[] =
		{
			"OK_F_35C_2Rnd_AIM9X_M"
		};
		textureType = "semi";
	};
	class USAF_F35A_GBU53_int_W: RocketPods
	{
		holdsterAnimValue=7;
		autoFire=0;
		displayName="GBU-53";
		reloadTime=0.1;
		magazineReloadTime=0.1;
		magazines[]=
		{
			"USAF_F35A_8Rnd_GBU53_M"
		};
		aiRateOfFire=5;
		aiRateOfFireDistance=500;
		nameSound="";
		cursorAim="EmptyCursor";
		textureType="fullAuto";
		missileLockCone=180;
		weaponLockDelay=0;
		weaponLockSystem=15;
	};
	class OK_F_35C_CANNON_W : CannonCore
	{
		holdsterAnimValue = 1;
		scope = protected;
		displayName = "GAU-22/A";
		displayNameMagazine = "GAU-22/A 25mm";
		shortNameMagazine = "GAU-22/A 25mm";
		nameSound = "cannon";
		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";
		cursorSize = 1;
		magazines[] =
		{
			"OK_F_35C_CANNON_M"
		};
		canLock = 2;
		ballisticsComputer = 1;
		missileLockCone = 20;
		weaponLockSystem = 3;
		modes[] =
		{
			"manual",
			"close",
			"near",
			"medium",
			"far"
		};
		class GunParticles
		{
			class Effect
			{
				effectName = "MachineGun1";
				positionName = "muzzle";
				directionName = "muzzle_dir";
			};
		};
		class manual : CannonCore
		{
			displayName = "GAU-22/A";
			autoFire = 1;
			sounds[] =
			{
				"StandardSound"
			};
			class StandardSound
			{
				begin1[] =
				{
					"A3\sounds_f\weapons\gatling\minigun2_m134",
					1.7782794,
					1,
					2000
				};
				begin2[] =
				{
					"A3\sounds_f\weapons\gatling\minigun3_m134",
					1.7782794,
					1,
					2000
				};
				begin3[] =
				{
					"A3\sounds_f\weapons\gatling\minigun4_m134",
					1.7782794,
					1,
					2000
				};
				begin4[] =
				{
					"A3\sounds_f\weapons\gatling\minigun5_m134",
					1.7782794,
					1,
					2000
				};
				begin5[] =
				{
					"A3\sounds_f\weapons\gatling\minigun6_m134",
					1.7782794,
					1,
					2000
				};
				soundBegin[] =
				{
					"begin1",
					0.5,
					"begin2",
					0.5,
					"begin3",
					0.5,
					"begin4",
					0.5,
					"begin5",
					0.5
				};
				weaponSoundEffect = "DefaultRifle";
			};
			reloadTime = 0.02;
			dispersion = 0.0036;
			soundContinuous = 0;
			soundBurst = 0;
			showToPlayer = 1;
			burst = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 1;
			minRangeProbab = 0.0099999998;
			midRange = 2;
			midRangeProbab = 0.0099999998;
			maxRange = 3;
			maxRangeProbab = 0.0099999998;
			textureType = "fullAuto";
		};
		class close : manual
		{
			showToPlayer = 0;
			burst = 15;
			aiRateOfFire = 0.25;
			aiRateOfFireDistance = 400;
			minRange = 0;
			minRangeProbab = 0.050000001;
			midRange = 200;
			midRangeProbab = 0.69999999;
			maxRange = 400;
			maxRangeProbab = 0.2;
		};
		class near : close
		{
			burst = 10;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 500;
			minRange = 300;
			minRangeProbab = 0.2;
			midRange = 400;
			midRangeProbab = 0.7;
			maxRange = 500;
			maxRangeProbab = 0.2;
		};
		class medium : close
		{
			burst = 7;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 900;
			minRange = 400;
			minRangeProbab = 0.2;
			midRange = 700;
			midRangeProbab = 0.7;
			maxRange = 900;
			maxRangeProbab = 0.2;
		};
		class far : close
		{
			burst = 4;
			aiRateOfFire = 1.5;
			aiRateOfFireDistance = 1500;
			minRange = 800;
			minRangeProbab = 0.2;
			midRange = 1000;
			midRangeProbab = 0.4;
			maxRange = 1500;
			maxRangeProbab = 0.01;
		};
	};
};
class CfgVehicles
{
	class Plane;
	class PlaneWreck;
	class thingX;
	class Motorcycle;
	class Tank;
	class ArtilleryTarget;
	class LaserTarget;
	class Helicopter_Base_F;
	class WeaponHolder;
	class B_Pilot_F;
	class LaserTarget;
	class USAF_F35A_GPSTarget: LaserTarget
	{
		displayName="GPS Target";
		nameSound="";
		threat[]={0,0,1};
		camouflage=99;
		accuracy=0;
		alwaysTarget=1;
		type=1;
		simulation="artillerymarker";
		laserTarget=1;
		irTarget=1;
		nvTarget=0;
		artilleryTarget=0;
		allowTabLock=1;
		side=0;
		scope=1;
	};
	class F_35C_Base: Plane
	{
		displayName = "FC-37 Thunder";						/// how is the plane displayed in editor
		model = "\ST11_F35C\F_35C.p3d";								/// path to model of the plane
		accuracy = 0.3;											/// how hard it is to distinguish the type of the vehicle (bigger number means harder)
		mapSize = 17.52;

		icon = "\ST11_F35C\data\f35_icon_bw.paa"; 	/// icon in map/editor
		picture = "\ST11_F35C\data\f35_icon_bw.paa";	/// small picture in command menu
		simulation="airplane";
		driverAction = "pilot_plane_OK_F_35C";	/// what is the standard pose for the pilot, defined as animation state
		getinAction = "pilot_plane_OK_F_35C_Enter";
		getoutaction = "pilot_plane_OK_F_35C_Exit";
		//driverAction = "pilot_plane_cas_01";
		//getinAction = "pilot_plane_cas_01_Enter";
		//getoutaction = "pilot_plane_cas_01_Exit";
		precisegetinout = 1;
		_generalMacro="Plane";
		viewDriverShadow = true;
		viewCargoShadow = true;
		viewDriverShadowDiff = 0.5;
		viewDriverShadowAmb = 0.5;
		cabinOpening = 1;
		selectionFireAnim = "zasleh";

		class Library {
			libTextDesc = "The F-37 is a family of single-engine multirole fighter airctaft. It is the product of an international NATO venture. A true 5th generation fighter aircraft to replace the F/A-18, F-16 and A-10. The FC carrier variant is equipped with a tail hook, sturdier landing gear, and a wider wingspan to enable operation from aircraft carriers.";
		};

		//memory points for get in pos?
		memoryPointsGetInDriver = "GetIn_driver_pos";
		memoryPointsGetInDriverDir = "GetIn_driver_dir";

		// driver hand IK links
		driverLeftHandAnimName = "throttle_stick";
		driverRightHandAnimName = "flight_stick";
		// driver leg IK links?
		driverLeftLegAnimName = "pedal_left";
		driverRightLegAnimName = "pedal_right"; 
		memoryPointCM[] =
		{
			"FlareLauncher_1_pos"
		};
		memoryPointCMDir[] =
		{
			"FlareLauncher_1_dir"
		};
		memoryPointLDust = "WheelDust_left_pos";
		memoryPointRDust = "WheelDust_right_pos";

		LockDetectionSystem = CM_Lock_Radar;			/// this uses macros from basicDefines_A3, just add more to gain more systems for the vehicle
		incomingMissileDetectionSystem = CM_Missile;	/// for example CM_Lock_Laser + CM_Lock_Radar, parser is able to evaluate that, or simply 12 in that case

		class Turrets {};	/// single seat planes don't have any kind of turret, we need to void it

		class TransportItems{};	/// planes are usually not used to transport items, there could possibly be a few FAKs	

		class WingVortices
		{
				class WingTipLeft
				{
						effectName = "WingVortices";// name of the effect
						position = "body_vapour_L_E"; // name of the memory point in model
				};

				class WingTipRight
				{
						effectName = "WingVortices"; // name of the effect
						position = "body_vapour_R_E";// name of the memory point in model
				};
				class BodyLeft
				{
						effectName = "OK_F_35C_BodyVortices";// name of the effect
						position = "body_vapour_L_S"; // name of the memory point in model
				};

				class BodyRight
				{
						effectName = "OK_F_35C_BodyVortices"; // name of the effect
						position = "body_vapour_R_S";// name of the memory point in model
				};
		};
		class Exhausts
		{
			class Exhaust1
			{
				position = "exhaust_pos";
				direction = "exhaust_dir";
				effect = "ExhaustsEffectJet";
			};
		};
		#include "sounds.hpp" 	/// sounds are included in separate file to prevent cluttering

		landingSpeed = 190;		/// used for AI to approach the runawy, the plane should be stable at this speed
		acceleration = 300; 	/// used for AI to plan the waypoints and accelerating, doesn't affect plane performance
		maxSpeed = 1200;		/// maximal speed of the plane, affects even thrust and is base for both envelope and thrustCoef
		
		driveOnComponent[] = {"wheel_1", "wheel_2", "wheel_3"};  /// array of components to be assigned special low-friction material (usually wheels) "wheel_1","wheel_2","wheel_3"
		wheelSteeringSensitivity = 2.5;

		rudderInfluence = 0.02;		/// coefficient of rudder affecting steering of the plane
		aileronSensitivity = 1.95;		/// coefficient of ailerons affecting twisting the plane
		elevatorSensitivity = 0.9;	/// coefficient of elevators affecting changing of plane horizontal heading
		gearUpTime=6;
		gearDownTime=5;

		USAF_RefuelType = "hose";
		USAF_RefuelPoint = "fuel_point";
		USAF_RefuelAnimation = "refuel_limb";

		
		altNoForce = 35000;
		altFullForce = 13636;

		radarType = 4;

		irScanRangeMin = 500;		/// defines the range of IR sight of the vehicle
		irScanRangeMax = 10000;		/// defines the range of IR sight of the vehicle
		irScanToEyeFactor = 2;		/// defines the effectivity of IR sight of the vehicle
		showAllTargets = 2;

		canFloat = false;
		waterLeakiness = 35.0;
		camouflage = 20;
		audible = 20;

		draconicForceXCoef=8.5;
		draconicForceYCoef=2;
		draconicForceZCoef=1;
		draconicTorqueXCoef=0.15000001;
		draconicTorqueYCoef=1;
		
		airFriction0[]={100,50,12};
		airFriction1[]={100,50,12};
		airFriction2[]={100,50,12};

		/// envelope defines lift produced by the shape of the plane according to current speed relative to maxSpeed
		/// the first element of the array is for zero speed, the last for 125 % of maxSpeed, the rest in between is evenly distributed
		/// there may be as many elements as you wish, using 13 should be preferred as it modulates the 10% increase with reasonable error
		///			  0%   10%   20%   30%   40%   50%   60%   70%   80%   90%   100%  110%  125%
		envelope[]={0,1.75,5.9000001,7,9.8000002,10.3,10.5,10.9,9.1999998,7.8000002,5,3.8,0.5,0};

		
		/// angle of incidence - difference between forward and airfold chord line - def. val is 3*PI/180 (meaning three degrees)
		angleOfIndicence = "rad 1";


		//! coefficient of player's controller sensitivity (does not affect AI) (does not affect the rate of steer, only sensitivity
		elevatorControlsSensitivityCoef = 4; 
		aileronControlsSensitivityCoef = 3;
		rudderControlsSensitivityCoef = 4;

		brakeDistance = 150;

		landingAoa = "rad 8"; 	/// what AoA is the AI going to use to land the plane

		laserScanner = 1;		/// if the vehicle is able to see targets marked by laser marker 
		gunAimDown = "rad 0";	/// adjusts the aiming of gun relative to the axis of model
		headAimDown = 0.0000;	/// adjusts the view of pilot to have crosshair centred


		flapsFrictionCoef = 0.5;	/// sets the effectivity of using flaps to increase drag/lift
		
		minFireTime = 30;			/// how long does the pilot fire at one target before switching to another one

		threat[] = {0.1, 0.5, 1};		/// multiplier of cost of the vehicle in eyes of soft, armoured and air enemies

		class Reflectors			/// landing lights of the plane, turned on by AI while in night and "careless" or "safe"
		{
			class Front
			{
				color[] = {7000, 7500, 10000, 1}; 	/// defines red, green, blue and alpha components of the light
				ambient[] = {100, 100, 100};		/// the same definition format for colouring the environment around
				position = "Light_front";				/// name of memory point in model to take the origin of the light
				direction = "Light_front_end";			/// name of memory point in the model to make a vector of direction of light from it's position
				hitpoint = "Light_front";				/// name of hitpoint selection in hitpoint lod of the model to be affected by damage
				selection = "Light_front_hide";				/// name of selection in visual lods of the model that are going to be hidden while the light is off
				innerAngle = 10;					/// angle from light direction vector where the light is at full strength
				outerAngle = 60;					/// angle from light direction vector where the light is completely faded out
				coneFadeCoef = 10;					/// coefficient of fading the light between inner and outer cone angles
				intensity = 80;						/// how much does the light shine (in some strange units, just tweak until it is satisfying), rough approximation is intensity = (brightness * 50) ^ 2
				useFlare = true;					/// boolean switch if the light produces flare or not
				dayLight = false;					/// boolean switch if the light is used during day or not
				FlareSize = 4;						/// how big is the flare, using the same metrics as intensity
				size = 1;							/// defines the visible size of light, has not much of an effect now
				class Attenuation					/// describes how fast does the light dim
				{
					start = 1;						/// offset of start of the attenuation
					constant = 0;					/// constant attenuation of the light in any distance from source
					linear = 0;						/// coefficient for linear attenuation
					quadratic = 4;					/// coefficient for attenuation with square of distance from source
					
					hardLimitStart = 150;			/// distance from source where the light intensity decreases for drawing
					hardLimitEnd = 300;				/// distance from source where the light is not displayed (shorter distances increase performance)				
				};
			};
		};		

		armor = 60;				/// just some protection against missiles, collisions and explosions	
		damageResistance = 0.004;	/// for AI if it is worth to be shoot at
		armorStructural = 1;
		destrType = DestructWreck;	/// how does the vehicle behave while destroyed, this one changes to the Wreck lod of the model
		class HitPoints
		{
			class HitHull
			{
				armor=3;
				explosionShielding=1;
				material=51;
				name="Hull";
				passThrough=1;
				visual="hull";
			};
		};

		class Damage 	/// damage changes material in specific places (visual in hitPoint)
		{
			tex[] = {};
			mat[] =
			{
				"ST11_F35C\data\F_35C_int_glass.rvmat",			/// material mapped in model
				"ST11_F35C\data\F_35C_int_glass_dmg.rvmat",	/// changes to this one once damage of the part reaches 0.5
				"ST11_F35C\data\F_35C_int_glass_dmg.rvmat",	/// changes to this one once damage of the part reaches 1
				"ST11_F35C\data\F_35C_ext_glass.rvmat",
				"ST11_F35C\data\F_35C_ext_glass_dmg.rvmat",	
				"ST11_F35C\data\F_35C_ext_glass_dmg.rvmat",
				"ST11_F35C\data\F_35C_ext_1.rvmat",
				"ST11_F35C\data\F_35C_ext_1_dmg.rvmat",
				"ST11_F35C\data\F_35C_ext_1_dest.rvmat",
				"ST11_F35C\data\F_35C_ext_2.rvmat",
				"ST11_F35C\data\F_35C_ext_2_dmg.rvmat",
				"ST11_F35C\data\F_35C_ext_1_dest.rvmat",
				"ST11_F35C\data\F_35C_ext_3.rvmat",
				"ST11_F35C\data\F_35C_ext_3_dmg.rvmat",
				"ST11_F35C\data\F_35C_ext_1_dest.rvmat"
			};
		};
		hiddenSelections[] =	/// we want to allow changing of colours, this defines on what selection are the textures used
		{
			"zbytek",
			"engine"
		};
		
				class pilotCamera
		{
			class OpticsIn
			{
				class Wide
				{
					opticsDisplayName = "W";
					initAngleX = 0;
					minAngleX = 0;
					maxAngleX = "+0";
					initAngleY = 0;
					minAngleY = 0;
					maxAngleY = "+0";
					initFov = 0.1;
					minFov = 0.0022;
					maxFov = 1.1;
					horizontallyStabilized = 1;
					visionMode[] = {"Normal","NVG","TI"};
					thermalMode[] = {0,1};
					gunnerOpticsModel = "A3\drones_f\Weapons_F_Gamma\Reticle\UGV_01_Optics_Driver_F.p3d";
				};
				class Medium: Wide
				{
					opticsDisplayName = "M";
					initFov = 0.1;
					minFov = 0.1;
					maxFov = 0.1;
					horizontallyStabilized = 1;
					gunnerOpticsModel = "A3\drones_f\Weapons_F_Gamma\Reticle\UGV_01_Optics_Driver_F.p3d";
				};
				class Narrow: Wide
				{
					opticsDisplayName = "N";
					initFov = 0.0286;
					minFov = 0.0286;
					maxFov = 0.0286;
					horizontallyStabilized = 1;
					gunnerOpticsModel = "A3\drones_f\Weapons_F_Gamma\Reticle\UGV_01_Optics_Driver_F.p3d";
				};
				class Far: Wide
				{
						opticsDisplayName = "F";
						initFov = 0.019;
						minFov = 0.019;
						maxFov = 0.019;
						horizontallyStabilized = 1;
						gunnerOpticsModel = "\A3\Weapons_F_Beta\Reticle\Optics_Commander_01_F";
					
				};
				showMiniMapInOptics = 1;
				showUAVViewpInOptics = 0;
				showSlingLoadManagerInOptics = 0;
			};
			minTurn = -40;
			maxTurn = 40;
			initTurn = 0;
			minElev = -10;
			maxElev = 100;
			initElev = 0;
			maxXRotSpeed = 0.4;
			maxYRotSpeed = 0.4;
			pilotOpticsShowCursor = 1;
			controllable = 1;
	};
		memoryPointDriverOptics = "camera";
		unitInfoType = "RscOptics_AV_pilot";
		
		class MFD				/// class for helmet mounted displays, is going to be documented separately
		{
			class AirplaneHUD
			{
				#include "cfgHUD.hpp"
				helmetMountedDisplay = 1;
				helmetPosition[] = { -0.04, 0.03, 0.1 };
				helmetRight[] = { 0.08, 0, 0 }; 
				helmetDown[] = { -0, -0.06, 0 };
			};
		};
		
		
		
		class UserActions
		{
			class refuel {
				displayName = "Extend refueling nozzle";
				position = "pilot";
				priority = 6;
				radius = 20;
				showWindow = 0;
				hideOnUse = 1;
				onlyforplayer = 1;
				condition = "player in this and this animationPhase ""refuel_hatch_1"" < 0.5";
				statement = "this animate [""refuel_hatch_1"",1];this animate [""refuel_hatch_2_a"",1];this animate [""refuel_hatch_2_b"",1];this animate [""refuel_limb"",1];this animate [""refuel_hatch_1_int"",1];this animate [""refuel_hatch_2_int_a"",1];this animate [""refuel_hatch_2_int_b"",1];this animate [""refuel_limb_int"",1];this animate [""refuel_mfd"",1];";
			};
			class unRefuel {
				displayName = "Retract refueling nozzle";
				position = "pilot";
				priority = 6;
				radius = 20;
				showWindow = 0;
				hideOnUse = 1;
				onlyforplayer = 1;
				condition = "player in this and this animationPhase ""refuel_hatch_1"" > 0.5";
				statement = "this animate [""refuel_hatch_1"",0];this animate [""refuel_hatch_2_a"",0];this animate [""refuel_hatch_2_b"",0];this animate [""refuel_limb"",0];this animate [""refuel_hatch_1_int"",0];this animate [""refuel_hatch_2_int_a"",0];this animate [""refuel_hatch_2_int_b"",0];this animate [""refuel_limb_int"",0];this animate [""refuel_mfd"",0];";
			};
			class service_menu
			{
				priority = 10;
				displayName = "Service Menu";
				position = "pilotcontrol";
				onlyforplayer=1;
				showWindow = 0;
				hideOnUse = 1;
				radius = 5;
				condition = "((this distance (nearestObject [this, ""B_Truck_01_ammo_F""]) < 25) and (damage (nearestObject [this, ""B_Truck_01_ammo_F""]) < 1) and player == driver this and this animationPhase ""service_menu_switch"" < 0.5 and speed this < 1) or ((this distance (nearestObject [this, ""Land_TentHangar_V1_F""]) < 55) and (damage (nearestObject [this, ""Land_TentHangar_V1_F""]) < 1) and player == driver this and this animationPhase ""service_menu_switch"" < 0.5 and speed this < 1) or ((this distance (nearestObject [this, ""Land_Hangar_F""]) < 55) and (damage (nearestObject [this, ""Land_Hangar_F""]) < 1) and player == driver this and this animationPhase ""service_menu_switch"" < 0.5 and speed this < 1)";
				statement = "this animate [""switch_service_menu"",1]; [] spawn SERVICE_MENU_fnc_MAIN_UI;";
			};
			class GPS_targeting_system
			{
				displayName="GPS / INS Guidance";
				shortcut="User3";
				position="pilotcontrol";
				radius=10;
				onlyforplayer=1;
				showWindow=0;
				hideOnUse=1;
				condition="player in this and ((""USAF_F35A_8Rnd_GBU53_M"" in magazines this) or (""USAF_1Rnd_GBU31"" in magazines this) or (""USAF_1Rnd_GBU32"" in magazines this) or (""USAF_1Rnd_AGM154A"" in magazines this) or (""USAF_1Rnd_GBU12"" in magazines this))";
				statement="[this] spawn SERVICE_MENU_fnc_GPS_TARGETING_INIT";
			};
			class afterburner
			{
				displayName = "Afterburner On";
				condition = "this animationPhase ""ab_switch"" == 0 and player in this";
				statement = "this animate [""ab_switch"",1]; [this] execVM ""ST11_F35C\functions\Afterburner.sqf""";
				position = "pilotcontrol";
				radius = 10;
				onlyforplayer = 1;
				showWindow = 0;
				hideOnUse = 1;
			};
			class afterburneroff
			{
				displayName = "Afterburner Off";
				condition = "this animationPhase ""ab_switch"" == 1 and player in this";
				statement = "this animate [""ab_switch"",0]";
				position = "pilotcontrol";
				radius = 10;
				onlyforplayer = 1;
				showWindow = 0;
				hideOnUse = 1;
			};
			class pilots_manual
			{
				displayName = "Pilots Manual";
				position = "pilotcontrol";
				onlyforplayer = 1;
				showWindow = 0;
				hideOnUse = 1;
				radius = 5;
				condition = "player in this and speed this < 1";
				statement = "[] spawn SERVICE_MENU_fnc_MANUAL_SETTINGS_UI;";
			};
		//	class tailHook {
		//		displayName = "Open Tail Hook Hatch";
		//		position = "pilot";
		//		priority = 5;
		//		radius = 20;
		//		showWindow = 0;
		//		hideOnUse = 1;
		//		onlyforplayer = 1;
		//		condition = "player in this and this animationPhase ""hook_hatch_1"" < 0.5";
		//		statement = "this animate [""hook_hatch_1"",1];this animate [""hook_hatch_2"",1];this animate [""hook_hatch_3"",1];";
		//	};
		//		class tailHookOff {
		//		displayName = "Close Tail Hook Hatch";
		//		position = "pilot";
		//		priority = 5;
		//		radius = 20;
		//		showWindow = 0;
		//		hideOnUse = 1;
		//		onlyforplayer = 1;
		//		condition = "player in this and this animationPhase ""hook_hatch_1"" > 0.5";
		//		statement = "this animate [""hook_hatch_1"",0];this animate [""hook_hatch_2"",0];this animate [""hook_hatch_3"",0];";
		//	};
			class foldWingtips {
				displayName = "Fold wings";
				position = "pilot";
				priority = 4;
				radius = 20;
				showWindow = 0;
				hideOnUse = 1;
				onlyforplayer = 1;
				condition = "player in this and this animationPhase ""wingtipR"" < 0.5 and velocityModelSpace this select 1 < 50 and getPosATL this select 2 < 18";
				statement = "this animate [""wingtipR"",1];this animate [""wingtipR_2"",1];this animate [""wingtipR_1"",1];this animate [""wingtipR_lip"",1];this animate [""wingtipL"",1];this animate [""wingtipL_2"",1];this animate [""wingtipL_1"",1];this animate [""wingtipL_lip"",1];";
			};
			class unfoldWingtips {
				displayName = "Unfold wings";
				position = "pilot";
				priority = 4;
				radius = 20;
				showWindow = 0;
				hideOnUse = 1;
				onlyforplayer = 1;
				condition = "player in this and this animationPhase ""wingtipR"" > 0.5";
				statement = "this animate [""wingtipR"",0];this animate [""wingtipR_2"",0];this animate [""wingtipR_1"",0];this animate [""wingtipR_lip"",0];this animate [""wingtipL"",0];this animate [""wingtipL_2"",0];this animate [""wingtipL_1"",0];this animate [""wingtipL_lip"",0];";
			};
			class formationLights 
			{
				displayName = "Formation Lights On";
				position = "pilot";
				priority = 4;
				radius = 20;
				showWindow = 0;
				hideOnUse = 1;
				onlyforplayer = 1;
				condition = "player in this and this animationPhase ""FormationLights"" > 0.5";
				statement = "this animate [""FormationLights"",0];";
			};
			class unFormationLights 
			{
				displayName = "Formation Lights Off";
				position = "pilot";
				priority = 4;
				radius = 20;
				showWindow = 0;
				hideOnUse = 1;
				onlyforplayer = 1;
				condition = "player in this and this animationPhase ""FormationLights"" < 0.5";
				statement = "this animate [""FormationLights"",1];";
			};
		};
		
		class AnimationSources	/// custom made animation sources to show/hide all the different parts for different loadout
		{												/// corresponds with source used in model.cfg, hidden by default
			class bayL_hatch_1		{ AnimPeriod = 1.5; source = "user"; InitPhase = 0; };
			class bayL_hatch_1_w : bayL_hatch_1	{ InitPhase = 1; };
			class bayL_hatch_2 : bayL_hatch_1	{};
			class bayR_hatch_1 : bayL_hatch_1	{};
			class bayR_hatch_1_w : bayL_hatch_1	{ InitPhase = 1; };
			class bayR_hatch_2 : bayL_hatch_1	{};
			class bayL_hatch_1_b : bayL_hatch_1	{};
			class bayL_hatch_2_b : bayL_hatch_1	{};
			class bayR_hatch_1_b : bayL_hatch_1	{};
			class bayR_hatch_2_b : bayL_hatch_1	{};

			class wingtipR		{ AnimPeriod = 4; source = "user"; InitPhase = 0; };
			class wingtipR_1 : wingtipR	{};
			class wingtipR_2 : wingtipR	{};
			class wingtipR_lip : wingtipR	{};
			class wingtipL : wingtipR	{};
			class wingtipL_1 : wingtipR	{};
			class wingtipL_2 : wingtipR	{};
			class wingtipL_lip : wingtipR	{};

			class FormationLights { AnimPeriod = 1; source = "user"; InitPhase = 1; };

			class refuel_hatch_1	{ AnimPeriod = 4; source = "user"; InitPhase = 0; };
			class refuel_hatch_2_a : refuel_hatch_1 {};
			class refuel_hatch_2_b : refuel_hatch_1 {};
			class refuel_limb : refuel_hatch_1 {};
			class refuel_hatch_1_int : refuel_hatch_1	{};
			class refuel_hatch_2_int_a : refuel_hatch_1 {};
			class refuel_hatch_2_int_b : refuel_hatch_1 {};
			class refuel_limb_int : refuel_hatch_1 {};
			class refuel_mfd : refuel_hatch_1 {};

			class tailhook { AnimPeriod = 2; source = "user"; InitPhase = 0; };
			class hook_hatch_1 : tailhook {};
			class hook_hatch_2 : tailhook {};
			class hook_hatch_3 : tailhook {};
			
			class VTOL
			{
				source = "user";
				animPeriod = 2.5;
			};
			
			class service_menu_switch 
		 {
		 source = "user";
		 animPeriod = 0.0001;
		 initPhase = 0;
		 };
		 	class ab_switch 
		 {
		 source = "user";
		 animPeriod = 0.0001;
		 initPhase = 0;
		 };
		 
		 
		 	class switch_GPS_TGT 
		 {
		 source = "user";
		 animPeriod = 0.0001;
		 initPhase = 0;
		 };
		 	class switch_GPS_TGT_CLEAR 
		 {
		 source = "user";
		 animPeriod = 0.0001;
		 initPhase = 0;
		 };
		 class rearming_done_switch 
		 {
		 source = "user";
		 animPeriod = 0.0001;
		 initPhase = 1;
		 };
		 class paint_scheme_switch
	     {
		 source = "user";
		 animPeriod = 0.0001;
		 initPhase = 0;
	     };
		


			
			class station_01_aim9x { AnimPeriod = 0.01; source = "user"; InitPhase = 0; };
			class station_02_aim120d : station_01_aim9x {};
			class station_02_gbu32 : station_01_aim9x {};
			class station_03_aim120d : station_01_aim9x {};
			class station_03_gbu32 : station_01_aim9x {};
			class station_04_gbu53 : station_01_aim9x {};
			class station_04_gbu32 : station_01_aim9x {};
			class station_04_aim120d : station_01_aim9x {};
			class station_06_gun : station_01_aim9x {};

			class single_sidewinder : station_01_aim9x {};
			class single_amraam_int : station_01_aim9x { InitPhase = 0; };
			class single_amraam: station_01_aim9x{};
			class double_amraam : station_01_aim9x {};
			class single_jdam_int : station_01_aim9x {};
			class single_jdam : station_01_aim9x {};
			class double_jdam : station_01_aim9x {};
			class single_sdb : station_01_aim9x {};
			class ext_stations : station_01_aim9x {};
			class ext_station_R : station_01_aim9x { InitPhase = 0; };
			class ext_station_L : ext_station_R {};

			class mfd_double_amraam_1 : station_01_aim9x {};
			class mfd_double_amraam_2 : station_01_aim9x {};
			class mfd_double_amraam_int_1 : station_01_aim9x {};
			class mfd_double_amraam_int_2 : station_01_aim9x {};
			class mfd_double_gbu_1 : station_01_aim9x {};
			class mfd_double_gbu_2 : station_01_aim9x {};

			class OK_F_35C_AIM9X_revolve { source = "revolving"; weapon = "OK_F_35C_AIM9X_W"; };
			class OK_F_35C_AIM120D_revolve { source = "revolving"; weapon = "OK_F_35C_AIM120D_W"; };
			class OK_F_35C_AIM120D_revolve_4 { source = "revolving"; weapon = "OK_F_35C_4Rnd_AIM120D_W"; };
			class OK_F_35C_AIM120D_int_revolve { source = "revolving"; weapon = "OK_F_35C_AIM120D_int_W"; };
			class OK_F_35C_AIM120D_int_revolve_4 { source = "revolving"; weapon = "OK_F_35C_4Rnd_AIM120D_int_W"; };
			class OK_F_35C_GBU32_revolve { source = "revolving"; weapon = "OK_F_35C_GBU32_W"; };
			class OK_F_35C_GBU32_revolve_4 { source = "revolving"; weapon = "OK_F_35C_4Rnd_GBU32_W"; };
			class OK_F_35C_GBU32_int_revolve { source = "revolving"; weapon = "OK_F_35C_GBU32_int_W"; };
			class OK_F_35C_GBU53_int_revolve { source = "revolving"; weapon = "OK_F_35C_GBU53_int_W"; };
		};
		class RenderTargets
		{
			class mfdFlir
			{
				renderTarget = "rendertarget0";
				class CameraView1
				{
					pointPosition = "flir_pos";
					pointDirection = "flir_dir";
					renderQuality = 2;
					renderVisionMode = 2;
					fov = 0.3;
				};
			};
		};
	};
	class F_35B : F_35C_Base /// Combat Air Patrol
	{
		scope = public;	/// scope 2 means it is available in editor, this is one of the macros in basicdefines_a3.hpp
		displayName = "F35B Lightning II"; /// how does the vehicle show itself in editor
		author = "Olli Koskelainen / SuperFastBlast";

		side = 1;						/// 3 stands for civilians, 0 is OPFOR, 1 is BLUFOR, 2 means guerrillas
		faction = BLU_F;					/// defines the faction inside of the side
		crew = "B_Pilot_F";		/// lets use the sample soldier we have as default captain of the boat
		vtol = 1;
		typicalCargo[] =
		{
			"B_pilot_F"
		};
		hiddenSelectionsTextures[] = /// changes of textures to distinguish variants in same order as hiddenSelections[]
		{
		"\ST11_F35C\skins\f35c_vfa101_co.paa",
		"\ST11_F35C\data\F_35C_ext_3_co.paa"
		};
		
		weapons[] =	
		{
			"CMFlareLauncher",
			"USAF_F35A_Master_Arm_Switch",
			"USAF_F35A_CANNON_W",
		};
		magazines[] =
		{
			"240Rnd_CMFlare_Chaff_Magazine",
			"USAF_F35A_CANNON_M",
		};
		class UserActions : UserActions {};
		class AnimationSources : AnimationSources {};
		class EventHandlers
		{
			init = "_this execVM ""ST11_F35C\data\scripts\init.sqf""";
			fired="_this call BIS_Effects_EH_Fired; _this execVM ""\USAF_missilebox\scr\fall.sqf""";
		};

		availableForSupportTypes[] = { "CAS_Bombing" };	/// use any number of expressions from "Artillery", "CAS_Heli", "CAS_Bombing", "Drop", "Transport"
		cost = 3000000;	/// we need some high cost for such vehicles to be prioritized by AA defences
	};
	class PlaneWreck;

	class F_35C_wreck : PlaneWreck
	{
		scope = protected;
		model = "\ST11_F35C\F_35C_wreck";
		typicalCargo[] = {};
		irTarget = 0;
		canFloat = false;
		transportAmmo = 0;
		transportRepair = 0;
		transportFuel = 0;
		transportSoldier = 0;
		class Eventhandlers{};
	};
};