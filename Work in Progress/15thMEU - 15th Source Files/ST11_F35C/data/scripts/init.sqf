_OK_F_35C = _this select 0;

_SAFE_weapon = "w_master_arms_safe"; 
_AIM9X_weapon = "USAF_AIM9X_Launcher";
_AGM65_weapon = "USAF_AGM65_Launcher";
_AGM84_weapon = "USAF_AGM84_Launcher";
_AGM86_weapon = "USAF_AGM86C_Launcher";
_AGM88_weapon = "USAF_AGM88_Launcher";
_AIM120_weapon = "USAF_AIM120_Launcher";
_AIM120I_weapon = "USAF_AIM120I_Launcher";
_AGM65E_weapon = "USAF_AGM65E_Launcher";
_AGM154_weapon = "USAF_AGM154A1_Launcher";    
_FFARU_weapon = "USAF_FFARLauncher_unguided";
_FFARL_weapon = "USAF_FFARLauncher_Laser";
_MK82_weapon = "USAF_MK82_Launcher";
_MK84_weapon = "USAF_MK84_Launcher";
_GBU12_weapon = "USAF_GBU12_Launcher";
_GBU24_weapon = "USAF_GBU24_Launcher";
_GBU31_weapon = "USAF_GBU31_Launcher";
_GBU32_weapon = "USAF_GBU32_Launcher";
_GBU38_weapon = "USAF_GBU38_Launcher";
_GBU39_weapon = "USAF_GBU39_Launcher";
_GBU53_weapon = "USAF_F35A_GBU53_int_W";
_CBU78_weapon = "USAF_CBU78B_Launcher";
_CBU87_weapon = "USAF_CBU87B_Launcher";
_CBU89_weapon = "USAF_CBU89B_Launcher";
_MK20_weapon = "USAF_CBU100_Launcher";
_MK77_weapon = "USAF_MK77_Launcher"; 
_JAGM_weapon = "USAF_JAGM_Launcher"; 



_OK_F_35C animate["ext_stations", 1];
_OK_F_35C animate["ext_station_R", 1];
_OK_F_35C animate["ext_station_L", 1];

while {true} do
{
	_internalAmraamCount = _OK_F_35C ammo _AIM120I_weapon;
	_internalGbuCount = _OK_F_35C ammo _GBU32_weapon;
	_internalSdbCount = _OK_F_35C ammo _GBU53_weapon;

	if	(
	(((currentWeapon _OK_F_35C) == _AIM120I_weapon) and (_internalAmraamCount > 0)) ||
	(((currentWeapon _OK_F_35C) == _GBU32_weapon) and (_internalGbuCount > 0 )) ||
	(((currentWeapon _OK_F_35C) == _GBU53_weapon) and (_internalSdbCount > 0 )) 
	)
	then
	{
	_OK_F_35C animate ["bayL_hatch_1",1];
	_OK_F_35C animate ["bayL_hatch_1_w",1];
	_OK_F_35C animate ["bayL_hatch_1_b",1];
	_OK_F_35C animate ["bayR_hatch_1",1];
	_OK_F_35C animate ["bayR_hatch_1_w",1];
	_OK_F_35C animate ["bayR_hatch_1_b",1];
	
	if (
		(((currentWeapon _OK_F_35C) == _GBU32_weapon) and (_internalGbuCount > 0 )) ||
		(((currentWeapon _OK_F_35C) == _GBU53_weapon) and (_internalSdbCount > 0 )) 
		)
		then 
		{
		_OK_F_35C animate ["bayL_hatch_2",1];
		_OK_F_35C animate ["bayL_hatch_2_b",1];
		_OK_F_35C animate ["bayR_hatch_2",1];
		_OK_F_35C animate ["bayR_hatch_2_b",1];
		}
		else
		{
		_OK_F_35C animate ["bayL_hatch_2",0];
		_OK_F_35C animate ["bayL_hatch_2_b",0];
		_OK_F_35C animate ["bayR_hatch_2",0];
		_OK_F_35C animate ["bayR_hatch_2_b",0];
		};
	}
	else
	{
	_OK_F_35C animate ["bayL_hatch_1",0];
	_OK_F_35C animate ["bayL_hatch_1_w",0];
	_OK_F_35C animate ["bayL_hatch_1_b",0];
	_OK_F_35C animate ["bayR_hatch_1",0];
	_OK_F_35C animate ["bayR_hatch_1_w",0];
	_OK_F_35C animate ["bayR_hatch_1_b",0];
	_OK_F_35C animate ["bayL_hatch_2",0];
	_OK_F_35C animate ["bayL_hatch_2_b",0];
	_OK_F_35C animate ["bayR_hatch_2",0];
	_OK_F_35C animate ["bayR_hatch_2_b",0];
	};
	sleep 0.2;
};