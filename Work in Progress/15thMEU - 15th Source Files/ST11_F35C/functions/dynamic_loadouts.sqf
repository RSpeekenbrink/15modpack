/*
	Author: John_Spartan & Saul
        Edited by: Peral 
		For F35C by Genesi

	Description:
	Dynamic loadout selection/rearming function for "F35C"

	Exucution:
	veh_dynamic_loadoutscript = [_veh,_station_1,_station_2,_station_3,_station_4,_station_5,_station_6,_station_7,_station_8,_station_9,_station_10,_station_11] execvm "\path too function\dynamic_loadouts.sqf";
	via unit init line in editor or via instance of mission script [INIT.sqf for example]
	
	Parameter(s):
		_this select 0: mode (Scalar)

		0: plane/object
		1: magazine classname to be equipped on station 1 ["my_magazine_classname"]
		2: magazine classname to be equipped on station 2 ["my_magazine_classname"]
		3: magazine classname to be equipped on station 3 ["my_magazine_classname"]
		4: magazine classname to be equipped on station 4 ["my_magazine_classname"]
		5: magazine classname to be equipped on station 5 ["my_magazine_classname"]
		6: magazine classname to be equipped on station 6 ["my_magazine_classname"]
		7: magazine classname to be equipped on station 7 ["my_magazine_classname"]
		8: magazine classname to be equipped on station 8 ["my_magazine_classname"]
		9: magazine classname to be equipped on station 9 ["my_magazine_classname"]
		10: magazine classname to be equipped on station 10 ["my_magazine_classname"]
		11: magazine classname to be equipped on station 11 ["my_magazine_classname"]


	Returns: nothing
	Result: rearmed with desiered loadout

*/




//BASIC DEFINITIONS
_veh = _this select 0;		        //name of the unit we are playing with
_veh_pilot = driver _veh;		//pilot
_default_sleep_time = 1.5;		//actual time to wait/delay before loading every next magazine
_sleep_time = 1.5;			//time to wait/delay before loading every next magazine
_veh_stop_speed = 1;			//maximum speed of vehicle during the rearming 
_x = 1;					//first magazine index in array passed to this function
_w = 34;				//total number of weapon proxies on model


//WEAPON CLASSNAME DEFINITIONS
//LIST OF ALL POSSIBLE WEAPONS USED WITH THIS PLANE
//IN CASE WE NEED TO ADAPT THIS TO SOME OTHER MOD
_SAFE_weapon = "w_master_arms_safe"; 
_AIM9X_weapon = "USAF_AIM9X_Launcher";
_AGM65_weapon = "USAF_AGM65_Launcher";
_AGM84_weapon = "USAF_AGM84_Launcher";
_AGM86_weapon = "USAF_AGM86C_Launcher";
_AGM88_weapon = "USAF_AGM88_Launcher";
_AIM120_weapon = "USAF_AIM120_Launcher";
_AIM120I_weapon = "USAF_AIM120I_Launcher";
_AGM65E_weapon = "USAF_AGM65E_Launcher";
_AGM154_weapon = "USAF_AGM154A1_Launcher";    
_FFARU_weapon = "USAF_FFARLauncher_unguided";
_FFARL_weapon = "USAF_FFARLauncher_Laser";
_MK82_weapon = "USAF_MK82_Launcher";
_MK84_weapon = "USAF_MK84_Launcher";
_GBU12_weapon = "USAF_GBU12_Launcher";
_GBU24_weapon = "USAF_GBU24_Launcher";
_GBU31_weapon = "USAF_GBU31_Launcher";
_GBU32_weapon = "USAF_GBU32_Launcher";
_GBU38_weapon = "USAF_GBU38_Launcher";
_GBU39_weapon = "USAF_GBU39_Launcher";
_GBU53_weapon = "USAF_F35A_GBU53_int_W";
_CBU78_weapon = "USAF_CBU78B_Launcher";
_CBU87_weapon = "USAF_CBU87B_Launcher";
_CBU89_weapon = "USAF_CBU89B_Launcher";
_MK20_weapon = "USAF_CBU100_Launcher";
_MK77_weapon = "USAF_MK77_Launcher"; 
_JAGM_weapon = "USAF_JAGM_Launcher"; 



//MAGAZINE CLASSNAME DEFINITIONS
//LIST OF ALL POSSIBLE MAGAZINES USED WITH THIS PLANE
//IN CASE WE NEED TO ADAPT THIS TO SOME OTHER MOD
_AIM9X_magazine = "USAF_1Rnd_AIM9X";
_AIM120_magazine = "USAF_1Rnd_AIM120";
_AIM120I_magazine = "USAF_1Rnd_AIM120I";
_DUAL_RAIL_magazine = "USAF_1Rnd_DUALRAIL";
_TRIPPLE_RAIL_magazine = "USAF_1Rnd_LAU88";
_SINGLE_BOMB_magazine = "USAF_1Rnd_LAU117";
_DOUBLE_MISSILE_magazine = "USAF_1Rnd_RACKS_X2";
_TRIPPLE_MISSILE_magazine = "USAF_1Rnd_RACKS_X3";
_GBU12_magazine = "USAF_1Rnd_GBU12";
_GBU24_magazine = "USAF_1Rnd_GBU24";
_GBU38_magazine = "USAF_1Rnd_GBU38";
_GBU39_magazine = "USAF_1Rnd_GBU39";  
_GBU31_magazine = "USAF_1Rnd_GBU31";
_GBU32_magazine = "USAF_1Rnd_GBU32";
_GBU53_magazine = "USAF_F35A_8Rnd_GBU53_M";
_MK82_magazine = "USAF_1Rnd_MK82";
_MK84_magazine = "USAF_1Rnd_MK84";
_MK82_RACK_magazine = "USAF_1Rnd_MK82RACK";
_F35_Station1_magazine = "USAF_1Rnd_Station1";
_F35_Station2_magazine = "USAF_1Rnd_Station2";
_F35_StationL_magazine = "USAF_1Rnd_StationL";
_F35_StationR_magazine = "USAF_1Rnd_StationR";
_FFARU7_magazine = "USAF_7Rnd_HYDRA70_unguided";
_FFARU19_magazine = "USAF_19Rnd_HYDRA70_unguided";
_FFARL7_magazine = "USAF_7Rnd_HYDRA70_laser";
_FFARL19_magazine = "USAF_19Rnd_HYDRA70_laser";
_FFAR_POD_magazine = "USAF_1Rnd_FFARPOD7";
_FFAR_POD2_magazine = "USAF_1Rnd_FFARPOD19";
_AGM65_magazine = "USAF_1Rnd_AGM65";
_AGM65E_magazine = "USAF_1Rnd_AGM65E";
_AGM84_magazine = "USAF_1Rnd_AGM84";
_AGM86_magazine = "USAF_1Rnd_AGM86";
_AGM88_magazine = "USAF_1Rnd_AGM88";  
_AGM154_magazine = "USAF_1Rnd_AGM154A1";      
_FUEL_TANK_1 = "USAF_1Rnd_F16_FUEL";
_FUEL_TANK_2 = "USAF_1Rnd_F16_DROPTANK";
_FUEL_TANK_3 = "USAF_1Rnd_F35_TANK";
_ANALQ131 = "USAF_1Rnd_ANALQ131";  
_ANAAQ28 = "USAF_1Rnd_ANAAQ28";
_FlirPod = "USAF_1Rnd_Altflir_pod";
_BRU61A = "USAF_1Rnd_BRU61A";
_EMPTY_magazine = "USAF_empty";
_EMPTY_Fake_magazine = "USAF_Fake_empty";
_CBU78_M = "USAF_1Rnd_CBU78B";
_CBU89_M = "USAF_1Rnd_CBU89B";
_CBU87_M = "USAF_1Rnd_CBU87B";
_MK20_M = "USAF_1Rnd_CBU100";
_MK77_M = "USAF_1Rnd_MK77";
_JAGM_M = "USAF_1Rnd_JAGM";



//REARMING SWITCH, CAN BE SUBSTITUDED BY VARIABLE, USED TO PREVENT
//MULTIPLE INSTANCES OF SAME FUNCTION AT ONCE
WaitUntil {(_veh animationPhase "rearming_done_switch") == 1};



//SWITCH PILOTS WEAPON TO SAFE
_veh selectWeapon _SAFE_weapon;
_veh animate ["rearming_done_switch",0];



//CHECK FOR PLANE SPEED, IF IN MOTION WE CANCEL REARMING WITH PENALTY
if (speed _veh > _veh_stop_speed) exitWith {veh_cancel_rearm_penalty = _veh execVM "\ST11_F35C\functions\empty.sqf";if (player == _veh_pilot) then {titleText ["Rearming canceled...", "PLAIN DOWN",0.3];};};
if (player == _veh_pilot) then {titleText ["Rearming...", "PLAIN DOWN",0.3];};



//ROMEVE ALL POSSIBLE DEFAULT WEAPONS
_veh removeWeapon _AIM9X_weapon;
_veh removeWeapon _AIM120_weapon;
_veh removeWeapon _AIM120I_weapon;  
_veh removeWeapon _AGM65_weapon;
_veh removeWeapon _AGM84_weapon;
_veh removeWeapon _AGM86_weapon;
_veh removeWeapon _AGM88_weapon;
_veh removeWeapon _AGM65E_weapon;
_veh removeWeapon _FFARU_weapon;  
_veh removeWeapon _FFARL_weapon;
_veh removeWeapon _MK82_weapon;  
_veh removeWeapon _MK84_weapon;
_veh removeWeapon _GBU12_weapon;
_veh removeWeapon _GBU24_weapon;
_veh removeWeapon _GBU31_weapon;
_veh removeWeapon _GBU32_weapon;
_veh removeWeapon _GBU38_weapon;
_veh removeWeapon _GBU39_weapon;
_veh removeWeapon _GBU53_weapon;
_veh removeWeapon _AGM154_weapon;
_veh removeWeapon _CBU78_weapon;
_veh removeWeapon _CBU87_weapon;
_veh removeWeapon _CBU89_weapon;
_veh removeWeapon _MK20_weapon;
_veh removeWeapon _MK77_weapon;
_veh removeWeapon _JAGM_weapon;


//ROMEVE ALL POSSIBLE DEFAULT MAGAZINES
_veh removeMagazines _AIM9X_magazine;
_veh removeMagazines _AIM120_magazine;
_veh removeMagazines _AIM120I_magazine;
_veh removeMagazines _SINGLE_BOMB_magazine;
_veh removeMagazines _DUAL_RAIL_magazine;
_veh removeMagazines _TRIPPLE_RAIL_magazine;
_veh removeMagazines _DOUBLE_MISSILE_magazine;
_veh removeMagazines _TRIPPLE_MISSILE_magazine;
_veh removeMagazines _MK82_RACK_magazine;
_veh removeMagazines _F35_Station1_magazine;
_veh removeMagazines _F35_Station2_magazine;
_veh removeMagazines _F35_StationL_magazine;
_veh removeMagazines _F35_StationR_magazine;
_veh removeMagazines _GBU12_magazine;
_veh removeMagazines _GBU24_magazine;
_veh removeMagazines _GBU31_magazine;
_veh removeMagazines _GBU32_magazine;
_veh removeMagazines _GBU38_magazine;
_veh removeMagazines _GBU39_magazine;
_veh removeMagazines _MK82_magazine;
_veh removeMagazines _MK84_magazine;
_veh removeMagazines _GBU31_magazine;
_veh removeMagazines _GBU53_magazine;
_veh removeMagazines _AGM65_magazine;
_veh removeMagazines _AGM65E_magazine;
_veh removeMagazines _AGM84_magazine;
_veh removeMagazines _AGM86_magazine;
_veh removeMagazines _AGM88_magazine;
_veh removeMagazines _AGM154_magazine;
_veh removeMagazines _FFARU7_magazine;
_veh removeMagazines _FFARU19_magazine;
_veh removeMagazines _FFARL7_magazine;
_veh removeMagazines _FFARL19_magazine;
_veh removeMagazines _FFAR_POD_magazine;
_veh removeMagazines _FFAR_POD2_magazine;
_veh removeMagazines _FUEL_TANK_1;
_veh removeMagazines _FUEL_TANK_2;
_veh removeMagazines _FUEL_TANK_3;
_veh removeMagazines _ANALQ131;
_veh removeMagazines _ANAAQ28;
_veh removeMagazines _FlirPod;
_veh removeMagazines _BRU61A;
_veh removeMagazines _EMPTY_magazine;
_veh removeMagazines _EMPTY_Fake_magazine;
_veh removeMagazines _CBU78_M;
_veh removeMagazines _CBU89_M;
_veh removeMagazines _CBU87_M;
_veh removeMagazines _MK20_M;
_veh removeMagazines _MK77_M;
_veh removeMagazines _JAGM_M;


//ADDING NEW MAGAZINES TO PLANE 
//ONLY STATIONS 1-11 ARE USED FOR CONVENTIONAL LOADOUTS
//replace "path to function" with actual path and F18
for "_i" from 1 to 34 do //or however many slots you have
{
	 
	_veh removeMagazines _EMPTY_FAKE_magazine; 
	_new_magazine = _this select _x;
	_veh addMagazine _new_magazine;
	for "_y" from 1 to _w do {_veh addMagazine _EMPTY_FAKE_magazine;};
	if (_new_magazine == _EMPTY_magazine) then {_sleep_time = 0;} else {_sleep_time = _default_sleep_time;}; 
	if (speed _veh > _veh_stop_speed) exitWith {veh_cancel_rearm_penalty = _veh execVM "\ST11_F35C\functions\empty.sqf";if (player == _veh_pilot) then {titleText ["Rearming canceled...", "PLAIN DOWN",0.3];};};
	if (player == _veh_pilot) then {titleText ["Rearming...", "PLAIN DOWN",0.3];};
	_x= _x +1;
	_w= _w -1;
	sleep _sleep_time;
	//hintsilent format["Station Number: %1\nNew magazine: %2\nMax proxies: %3\nSleep time: %4\n",_station_number,_veh_new_magazine_station,_max_proxies,_sleep_time];

};






//ADD NEW WEAPONS FOR PRELOADED MAGAZINES
_loadout = magazines _veh;
sleep 0.5;
sleep 0.5;
if ((_AIM9X_magazine in _loadout)) then {_veh addWeapon _AIM9X_weapon;};
if ((_AIM120_magazine in _loadout)) then {_veh addWeapon _AIM120_weapon;};
if ((_AIM120I_magazine in _loadout)) then {_veh addWeapon _AIM120I_weapon;};
if ((_FFARU7_magazine in _loadout)) then {_veh addWeapon _FFARU_weapon;};
if ((_FFARU19_magazine in _loadout)) then {_veh addWeapon _FFARU_weapon;};
if ((_FFARL7_magazine in _loadout)) then {_veh addWeapon _FFARL_weapon;};
if ((_FFARL19_magazine in _loadout)) then {_veh addWeapon _FFARL_weapon;};
if ((_FFARU19_magazine in _loadout)) then {_veh addWeapon _FFARL_weapon;};
if ((_FFARU19_magazine in _loadout)) then {_veh addWeapon _FFARU_weapon;};
if ((_MK82_magazine in _loadout)) then {_veh addWeapon _MK82_weapon;};
if ((_MK84_magazine in _loadout)) then {_veh addWeapon _MK84_weapon;};
if ((_GBU12_magazine in _loadout)) then {_veh addWeapon _GBU12_weapon;};
if ((_GBU24_magazine in _loadout)) then {_veh addWeapon _GBU24_weapon;};
if ((_GBU31_magazine in _loadout)) then {_veh addWeapon _GBU31_weapon;};
if ((_GBU32_magazine in _loadout)) then {_veh addWeapon _GBU32_weapon;};
if ((_GBU38_magazine in _loadout)) then {_veh addWeapon _GBU38_weapon;};
if ((_GBU39_magazine in _loadout)) then {_veh addWeapon _GBU39_weapon;};
if ((_GBU53_magazine in _loadout)) then {_veh addWeapon _GBU53_weapon;};
if ((_AGM65_magazine in _loadout)) then {_veh addWeapon _AGM65_weapon;};
if ((_AGM65E_magazine in _loadout)) then {_veh addWeapon _AGM65E_weapon;};
if ((_AGM84_magazine in _loadout)) then {_veh addWeapon _AGM84_weapon;};
if ((_AGM86_magazine in _loadout)) then {_veh addWeapon _AGM86_weapon;};
if ((_AGM88_magazine in _loadout)) then {_veh addWeapon _AGM88_weapon;};
if ((_AGM154_magazine in _loadout)) then {_veh addWeapon _AGM154_weapon;};
if ((_CBU78_M in _loadout)) then {_veh addWeapon _CBU78_weapon;};
if ((_CBU89_M in _loadout)) then {_veh addWeapon _CBU89_weapon;};
if ((_CBU87_M in _loadout)) then {_veh addWeapon _CBU87_weapon;};
if ((_MK20_M in _loadout)) then {_veh addWeapon _MK20_weapon;};
if ((_MK77_M in _loadout)) then {_veh addWeapon _MK77_weapon;};
if ((_JAGM_M in _loadout)) then {_veh addWeapon _JAGM_weapon;};


//CHECK FOR PLANE SPEED, IF IN MOTION WE CANCEL REARMING WITH PENALTY
if (speed _veh > _veh_stop_speed) exitWith {veh_cancel_rearm_penalty = _veh execVM "\ST11_F35C\functions\empty.sqf";if (player == _veh_pilot) then {titleText ["Rearming canceled...", "PLAIN DOWN",0.3];};};
if (player == _veh_pilot) then {titleText ["Rearming...", "PLAIN DOWN",0.3];};



//FINALIZING LOADOUT 
_veh setVehicleAmmo 1;
_veh selectWeapon _SAFE_weapon;
_veh animate ["rearming_done_switch",1];
if (player == _veh_pilot) then {titleText ["Rearming complete...", "PLAIN DOWN",0.6];};
