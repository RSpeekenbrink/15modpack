If (!Local Player) ExitWith {};  
Private "_veh";
_veh = vehicle player;

closeDialog 0;

sleep 0.01;
_veh_current_skin = (_veh animationphase "paint_scheme_switch");
CreateDialog "SKINS_UI";

sleep 0.01;

disableSerialization;
if (_veh animationphase "paint_scheme_switch" <= 0.1) then 
{
	_ui = uiNamespace getVariable "skins_Display";
	(_ui displayCtrl 2501) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_on.paa";
	(_ui displayCtrl 2502) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2503) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2504) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
        (_ui displayCtrl 2505) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2506) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2507) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2508) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
};

if ((_veh animationphase "paint_scheme_switch" < 0.26) and (_veh animationphase "paint_scheme_switch" > 0.2)) then 
{
	_ui = uiNamespace getVariable "skins_Display";
	(_ui displayCtrl 2501) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2502) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2503) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_on.paa";
	(_ui displayCtrl 2504) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
        (_ui displayCtrl 2505) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2506) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2507) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2508) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
};

if ((_veh animationphase "paint_scheme_switch" < 0.56) and (_veh animationphase "paint_scheme_switch" > 0.45)) then 
{
	_ui = uiNamespace getVariable "skins_Display";
	(_ui displayCtrl 2501) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2502) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_on.paa";
	(_ui displayCtrl 2503) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2504) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
        (_ui displayCtrl 2505) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2506) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2507) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2508) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
};

if ((_veh animationphase "paint_scheme_switch" == 1)) then 
{
	_ui = uiNamespace getVariable "skins_Display";
	(_ui displayCtrl 2501) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2502) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2503) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2504) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_on.paa";
        (_ui displayCtrl 2505) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2506) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2507) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
	(_ui displayCtrl 2508) ctrlSetText "\ST11_F35C\UI\GUI_button_tickbox_off.paa";
};

WaitUntil {!Dialog};
_veh animate ["service_menu_switch",0];