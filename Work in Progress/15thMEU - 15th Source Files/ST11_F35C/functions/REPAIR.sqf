	_veh = vehicle player;
	_veh_pilot = driver _veh;
	_default_sleep_time = 3;
	_sleep_time = 3;		
	_veh_stop_speed = 1;
	_damage_veh = damage _veh;
  	If (!Local Player) ExitWith {};

	if (speed _veh > _veh_stop_speed) exitWith {if ((player == _veh_pilot)) then {titleText ["Repairing canceled...", "PLAIN DOWN",0.3];};};
	if ((player == _veh_pilot)) then {titleText ["Repairing...", "PLAIN DOWN",0.3];};

	while {alive _veh} do 
	{
	
		_damage_veh = damage _veh;
		_veh Setdamage (_damage_veh - 0.015);	
		If ((_damage_veh < 0.01) and (player == _veh_pilot)) exitWith {titleText ["Repairing complete...", "PLAIN DOWN",0.6];};
		if (speed _veh > _veh_stop_speed) exitWith {if ((player == _veh_pilot)) then {titleText ["Repairing canceled...", "PLAIN DOWN",0.3];};};
		if ((player == _veh_pilot) or (player == _veh_WSO)) then {titleText ["Repairing...", "PLAIN DOWN",0.3];};
		sleep _sleep_time;
	};