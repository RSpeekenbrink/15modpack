Private ["_USAF_F35A"];
_USAF_F35A = vehicle player;
_USAF_F35A_PILOT = driver _USAF_F35A;
_USAF_F35A_WSO = gunner _USAF_F35A;
If (!Local _USAF_F35A) ExitWith {};

_USAF_F35A animate ["switch_GPS_TGT_CLEAR",1];
onMapSingleClick "";

sleep 1;

_USAF_F35A animate ["switch_GPS_TGT_CLEAR",0];
sleep 0.01;
if ((player == _USAF_F35A_PILOT) or (player == _USAF_F35A_WSO)) then {titleText ["\n \n GPS/INS target is CLEARED, \n set the new GPS/INS target cordinates...", "PLAIN DOWN",1];};

Exit;