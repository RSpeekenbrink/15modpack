	If (!Local Player) ExitWith {};  
	Private ["_veh"];
	_veh = vehicle player;

	closeDialog 0;

	sleep 0.01;

	CreateDialog "LOADOUTS_UI";

        {lbAdd[3101,_x]} forEach [ 
		"EMPTY", 
             	"AIM-9X ",
             	"AIM-120D"
           ];

           {lbAdd[3102,_x]} forEach [
              	"EMPTY", 
             	"AIM-9X",
             	"AIM-120D"
            ];

           {lbAdd[3103,_x]} forEach [
              	"EMPTY", 
             	"MK-82 (X2)",
             	"GBU-32 (X2)",
				"AGM-154",
				"GBU-31",
				"GBU-12",
				"AGM-88",
				"CBU-78 (X2)",
				"CBU-87 (X2)",
				"CBU-89 (X2)"
            ];

           {lbAdd[3104,_x]} forEach [
              	"EMPTY", 
             	"MK-82 (X2)",
             	"GBU-32 (X2)",
				"AGM-154",
				"GBU-31",
				"GBU-12",
				"AGM-88",
				"CBU-78 (X2)",
				"CBU-87 (X2)",
				"CBU-89 (X2)"
            ];

           {lbAdd[3105,_x]} forEach [
              	"EMPTY", 
             	"Fuel Pod",
				"AGM-154",
				"GBU-12",
             	"GBU-31",
				"CBU-78 (X2)",
				"CBU-87 (X2)",
				"CBU-89 (X2)"
            ];

           {lbAdd[3106,_x]} forEach [
              "EMPTY", 
             	"Fuel Pod",
             	"AGM-154",
				"GBU-12",
				"GBU-31",
				"CBU-78 (X2)",
				"CBU-87 (X2)",
				"CBU-89 (X2)"
            ];
			{lbAdd[3107,_x]} forEach [
              "EMPTY", 
             	"AIM-120D"
            ];
			{lbAdd[3108,_x]} forEach [
              "EMPTY", 
             	"AIM-120D"
            ];
			{lbAdd[3109,_x]} forEach [
              "EMPTY", 
             	"GBU-31 (X2)",
				"GBU-32 (X2)",
				"GBU-38 (X2)",
				"GBU-24 (x2)",
				"GBU-53 (x8)",
				"AGM-154 (x2)",
				"AIM-120 (x4)",
				"CBU-100 (x2)",
				"JAGM (Joint Air to Ground Missile (X4))"
				
				
            ];

          

         	((uiNamespace getVariable "myDisplay") displayCtrl 3101) lbSetCurSel 1;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3102) lbSetCurSel 1;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3103) lbSetCurSel 2;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3104) lbSetCurSel 2;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3105) lbSetCurSel 11;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3106) lbSetCurSel 11;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3107) lbSetCurSel 8;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3108) lbSetCurSel 8;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3109) lbSetCurSel 0;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3110) lbSetCurSel 2;
         	((uiNamespace getVariable "myDisplay") displayCtrl 3111) lbSetCurSel 1;
		

      
    
	WaitUntil {!Dialog};
 
	_veh animate ["service_menu_switch",0];