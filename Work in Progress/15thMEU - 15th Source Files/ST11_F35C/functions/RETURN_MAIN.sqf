If (!Local Player) ExitWith {};  
Private "_veh";
_veh = vehicle player;

closeDialog 0;

sleep 0.01;
if 
(
((_veh distance (nearestObject [_veh, "B_Truck_01_ammo_F"]) < 25) and (damage (nearestObject [_veh, "B_Truck_01_ammo_F"]) < 1) and (player == driver _veh) and (_veh animationPhase "service_menu_switch" < 0.5) and (speed _veh < 1))
or
((_veh distance (nearestObject [_veh, "Land_Hangar_F"]) < 55) and (damage (nearestObject [_veh, "Land_Hangar_F"]) < 1) and (player == driver _veh) and (_veh animationPhase "service_menu_switch" < 0.5) and (speed _veh < 1))
or
((_veh distance (nearestObject [_veh, "Land_TentHangar_V1_F"]) < 55) and (damage (nearestObject [_veh, "Land_TentHangar_V1_F"]) < 1) and (player == driver _veh) and (_veh animationPhase "service_menu_switch" < 0.5) and (speed _veh < 1))
or
((_veh distance (nearestObject [_veh, "JDG_carrier_Spawner"]) < 555) and (damage (nearestObject [_veh, "JDG_carrier_Spawner"]) < 1) and (player == driver _veh) and (_veh animationPhase "service_menu_switch" < 0.5) and (speed _veh < 1))
) 

then {CreateDialog "MAIN_UI";} else {CreateDialog "MAIN_RESTRICTED_UI";};

