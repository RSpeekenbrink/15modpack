	_veh = vehicle player;
	_veh_pilot = driver _veh;
	_default_sleep_time = 3;
	_i = 0;
	_sleep_time = 3;		
	_veh_stop_speed = 1;
  	If (!Local Player) ExitWith {};


	while {alive _veh} do 
	{
		_i=_i +1;
		if ((_i>=19)) exitWith {_veh setVehicleAmmo 1; if ((player == _veh_pilot)) then {titleText ["Rearming complete...", "PLAIN DOWN",0.3];};};
		if (speed _veh > _veh_stop_speed) exitWith {if ((player == _veh_pilot)) then {titleText ["Rearming canceled...", "PLAIN DOWN",0.3];};};
		sleep _sleep_time;
		if ((player == _veh_pilot)) then {titleText ["Rearming...", "PLAIN DOWN",0.3];};
		
	};