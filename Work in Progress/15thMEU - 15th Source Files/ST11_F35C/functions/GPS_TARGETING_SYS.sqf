Private ["_USAF_F35A"];
_USAF_F35A = vehicle player;
_USAF_F35A_PILOT = driver _USAF_F35A;
_USAF_F35A_WSO = gunner _USAF_F35A;
If (!Local _USAF_F35A) ExitWith {};

If (_USAF_F35A animationphase "switch_GPS_TGT" > 0.01) exitWith {if ((player == _USAF_F35A_PILOT) or (player == _USAF_F35A_WSO)) then {titleText ["\n \n GPS/INS target is SET/ACTIVE, \n clear the previuos taget first...", "PLAIN DOWN",1];};};
sleep 0.01;
_USAF_F35A animate ["switch_GPS_TGT",1];

target_marker = createMarkerLocal ["GPS_marker",[0,0]]; 
target_marker setMarkerColorLocal "colorBlack";
target_marker setMarkerShapeLocal "icon";
target_marker setMarkerSizeLocal [1,1];
target_marker setMarkerTypeLocal "mil_destroy";
target_marker setMarkerTextLocal "GPS/INS TGT POS";
target_marker setMarkerAlphaLocal 0;

mapclick = false;
onMapSingleClick "target_marker setMarkerPosLocal _pos; clickpos = _pos; mapclick = true; target_marker setMarkerAlphaLocal 1;";

waituntil {mapclick};
_target_pos = clickpos;
_target = "USAF_F35A_GPSTarget" createVehicleLocal _target_pos;

sleep 0.5;
_target setPos _target_pos;
onMapSingleClick "";

disableSerialization;
_ui = uiNamespace getVariable "GPS_TGT_Display"; 
(_ui displayCtrl 5004) ctrlSetText "GPS TGT SET/ACTIVE";

while {(_USAF_F35A animationphase "switch_GPS_TGT_CLEAR" < 0.01) and (alive _USAF_F35A)} do 
{

	if ((currentweapon _USAF_F35A == "USAF_F35A_GBU53_int_W") or (currentweapon _USAF_F35A == "USAF_GBU31_Launcher") or (currentweapon _USAF_F35A == "USAF_GBU32_Launcher") or (currentweapon _USAF_F35A == "USAF_GBU24_Launcher") or (currentweapon _USAF_F35A == "USAF_AGM154A1_Launcher") or (currentweapon _USAF_F35A == "USAF_GBU12_Launcher")) then {_target hideObject false} else {_target hideObject true};
	
	sleep 0.1;
};

deleteVehicle _target;
deleteMarkerLocal target_marker;
_USAF_F35A animate ["switch_GPS_TGT",0];
(_ui displayCtrl 5004) ctrlSetText "GPS TGT NOT SET/ACTIVE";
Exit;














