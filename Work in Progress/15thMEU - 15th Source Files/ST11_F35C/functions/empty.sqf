/*
	Author: John_Spartan & Saul

	Description:
	Empty loadout function for FA18X Black Wasp
	Addapted for use with USNW Pack with persmissions from author's

	Exucution:
	_plane execVM "\USAF_F35A\scripts\LOADOUTS\USAF_F35A_empty_loadout.sqf";
	via unit init line in editor or via instance of mission script [INIT.sqf for example]
	
	Parameter(s):
		NONE


	Returns: nothing
	Result: aircrfat stripped of all weapons/magazines

*/



//BASIC DEFINITIONS
_plane = _this;				//name of the unit we are playing with



//WEAPON CLASSNAME DEFINITIONS
//LIST OF ALL POSSIBLE WEAPONS USED WITH THIS PLANE
//IN CASE WE NEED TO ADAPT THIS TO SOME OTHER MOD
_SAFE_weapon = "Fuller_master_arms_safe";
_AIM9X_weapon = "USAF_AIM9X_Launcher";
_AGM65_weapon = "USAF_AGM65_Launcher";
_AGM84_weapon = "USAF_AGM84_Launcher";
_AGM86_weapon = "USAF_AGM86C_Launcher";
_AGM88_weapon = "USAF_AGM88_Launcher";
_AIM120_weapon = "USAF_AIM120_Launcher";
_AIM120I_weapon = "USAF_AIM120I_Launcher";
_AGM65E_weapon = "USAF_AGM65E_Launcher";
_AGM154_weapon = "USAF_AGM154A1_Launcher";    
_FFARU_weapon = "USAF_FFARLauncher_unguided";
_FFARL_weapon = "USAF_FFARLauncher_Laser";
_MK82_weapon = "USAF_MK82_Launcher";
_MK84_weapon = "USAF_MK84_Launcher";
_GBU12_weapon = "USAF_GBU12_Launcher";
_GBU24_weapon = "USAF_GBU24_Launcher";
_GBU31_weapon = "USAF_GBU31_Launcher";
_GBU32_weapon = "USAF_GBU32_Launcher";
_GBU38_weapon = "USAF_GBU38_Launcher";
_GBU39_weapon = "USAF_GBU39_Launcher";
_GBU53_weapon = "USAF_F35A_GBU53_int_W";
_CBU78_weapon = "USAF_CBU78B_Launcher";
_CBU87_weapon = "USAF_CBU87B_Launcher";
_CBU89_weapon = "USAF_CBU89B_Launcher";
_MK20_weapon = "USAF_CBU100_Launcher";
_MK77_weapon = "USAF_MK77_Launcher"; 
_JAGM_weapon = "USAF_JAGM_Launcher"; 
 
 
 
 
//MAGAZINE CLASSNAME DEFINITIONS
//LIST OF ALL POSSIBLE MAGAZINES USED WITH THIS PLANE
//IN CASE WE NEED TO ADAPT THIS TO SOME OTHER MOD
_AIM9X_magazine = "USAF_1Rnd_AIM9X";
_AIM120_magazine = "USAF_1Rnd_AIM120";
_AIM120I_magazine = "USAF_1Rnd_AIM120I";
_DUAL_RAIL_magazine = "USAF_1Rnd_DUALRAIL";
_TRIPPLE_RAIL_magazine = "USAF_1Rnd_LAU88";
_SINGLE_BOMB_magazine = "USAF_1Rnd_LAU117";
_DOUBLE_MISSILE_magazine = "USAF_1Rnd_RACKS_X2";
_TRIPPLE_MISSILE_magazine = "USAF_1Rnd_RACKS_X3";
_GBU12_magazine = "USAF_1Rnd_GBU12";
_GBU24_magazine = "USAF_1Rnd_GBU24";
_GBU38_magazine = "USAF_1Rnd_GBU38";
_GBU39_magazine = "USAF_1Rnd_GBU39";  
_GBU31_magazine = "USAF_1Rnd_GBU31";
_GBU32_magazine = "USAF_1Rnd_GBU32";
_GBU53_magazine = "USAF_F35A_8Rnd_GBU53_M";
_MK82_magazine = "USAF_1Rnd_MK82";
_MK84_magazine = "USAF_1Rnd_MK84";
_MK82_RACK_magazine = "USAF_1Rnd_MK82RACK";
_F35_Station1_magazine = "USAF_1Rnd_Station1";
_F35_Station2_magazine = "USAF_1Rnd_Station2";
_F35_StationL_magazine = "USAF_1Rnd_StationL";
_F35_StationR_magazine = "USAF_1Rnd_StationR";
_FFARU7_magazine = "USAF_7Rnd_HYDRA70_unguided";
_FFARU19_magazine = "USAF_19Rnd_HYDRA70_unguided";
_FFARL7_magazine = "USAF_7Rnd_HYDRA70_laser";
_FFARL19_magazine = "USAF_19Rnd_HYDRA70_laser";
_FFAR_POD_magazine = "USAF_1Rnd_FFARPOD7";
_FFAR_POD2_magazine = "USAF_1Rnd_FFARPOD19";
_AGM65_magazine = "USAF_1Rnd_AGM65";
_AGM65E_magazine = "USAF_1Rnd_AGM65E";
_AGM84_magazine = "USAF_1Rnd_AGM84";
_AGM86_magazine = "USAF_1Rnd_AGM86";
_AGM88_magazine = "USAF_1Rnd_AGM88";  
_AGM154_magazine = "USAF_1Rnd_AGM154A1";      
_FUEL_TANK_1 = "USAF_1Rnd_F16_FUEL";
_FUEL_TANK_2 = "USAF_1Rnd_F16_DROPTANK";
_FUEL_TANK_3 = "USAF_1Rnd_F35_TANK";
_ANALQ131 = "USAF_1Rnd_ANALQ131";  
_ANAAQ28 = "USAF_1Rnd_ANAAQ28";
_FlirPod = "USAF_1Rnd_Altflir_pod";
_BRU61A = "USAF_1Rnd_BRU61A";
_EMPTY_magazine = "USAF_empty";
_EMPTY_Fake_magazine = "USAF_Fake_empty";
_CBU78_M = "USAF_1Rnd_CBU78B";
_CBU89_M = "USAF_1Rnd_CBU89B";
_CBU87_M = "USAF_1Rnd_CBU87B";
_MK20_M = "USAF_1Rnd_CBU100";
_MK77_M = "USAF_1Rnd_MK77";
_JAGM_M = "USAF_1Rnd_JAGM";
  



//SWITCH PILOTS WEAPON TO SAFE
_plane selectWeapon _SAFE_weapon;
_plane setVariable ["Rearming_done_switch", 0];



//ROMEVE ALL POSSIBLE DEFAULT WEAPONS
_Plane removeWeapon _AIM9X_weapon;
_Plane removeWeapon _AIM120_weapon;
_Plane removeWeapon _AIM120I_weapon;  
_Plane removeWeapon _AGM65_weapon;
_Plane removeWeapon _AGM84_weapon;
_Plane removeWeapon _AGM86_weapon;
_Plane removeWeapon _AGM88_weapon;
_Plane removeWeapon _AGM65E_weapon;
_Plane removeWeapon _FFARU_weapon;  
_Plane removeWeapon _FFARL_weapon;
_Plane removeWeapon _MK82_weapon;  
_Plane removeWeapon _MK84_weapon;
_Plane removeWeapon _GBU12_weapon;
_Plane removeWeapon _GBU24_weapon;
_Plane removeWeapon _GBU31_weapon;
_Plane removeWeapon _GBU32_weapon;
_Plane removeWeapon _GBU38_weapon;
_Plane removeWeapon _GBU39_weapon;
_Plane removeWeapon _GBU53_weapon;
_Plane removeWeapon _AGM154_weapon;
_Plane removeWeapon _CBU78_weapon;
_Plane removeWeapon _CBU87_weapon;
_Plane removeWeapon _CBU89_weapon;
_Plane removeWeapon _MK20_weapon;
_Plane removeWeapon _MK77_weapon;
_Plane removeWeapon _JAGM_weapon;
 
 
 
 
//ROMEVE ALL POSSIBLE DEFAULT MAGAZINES
_Plane removeMagazines _AIM9X_magazine;
_Plane removeMagazines _AIM120_magazine;
_Plane removeMagazines _AIM120I_magazine;
_Plane removeMagazines _SINGLE_BOMB_magazine;
_Plane removeMagazines _DUAL_RAIL_magazine;
_Plane removeMagazines _TRIPPLE_RAIL_magazine;
_Plane removeMagazines _DOUBLE_MISSILE_magazine;
_Plane removeMagazines _TRIPPLE_MISSILE_magazine;
_Plane removeMagazines _MK82_RACK_magazine;
_Plane removeMagazines _F35_Station1_magazine;
_Plane removeMagazines _F35_Station2_magazine;
_Plane removeMagazines _F35_StationL_magazine;
_Plane removeMagazines _F35_StationR_magazine;
_Plane removeMagazines _GBU12_magazine;
_Plane removeMagazines _GBU24_magazine;
_Plane removeMagazines _GBU31_magazine;
_Plane removeMagazines _GBU32_magazine;
_Plane removeMagazines _GBU38_magazine;
_Plane removeMagazines _GBU39_magazine;
_Plane removeMagazines _MK82_magazine;
_Plane removeMagazines _MK84_magazine;
_Plane removeMagazines _GBU31_magazine;
_Plane removeMagazines _GBU53_magazine;
_Plane removeMagazines _AGM65_magazine;
_Plane removeMagazines _AGM65E_magazine;
_Plane removeMagazines _AGM84_magazine;
_Plane removeMagazines _AGM86_magazine;
_Plane removeMagazines _AGM88_magazine;
_Plane removeMagazines _AGM154_magazine;
_Plane removeMagazines _FFARU7_magazine;
_Plane removeMagazines _FFARU19_magazine;
_Plane removeMagazines _FFARL7_magazine;
_Plane removeMagazines _FFARL19_magazine;
_Plane removeMagazines _FFAR_POD_magazine;
_Plane removeMagazines _FFAR_POD2_magazine;
_Plane removeMagazines _FUEL_TANK_1;
_Plane removeMagazines _FUEL_TANK_2;
_Plane removeMagazines _FUEL_TANK_3;
_Plane removeMagazines _ANALQ131;
_Plane removeMagazines _ANAAQ28;
_Plane removeMagazines _FlirPod;
_Plane removeMagazines _BRU61A;
_Plane removeMagazines _EMPTY_magazine;
_Plane removeMagazines _EMPTY_Fake_magazine;
_Plane removeMagazines _CBU78_M;
_Plane removeMagazines _CBU89_M;
_Plane removeMagazines _CBU87_M;
_Plane removeMagazines _MK20_M;
_Plane removeMagazines _MK77_M;
_Plane removeMagazines _JAGM_M;

//ANIMATE ALL HIDDEN SELECTIONS TO SUPPORT NEW LOADOUT
_Plane animate ["pylon_1_hide",1];
_Plane animate ["pylon_2_hide",1];
_Plane animate ["pylon_3_hide",1];
_Plane animate ["pylon_4_hide",1];
_Plane animate ["pylon_5_hide",1];
_Plane animate ["pylon_6_hide",1];






//FINALIZING FUNCTION 
_plane setVehicleAmmo 1;
_plane selectWeapon _SAFE_weapon;
_plane setVariable ["Rearming_done_switch", 1,false];

