
class CfgPatches
{
	class 15thmeu_rhs
	{
		units[] = {};
		weapons[] = {"rhsusf_acc_ACOG6","rhsusf_mich_bare_norotos_tan_w","rhs_Booniehat2_marpatw","rhsusf_mich_bare_norotos_alt_tan_w","rhsusf_ech_helmet_marpatd_headset","rhsusf_ech_helmet_marpatw_headset","rhsusf_ech_helmet_marpatw","rhsusf_ech_helmet_marpatd","meu_marpat_w","rhsusf_ech_helmet_marpatwd_headset","rhsusf_ech_helmet_marpatwd","rhsusf_ech_helmet_headset_marpatwd","rhsusf_ech_helmet_headset_marpatw","rhsusf_ech_helmet_headset_ess_marpatwd","rhsusf_ech_helmet_headset_ess_marpatd","rhsusf_ech_helmet_headset_ess_marpatw","rhsusf_ech_helmet_headset_marpatd"};
		magazine[] = {};
		ammo[] = {};
		author[] = {"15thMEU"};
		requiredVersion = 4.9;
		requiredAddons[] = {"A3_Weapons_F"};
		version = 4.9;
        versionStr = "4.9";
        versionAr[] = {4.9};
        versionDesc = "15th MEU RHS FIXES";
        versionAct = "";
	};
};
class CfgSettings 
{
   class CBA 
   {
      class Versioning 
      {
         class 15thmeu_rhs {};
      };
   };
};
class CfgVehicles  //Eagle 3 Space Buff and Winter MARPART Uniform
{
	class B_AssaultPack_Base;
	class rhsusf_infantry_usmc_base;
	class rhsusf_infantry_usmc_recon_base;
	class rhsusf_usmc_recon_marpat_wd_rifleman;
	class rhsusf_usmc_recon_marpat_d_rifleman;
	class rhsusf_assault_eagleaiii_ucp: B_AssaultPack_Base
	{
		maximumLoad = 300;
		mass = 20;
	};
	class meu_usmc_winter: rhsusf_usmc_recon_marpat_d_rifleman
	{
		side = 1;
		scope = 2;
		author = "RHS/15thMEU";
		vehicleClass = "meu_vehicleclass";
		uniformClass = "meu_marpat_w";
		displayName = "Rifleman";
		hiddenSelections[] = {"Camo"};
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\meu_frog02_w_co.paa"};
	};
};
class CfgWeapons
{
	class UGL_F;
	class Rifle_Base_F;
	class rhs_weap_m4_m203;
	class InventoryOpticsItem_Base_F;
	class ItemCore;
	class SlotInfo;
	class CowsSlot;
	class muzzle_snds_H;
	class InventoryMuzzleItem_Base_F;
	class MuzzleSlot;
	class rhs_uniform_FROG01_d;
	class UniformItem;
	class Uniform_Base;
	class Launcher_Base_F;
	class arifle_MX_Base_F: Rifle_Base_F{};
	class rhs_weap_m4_Base: arifle_MX_Base_F //Adds Hunt IR to M203
	{
		class M203_GL: UGL_F
		{
			magazines[] = {"rhs_mag_M441_HE","rhs_mag_M433_HEDP","rhs_mag_M4009","rhs_mag_m576","rhs_mag_M585_white","rhs_mag_M661_green","rhs_mag_M662_red","rhs_mag_M713_red","rhs_mag_M714_white","rhs_mag_M715_green","rhs_mag_M716_yellow","rhs_mag_M781_Practice","1Rnd_HE_Grenade_shell","UGL_FlareWhite_F","UGL_FlareGreen_F","UGL_FlareRed_F","UGL_FlareYellow_F","UGL_FlareCIR_F","1Rnd_Smoke_Grenade_shell","1Rnd_SmokeRed_Grenade_shell","1Rnd_SmokeGreen_Grenade_shell","1Rnd_SmokeYellow_Grenade_shell","1Rnd_SmokePurple_Grenade_shell","1Rnd_SmokeBlue_Grenade_shell","1Rnd_SmokeOrange_Grenade_shell","3Rnd_HE_Grenade_shell","3Rnd_UGL_FlareWhite_F","3Rnd_UGL_FlareGreen_F","3Rnd_UGL_FlareRed_F","3Rnd_UGL_FlareYellow_F","3Rnd_UGL_FlareCIR_F","3Rnd_Smoke_Grenade_shell","3Rnd_SmokeRed_Grenade_shell","3Rnd_SmokeGreen_Grenade_shell","3Rnd_SmokeYellow_Grenade_shell","3Rnd_SmokePurple_Grenade_shell","3Rnd_SmokeBlue_Grenade_shell","3Rnd_SmokeOrange_Grenade_shell","ACE_HuntIR_M203"};
		};
		class M320_GL: M203_GL
		{
			magazines[] = {"rhs_mag_M441_HE","rhs_mag_M433_HEDP","rhs_mag_M4009","rhs_mag_m576","rhs_mag_M585_white","rhs_mag_M661_green","rhs_mag_M662_red","rhs_mag_M713_red","rhs_mag_M714_white","rhs_mag_M715_green","rhs_mag_M716_yellow","rhs_mag_M781_Practice","1Rnd_HE_Grenade_shell","UGL_FlareWhite_F","UGL_FlareGreen_F","UGL_FlareRed_F","UGL_FlareYellow_F","UGL_FlareCIR_F","1Rnd_Smoke_Grenade_shell","1Rnd_SmokeRed_Grenade_shell","1Rnd_SmokeGreen_Grenade_shell","1Rnd_SmokeYellow_Grenade_shell","1Rnd_SmokePurple_Grenade_shell","1Rnd_SmokeBlue_Grenade_shell","1Rnd_SmokeOrange_Grenade_shell","3Rnd_HE_Grenade_shell","3Rnd_UGL_FlareWhite_F","3Rnd_UGL_FlareGreen_F","3Rnd_UGL_FlareRed_F","3Rnd_UGL_FlareYellow_F","3Rnd_UGL_FlareCIR_F","3Rnd_Smoke_Grenade_shell","3Rnd_SmokeRed_Grenade_shell","3Rnd_SmokeGreen_Grenade_shell","3Rnd_SmokeYellow_Grenade_shell","3Rnd_SmokePurple_Grenade_shell","3Rnd_SmokeBlue_Grenade_shell","3Rnd_SmokeOrange_Grenade_shell","ACE_HuntIR_M203","rhs_mag_M441_HE","rhs_mag_M433_HEDP","rhs_mag_M4009","rhs_mag_m576","rhs_mag_M585_white","rhs_mag_M661_green","rhs_mag_M662_red","rhs_mag_M713_red","rhs_mag_M714_white","rhs_mag_M715_green","rhs_mag_M716_yellow","rhs_mag_M781_Practice","1Rnd_HE_Grenade_shell","UGL_FlareWhite_F","UGL_FlareGreen_F","UGL_FlareRed_F","UGL_FlareYellow_F","UGL_FlareCIR_F","1Rnd_Smoke_Grenade_shell","1Rnd_SmokeRed_Grenade_shell","1Rnd_SmokeGreen_Grenade_shell","1Rnd_SmokeYellow_Grenade_shell","1Rnd_SmokePurple_Grenade_shell","1Rnd_SmokeBlue_Grenade_shell","1Rnd_SmokeOrange_Grenade_shell","3Rnd_HE_Grenade_shell","3Rnd_UGL_FlareWhite_F","3Rnd_UGL_FlareGreen_F","3Rnd_UGL_FlareRed_F","3Rnd_UGL_FlareYellow_F","3Rnd_UGL_FlareCIR_F","3Rnd_Smoke_Grenade_shell","3Rnd_SmokeRed_Grenade_shell","3Rnd_SmokeGreen_Grenade_shell","3Rnd_SmokeYellow_Grenade_shell","3Rnd_SmokePurple_Grenade_shell","3Rnd_SmokeBlue_Grenade_shell","3Rnd_SmokeOrange_Grenade_shell","ACE_HuntIR_M203"};
		};
	};
	class rhs_weap_smaw: Launcher_Base_F // Adds buddy reload to SMAW
	{
		ace_reloadlaunchers_enabled = 1;
	};
	class rhs_weap_smaw_green: rhs_weap_smaw // Adds Buddy Reload to SMAW
	{
		ace_reloadlaunchers_enabled = 1;
	};
	class GM6_base_F;
	class rhs_weap_M107_Base_F: GM6_base_F // M107 fixes
	{
		dispersion = 0.00002909;
		ACE_barrelTwist = 381;
		ACE_barrelLength = 737;
		ACE_Overheating_dispersion = 0.0;
		ACE_Overheating_slowdownFactor = 0.001;
		ACE_Overheating_mrbs = 18000;
	};
	class rhsusf_acc_sniper_base: ItemCore // Framework for the TA-648
	{
		dlc = "RHS_USAF";
		scope = 0;
		author = "RHSUSAF";
		displayName = "$STR_RHSUSF_ACC_LEUPOLDMK401";
		descriptionShort = "$STR_RHSUSF_ACC_LEUPOLDMK4_SD";
		picture = "\rhsusf\addons\rhsusf_weapons\gear_acc\rhs_leo_icon_ca.paa";
		model = "\rhsusf\addons\rhsusf_weapons\acc\scopes\leupoldmk4\leupoldmk4";
		weaponInfoType = "RscWeaponZeroing";
		class ItemInfo: InventoryOpticsItem_Base_F
		{
			opticType = 2;
			mass = 4;
			RMBhint = "Optical Sniper Sight";
			optics = 1;
			modelOptics = "\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_leu_mildot";
			class OpticsModes
			{
				class pso1_scope
				{
					opticsID = 2;
					useModelOptics = 1;
					opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
					discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000};
					discreteDistanceInitIndex = 1;
					opticsZoomMax = "0.35/3.5";
					opticsZoomMin = "0.35/10";
					opticsZoomInit = "0.35/3.5";
					discretefov[] = {"0.35/3.5","0.35/10"};
					discreteInitIndex = 0;
					modelOptics[] = {"\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_leu_mildot3_5","\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_leu_mildot10"};
					memoryPointCamera = "opticView";
					visionMode[] = {"Normal"};
					opticsFlare = 1;
					opticsDisablePeripherialVision = 1;
					distanceZoomMin = 100;
					distanceZoomMax = 1000;
					cameraDir = "";
				};
			};
		};
		inertia = 0.2;
	};
	class rhsusf_acc_ACOG6: rhsusf_acc_sniper_base // TA- 648
	{
		scope = 2;
		author = "RHSUSAF";
		displayName = "TA-648";
		descriptionShort = "TA-648";
		picture = "\rhsusf\addons\rhsusf_weapons\gear_acc\rhs_acog_icon_ca.paa";
		model = "\rhsusf\addons\rhsusf_weapons\acc\scopes\acog\acog";
		opticsDisablePeripherialVision = 0;
		weaponInfoType = "RscWeaponZeroing";
		class ItemInfo: InventoryOpticsItem_Base_F
		{
			opticType = 2;
			mass = 4;
			RMBhint = "Rifle Combat Optic";
			optics = 1;
			modelOptics = "\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_acog";
			class OpticsModes
			{
				class elcan_scope
				{
					opticsID = 2;
					useModelOptics = 1;
					opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
					discreteDistance[] = {100};
					discreteDistanceInitIndex = 0;
					opticsZoomMin = "0.48/6";
					opticsZoomMax = "0.48/6";
					opticsZoomInit = "0.48/6";
					memoryPointCamera = "opticView";
					visionMode[] = {"Normal"};
					opticsFlare = 1;
					opticsDisablePeripherialVision = 1;
					distanceZoomMin = 300;
					distanceZoomMax = 1000;
					cameraDir = "";
				};
				class alternative_view
				{
					opticsID = 1;
					useModelOptics = 0;
					opticsPPEffects[] = {""};
					opticsZoomMin = 0.275;
					opticsZoomMax = 1.1;
					opticsZoomInit = 0.75;
					memoryPointCamera = "eye";
					visionMode[] = {};
					opticsFlare = 0;
					opticsDisablePeripherialVision = 0;
					distanceZoomMin = 300;
					distanceZoomMax = 300;
					cameraDir = "";
				};
			};
		};
		inertia = 0.1;
	};
		class rhsusf_acc_LEUPOLDMK4_2;
		class rhsusf_acc_premier: rhsusf_acc_LEUPOLDMK4_2 // ACE Adjustments for M8541A
	{
		displayName = "M8541A SSDS";
		scope = 2;
		author = "RHSUSAF";
		weaponInfoType = "RscWeaponZeroing";
		model = "\rhsusf\addons\rhsusf_weapons\acc\scopes\ph_5_25x_56mm\prem_scope";
		picture = "\rhsusf\addons\rhsusf_weapons\gear_acc\rhs_prem_icon_ca.paa";
		class ItemInfo: InventoryOpticsItem_Base_F
		{
			opticType = 2;
			mass = 8;
			ACE_ScopeAdjust_Vertical[] = { -4, 30 };
            ACE_ScopeAdjust_Horizontal[] = { -6, 6 };
            ACE_ScopeAdjust_VerticalIncrement = 0.1;
            ACE_ScopeAdjust_HorizontalIncrement = 0.1;
			RMBhint = "Optical Sniper Sight";
			optics = 1;
			modelOptics = "\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot3";
			class OpticsModes
			{
				class pso1_scope
				{
					opticsID = 1;
					useModelOptics = 1;
					opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
					discreteDistance[] = {100};
					discreteDistanceInitIndex = 0;
					opticsZoomMax = "0.25/3";
					opticsZoomMin = "0.25/15";
					opticsZoomInit = "0.25/3";
					discretefov[] = {"0.25/3","0.25/6","0.25/9","0.25/12","0.25/15"};
					discreteInitIndex = 0;
					modelOptics[] = {"\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot3","\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot6","\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot9","\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot12","\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot15"};
					memoryPointCamera = "opticView";
					visionMode[] = {"Normal"};
					opticsFlare = 1;
					opticsDisablePeripherialVision = 1;
					distanceZoomMin = 200;
					distanceZoomMax = 1800;
					cameraDir = "";
				};
			};
		};
	};
	class rhsusf_acc_premier_anpvs27: rhsusf_acc_premier // ACE Adjustments for M8541A
	{
		displayName = "M8541A + AN/PVS-27";
		scope = 2;
		author = "RHSUSAF";
		weaponInfoType = "RscWeaponZeroing";
		ACE_ScopeAdjust_Vertical[] = { -4, 30 };
        ACE_ScopeAdjust_Horizontal[] = { -6, 6 };
        ACE_ScopeAdjust_VerticalIncrement = 0.1;
        ACE_ScopeAdjust_HorizontalIncrement = 0.1;
		model = "\rhsusf\addons\rhsusf_weapons\acc\scopes\PVS27\PVS_27_M8451A";
		picture = "\rhsusf\addons\rhsusf_weapons\gear_acc\rhs_prem_icon_ca.paa";
		class ItemInfo: InventoryOpticsItem_Base_F
		{
			opticType = 2;
			mass = 14;
			RMBhint = "Optical Sniper Sight + Night Vision Device";
			optics = 1;
			modelOptics = "\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot3";
			class OpticsModes
			{
				class pso1_nvg
				{
					opticsID = 2;
					useModelOptics = 1;
					opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1","TankCommanderOptics1"};
					discreteDistance[] = {100};
					discreteDistanceInitIndex = 0;
					opticsZoomMax = "0.25/3";
					opticsZoomMin = "0.25/15";
					opticsZoomInit = "0.25/3";
					discretefov[] = {"0.25/3","0.25/6","0.25/9","0.25/12","0.25/15"};
					discreteInitIndex = 0;
					modelOptics[] = {"\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot3","\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot6","\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot9","\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot12","\rhsusf\addons\rhsusf_weapons\acc\scopes\optics\rhsusf_prem_mildot15"};
					memoryPointCamera = "opticView";
					visionMode[] = {"NVG"};
					opticsFlare = 1;
					opticsDisablePeripherialVision = 1;
					distanceZoomMin = 200;
					distanceZoomMax = 1800;
					cameraDir = "";
				};
			};
		};
		inertia = 0.4;
	};
	class rhs_weap_m240_base;
	class rhs_weap_m240B: rhs_weap_m240_base //Dispersion fix for M240
	{
		dispersion = 0.00101;
	};
	class rhs_weap_m240G: rhs_weap_m240B //Dispersion fix for M240
	{
		dispersion = 0.00101;
	};
	class rhs_weap_m240B_CAP: rhs_weap_m240B //Dispersion fix for M240
	{
		dispersion = 0.00101;
	};
	class H_HelmetB : ItemCore {
	class ItemInfo;
    };
    class rhsusf_mich_bare_norotos;
	class rhsusf_mich_bare_norotos_tan;
	class rhs_Booniehat_ocp;
	class rhsusf_mich_bare_norotos_alt: rhsusf_mich_bare_norotos // Hearing Protection for peltors
	{
		ace_hearing_protection = 0.9;
		ace_hearing_lowerVolume = 0.1;
	};
	class rhsusf_mich_bare_norotos_alt_tan: rhsusf_mich_bare_norotos_tan // Hearing Protection for peltors
	{
		ace_hearing_protection = 0.9;
		ace_hearing_lowerVolume = 0.1;
	};
	class rhsusf_mich_bare_norotos_alt_tan_w: rhsusf_mich_bare_norotos_alt_tan // Hearing Protection for peltors
	{
		scope = 2;
		displayName = "Mich 2000 WINTER/NOROTOS/ALT";
		ace_hearing_protection = 0.9;
		ace_hearing_lowerVolume = 0.01;
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\meu_bare_mich_norotos_w_co","meu_rhs_fixes\data\meu_mich_acc_w_co","meu_rhs_fixes\data\meu_mich_acc_w_co"};
	};
	class rhsusf_mich_bare_norotos_tan_w: rhsusf_mich_bare_norotos_tan // Hearing Protection for peltors
	{
		scope = 2;
		displayName = "Mich 2000 WINTER/NOROTOS";
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\meu_bare_mich_norotos_w_co","meu_rhs_fixes\data\meu_mich_acc_w_co","meu_rhs_fixes\data\meu_mich_acc_w_co"};
	};
	class rhs_booniehat2_marpatd;
	class rhs_Booniehat2_marpatw: rhs_booniehat2_marpatd // Winter Boonie
	{
		scope = 2;
		displayName = "Booniehat MARPAT-W";
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\meu_booniehat_marpatw_co"};
	};
	class meu_marpat_w: rhs_uniform_FROG01_d // Winter Uniform
	{
		scope = 2;
		author = "RHS/15thMEU";
		displayName = "FROG MARPAT-W";
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\meu_frog02_w_co.paa"};
		class ItemInfo: UniformItem
		{
			uniformModel = "-";
			uniformClass = "meu_usmc_winter";
			containerClass = "Supply40";
			mass = 40;
			hiddenSelections[] = {"Camo"};
		};
	};
	class rhsusf_ech_helmet_headset_marpatwd : H_HelmetB // Creates the additonal ECH helmets
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-WD (Peltor)";
		picture = "\rhsusf\addons\rhsusf_infantry2\ui\ach_ca.paa";
        model = "\rhsusf\addons\rhsusf_infantry\gear\head\ach_headset";
		hiddenSelections[] = {"Camo"};
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\15th_meu_mich_covered_rhino_wd_co.paa"};
		class ItemInfo: ItemInfo
		{
			mass = 40;
			uniformModel = "\rhsusf\addons\rhsusf_infantry\gear\head\ach_headset";
			allowedSlots[] = {801,901,701,605};
			modelSides[] = {6};
			class HitpointsProtectionInfo {
				class Head
				{
					hitpointName = "HitHead";
					armor = 6;
					passThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_ech_helmet_headset_marpatd : rhsusf_ech_helmet_headset_marpatwd
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-D (Peltor)";
		hiddenSelectionsTextures[] = {"rhsusf\addons\rhsusf_infantry\gear\head\data\mich_covered_rhino_des_co.paa"};
	};
	class rhsusf_ech_helmet_headset_marpatw : rhsusf_ech_helmet_headset_marpatwd
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-W (Peltor)";
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\15th_meu_mich_covered_rhino_w_co.paa"};
	};
	class rhsusf_ech_helmet_headset_ess_marpatwd : rhsusf_ech_helmet_headset_marpatwd
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-WD (Peltor/ESS)";
		model = "\rhsusf\addons\rhsusf_infantry\gear\head\ach_headset_ess";
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\15th_meu_mich_covered_rhino_wd_co.paa"};
		class ItemInfo: ItemInfo
		{
			mass = 40;
		uniformModel = "\rhsusf\addons\rhsusf_infantry\gear\head\ach_headset_ess";

		};
	};
	class rhsusf_ech_helmet_headset_ess_marpatd : rhsusf_ech_helmet_headset_ess_marpatwd
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-D (Peltor/ESS)";
		hiddenSelectionsTextures[] = {"rhsusf\addons\rhsusf_infantry\gear\head\data\mich_covered_rhino_des_co.paa"};
	};
	class rhsusf_ech_helmet_headset_ess_marpatw : rhsusf_ech_helmet_headset_ess_marpatwd
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-W (Peltor/ESS)";
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\15th_meu_mich_covered_rhino_w_co.paa"};
	};
	class rhsusf_ech_helmet_marpatwd: H_HelmetB // Lines 336-397 create the ECH for USMC
	{
		author = "RHS/15thMEU";
		scope = 2;
		displayName = "ECH MARPAT-WD";
		model = "\rhsusf\addons\rhsusf_infantry\gear\head\mich_01";
		picture = "\rhsusf\addons\rhsusf_infantry2\ui\mich2000_ca.paa";
		hiddenSelections[] = {"Camo1","Camo2"};
		hiddenSelectionsTextures[] = {"rhsusf\addons\rhsusf_infantry\gear\head\data\mich_covered_rhino_wd_co.paa","rhsusf\addons\rhsusf_infantry\gear\head\data\ach_acc_tan_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelections[] = {"Camo1","Camo2"};
			uniformModel = "\rhsusf\addons\rhsusf_infantry\gear\head\mich_01";
			allowedSlots[] = {801,901,701,605};
			modelSides[] = {6};
			mass = 40;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 6;
					passThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_ech_helmet_marpatwd_headset: rhsusf_ech_helmet_marpatwd
	{
		author = "RHS/15thMEU";
		displayName = "ECH MARPAT-WD (Headset)";
		model = "\rhsusf\addons\rhsusf_infantry\gear\head\mich_01_bowman";
		class ItemInfo: ItemInfo
		{
			uniformModel = "\rhsusf\addons\rhsusf_infantry\gear\head\mich_01_bowman";
		};
	};
	class rhsusf_ech_helmet_marpatd: rhsusf_ech_helmet_marpatwd
	{
		author = "RHS/15thMEU";
		displayName = "ECH MARPAT-D";
		hiddenSelectionsTextures[] = {"rhsusf\addons\rhsusf_infantry\gear\head\data\mich_covered_rhino_des_co.paa","rhsusf\addons\rhsusf_infantry\gear\head\data\ach_acc_tan_co.paa"};
	};
	class rhsusf_ech_helmet_marpatw: rhsusf_ech_helmet_marpatwd
	{
		author = "RHS/15thMEU";
		displayName = "ECH MARPAT-Winter";
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\meu_mich_covered_rhino_w_co.paa","rhsusf\addons\rhsusf_infantry\gear\head\data\ach_acc_tan_co.paa"};
	};
	class rhsusf_ech_helmet_marpatw_headset: rhsusf_ech_helmet_marpatwd_headset
	{
		author = "RHS/15thMEU";
		displayName = "ECH MARPAT-W (Headset)";
		hiddenSelectionsTextures[] = {"meu_rhs_fixes\data\meu_mich_covered_rhino_w_co.paa","rhsusf\addons\rhsusf_infantry\gear\head\data\ach_acc_tan_co.paa"};
	};
	class rhsusf_ech_helmet_marpatd_headset: rhsusf_ech_helmet_marpatwd_headset
	{
		author = "RHS/15thMEU";
		displayName = "ECH MARPAT-D (Headset)";
		hiddenSelectionsTextures[] = {"rhsusf\addons\rhsusf_infantry\gear\head\data\mich_covered_rhino_des_co.paa","rhsusf\addons\rhsusf_infantry\gear\head\data\ach_acc_tan_co.paa"};
	};
};
class cfgAmmo 
{
   class B_127x99_Ball;
   class B_762x54_Ball;
   class rhs_ammo_762x51_M80_Ball;
   class rhsusf_ammo_127x99_M33_Ball: B_127x99_Ball // Bring RHS 127x99 to realistic value 
    {
        hit = 45;
		ACE_caliber=12.954;
        ACE_bulletLength=58.674;
        ACE_bulletMass=41.9256;
        ACE_ammoTempMuzzleVelocityShifts[]={-26.55, -25.47, -22.85, -20.12, -16.98, -12.80, -7.64, -1.53, 5.96, 15.17, 26.19};
        ACE_ballisticCoefficients[]={0.670};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ASM";
        ACE_dragModel=1;
        ACE_muzzleVelocities[]={900};
        ACE_barrelLengths[]={737};
    };
	class rhsusf_ammo_127x99_mk211: rhsusf_ammo_127x99_M33_Ball // Bring RHS 127x99 to realistic value
	{
		hit = 44;
		ACE_caliber=12.954;
        ACE_bulletLength=58.674;
        ACE_bulletMass=41.9256;
        ACE_ammoTempMuzzleVelocityShifts[]={-26.55, -25.47, -22.85, -20.12, -16.98, -12.80, -7.64, -1.53, 5.96, 15.17, 26.19};
        ACE_ballisticCoefficients[]={0.670};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ASM";
        ACE_dragModel=1;
        ACE_muzzleVelocities[]={900};
        ACE_barrelLengths[]={737};
	};
	class rhs_ammo_762x51_M993_Ball: rhs_ammo_762x51_M80_Ball // Brings RHS 762x51 DM Ammo to better value than 5.56
	{
		hit = 20;
        ACE_caliber=7.823;
        ACE_bulletLength=31.496;
        ACE_bulletMass=8.2296;
        ACE_ammoTempMuzzleVelocityShifts[]={-26.55, -25.47, -22.85, -20.12, -16.98, -12.80, -7.64, -1.53, 5.96, 15.17, 26.19};
        ACE_ballisticCoefficients[]={0.377};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocities[]={875, 910, 930};
        ACE_barrelLengths[]={330.2, 406.4, 508.0};
	};
};
class asdg_SlotInfo; 
class asdg_OpticRail;
class asdg_OpticRail1913: asdg_OpticRail
{
	linkProxy = "\A3\data_f\proxies\weapon_slots\TOP";
	displayName = "$STR_A3_CowsSlot0";
	class compatibleItems
	{
		optic_Nightstalker = 1;
		optic_tws = 1;
		optic_tws_mg = 1;
		optic_NVS = 1;
		optic_SOS = 1;
		optic_MRCO = 1;
		optic_Arco = 1;
		optic_aco = 1;
		optic_ACO_grn = 1;
		optic_aco_smg = 1;
		optic_ACO_grn_smg = 1;
		optic_hamr = 1;
		optic_Holosight = 1;
		optic_Holosight_smg = 1;
		optic_DMS = 1;
		optic_LRPS = 1;
		optic_AMS = 1;
		optic_AMS_khk = 1;
		optic_AMS_snd = 1;
		optic_KHS_blk = 1;
		optic_KHS_hex = 1;
		optic_KHS_old = 1;
		optic_KHS_tan = 1;
		rhsusf_acc_LEUPOLDMK4 = 1;
		rhsusf_acc_LEUPOLDMK4_2 = 1;
		rhsusf_acc_LEUPOLDMK4_2_d = 1;
		rhsusf_acc_premier = 1;
		rhsusf_acc_premier_low = 1;
		rhsusf_acc_premier_anpvs27 = 1;
		rhsusf_acc_EOTECH = 1;
		rhsusf_acc_eotech_552 = 1;
		rhsusf_acc_eotech_552_d = 1;
		rhsusf_acc_eotech_xps3 = 1;
		rhsusf_acc_compm4 = 1;
		rhsusf_acc_ACOG = 1;
		rhsusf_acc_ACOG2 = 1;
		rhsusf_acc_ACOG3 = 1;
		rhsusf_acc_ACOG_wd = 1;
		rhsusf_acc_ACOG_d = 1;
		rhsusf_acc_ACOG_sa = 1;
		rhsusf_acc_ACOG_USMC = 1;
		rhsusf_acc_ACOG2_USMC = 1;
		rhsusf_acc_ACOG3_USMC = 1;
		rhsusf_acc_ACOG_PIP = 1;
		rhsusf_acc_ACOG2_pip = 1;
		rhsusf_acc_ACOG3_pip = 1;
		rhsusf_acc_ACOG_wd_pip = 1;
		rhsusf_acc_ACOG_d_pip = 1;
		rhsusf_acc_ACOG_sa_pip = 1;
		rhsusf_acc_ACOG_USMC_pip = 1;
		rhsusf_acc_ACOG2_USMC_pip = 1;
		rhsusf_acc_ACOG3_USMC_pip = 1;
		rhsusf_acc_ACOG_3d = 1;
		rhsusf_acc_ACOG2_3d = 1;
		rhsusf_acc_ACOG3_3d = 1;
		rhsusf_acc_ACOG_wd_3d = 1;
		rhsusf_acc_ACOG_d_3d = 1;
		rhsusf_acc_ACOG_sa_3d = 1;
		rhsusf_acc_ACOG_USMC_3d = 1;
		rhsusf_acc_ACOG2_USMC_3d = 1;
		rhsusf_acc_ACOG3_USMC_3d = 1;
		rhsusf_acc_ACOG_anpvs27 = 1;
		rhsusf_acc_ACOG6 = 1;
		rhsusf_acc_ELCAN = 1;
		rhsusf_acc_ELCAN_ard = 1;
		rhsusf_acc_ELCAN_3d = 1;
		rhsusf_acc_ELCAN_ard_3d = 1;
		rhsusf_acc_ELCAN_PIP = 1;
		rhsusf_acc_ELCAN_ard_PIP = 1;
		rhsusf_acc_SpecterDR = 1;
		rhsusf_acc_SpecterDR_3d = 1;
		rhsusf_acc_SpecterDR_A = 1;
		rhsusf_acc_SpecterDR_A_3d = 1;
		rhsusf_acc_SpecterDR_CX_3D = 1;
		rhsusf_acc_SpecterDR_pvs27 = 1;
		rhsusf_acc_SpecterDR_D = 1;
		rhsusf_acc_SpecterDR_OD = 1;
		rhsusf_acc_SpecterDR_D_3D = 1;
		rhsusf_acc_SpecterDR_OD_3D = 1;
		rhsusf_acc_anpvs27 = 1;
		rhsusf_acc_M2A1 = 1;
	};
};
class asdg_OpticRail1913_short: asdg_OpticRail1913
{
	class compatibleItems: compatibleItems
	{
		rhsusf_acc_LEUPOLDMK4 = 1;
		rhsusf_acc_LEUPOLDMK4_2 = 1;
		rhsusf_acc_LEUPOLDMK4_2_d = 1;
		rhsusf_acc_premier = 1;
		rhsusf_acc_premier_low = 1;
		rhsusf_acc_premier_anpvs27 = 1;
		rhsusf_acc_ACOG_anpvs27 = 1;
		rhsusf_acc_SpecterDR_pvs27 = 1;
	};
};