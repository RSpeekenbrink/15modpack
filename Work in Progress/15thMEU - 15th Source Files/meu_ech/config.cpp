#include "BIS_AddonInfo.hpp"
class CfgPatches
{
	class 15thmeu_ach
	{
		units[]={};
		weapons[]=
		{
			"rhsusf_ach_helmet_headset_marpatwd",
			"rhsusf_ach_helmet_headset_marpatw",
			"rhsusf_ach_helmet_headset_ess_marpatwd",
			"rhsusf_ach_helmet_headset_ess_marpatd",
			"rhsusf_ach_helmet_headset_ess_marpatw",
			"rhsusf_ach_helmet_headset_marpatd"
		};
		magazine[]={};
		ammo[]={};
		author[]=
		{
			"15thMEU"
		};
		requiredVersion=1.3200001;
		requiredAddons[]=
		{
			"A3_Weapons_F",
			"rhsusf_c_weapons"
		};
	};
};
class CfgWeapons
{
	class ItemInfo;
	class H_HelmetB;
	class rhsusf_ach_helmet_headset_marpatwd : H_HelmetB
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-WD (Peltor)";
		picture = "\rhsusf\addons\rhsusf_infantry2\ui\ach_ca.paa";

		model = "\rhsusf\addons\rhsusf_infantry\gear\head\ach_headset";

		hiddenSelections[] = {"Camo"};
		hiddenSelectionsTextures[] = {"15thmeu_ech\data\15th_meu_mich_covered_rhino_wd_co.paa"};

		class ItemInfo: ItemInfo
		{
			mass = 40;
			uniformModel = "\rhsusf\addons\rhsusf_infantry\gear\head\ach_headset";
			allowedSlots[] = {801,901,701,605};
			modelSides[] = {6};
			class HitpointsProtectionInfo {
				class Head
				{
					hitpointName = "HitHead";
					armor = 6;
					passThrough = 0.69999999;
				};
			};
		};
	};
	class rhsusf_ach_helmet_headset_marpatd : rhsusf_ach_helmet_headset_marpatwd
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-D (Peltor)";
		hiddenSelectionsTextures[] = {"rhsusf\addons\rhsusf_infantry\gear\head\data\mich_covered_rhino_des_co.paa"};
	};
	class rhsusf_ach_helmet_headset_marpatw : rhsusf_ach_helmet_headset_marpatwd
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-W (Peltor)";
		hiddenSelectionsTextures[] = {"15thmeu_ech\data\15th_meu_mich_covered_rhino_w_co.paa"};
	};
	class rhsusf_ach_helmet_headset_ess_marpatwd : rhsusf_ach_helmet_headset_marpatwd
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-WD (Peltor/ESS)";
		model = "\rhsusf\addons\rhsusf_infantry\gear\head\ach_headset_ess";
		hiddenSelectionsTextures[] = {"15thmeu_ech\data\15th_meu_mich_covered_rhino_wd_co.paa"};
		class ItemInfo: ItemInfo
		{
			mass = 40;
		uniformModel = "\rhsusf\addons\rhsusf_infantry\gear\head\ach_headset_ess";

		};
	};
	class rhsusf_ach_helmet_headset_ess_marpatd : rhsusf_ach_helmet_headset_ess_marpatwd
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-D (Peltor/ESS)";
		hiddenSelectionsTextures[] = {"rhsusf\addons\rhsusf_infantry\gear\head\data\mich_covered_rhino_des_co.paa"};
	};
	class rhsusf_ach_helmet_headset_ess_marpatw : rhsusf_ach_helmet_headset_ess_marpatwd
	{
		author="RHS/15thMEU";
		displayName = "ECH MARPAT-W (Peltor/ESS)";
		hiddenSelectionsTextures[] = {"15thmeu_ech\data\15th_meu_mich_covered_rhino_w_co.paa"};
	};
};	